<?php

class My_Controller extends CI_Controller{

	var $privilage_x;
	var $set_active_x;


	public function __construct() 
	{

		parent::__construct();
		$this->output->enable_profiler(false);
		$this->load->model('mdl_user', 'mdl_user');
		
		// jika belum login
		if (is_login() == FALSE){
			redirect(site_url().'auth/login');
		}

		// set privilage
		$this->set_privilage();

		// jika tidak diperbolehkan mengakses controller
		$ctr = $this->uri->segment(1);
		if ($this->can_access($ctr) == FALSE){
			redirect(site_url().'auth/failed');
		}

	}


	function open($page)
	{ 
		 
		$data['privilage'] = $this->privilage_x;
		$data['set_active'] = $this->set_active_menu($page); 
		$data['user_id'] = $this->session->userdata('user_id');
		$data['username'] = $this->session->userdata('username');
		$data['users'] = $this->mdl_user->get_data_where($data['user_id']);
		$this->load->view('layout/header',$data);
		$this->load->view('layout/menu', $data);
		
	}

	

	function close() 
	{

		$this->load->view('layout/footer');

	}
	
	function alert($post) 
	{
	
		$result = '';
		
		if($post)
		{
			$array = explode (',', $post);   
			$id = $array[0]; 
			$message = "".$array[1].""; 
				
			if($id == 1)
			{
				$result = '
				<div class="alert alert-success icons-alert" style="margin-bottom:20px;">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<i class="icofont icofont-close-line-circled"></i>
					</button>
					<p>'.$message.'</p>
				</div> ';
			}
			else if($id == 0)
			{
				
				$result = '
				<div class="alert alert-danger icons-alert" style="margin-bottom:20px;">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<i class="icofont icofont-close-line-circled"></i>
					</button>
					<p>'.$message.'</p>
				</div> ';
			}
		
		}
		return $result;

	}

	

	function set_privilage()
	{

		// cek user
		$row = $this->session->userdata('userlevel');

		# set privilage
		# init privilage
		# ambil privilage dari users_level
		$this->db->flush_cache();
		$this->db->where('id', $row);
		$row_pri = $this->db->get('user_level')->row_array();
		$fields = $this->db->field_data('user_level'); 

		foreach($fields as $field){

			if (($field->name != 'id') and ($field->name != 'nama'))
			{
				$tmp_pri = str_split($row_pri[$field->name]); 
				$privilage[$field->name][0] = $tmp_pri[0];
				$privilage[$field->name][1] = $tmp_pri[1]; //view
				$privilage[$field->name][2] = $tmp_pri[2]; //insert
				$privilage[$field->name][3] = $tmp_pri[3]; //update
				$privilage[$field->name][4] = $tmp_pri[4]; //delete
			}

		}

		$this->privilage_x = $privilage;

	}

	
	
	function can_access($ctr='')
	{	

		$priv = $this->privilage_x;
		$form = $ctr;

		if($form){
			if($priv[$form][0] == 1){
				return TRUE;
			}else{
				return FALSE;
			}
		}else{
			return TRUE;
		}

	}

	

	function can_view()
	{

		$priv = $this->privilage_x;
		$form = $this->uri->segment(1);

		if($form){
			if($priv[$form][1] == 1){
				return TRUE;
			}else{
				return FALSE;
			}
		}else{
			return TRUE;
		}

	}

	

	function can_insert()
	{

		$priv = $this->privilage_x;
		$form = $this->uri->segment(1);

		if($form){
			if($priv[$form][2] == 1){
				return TRUE;
			}else{
				return FALSE;
			}
		}else{
			return TRUE;
		}

	}

	

	function can_update()
	{

		$priv = $this->privilage_x;
		$form = $this->uri->segment(1);

		if($form){
			if($priv[$form][3] == 1){
				return TRUE;
			}else{
				return FALSE;
			}
		}else{
			return TRUE;
		}

	}

	

	function can_delete()
	{

		$priv = $this->privilage_x;
		$form = $this->uri->segment(1);

		if($form){
			if($priv[$form][4] == 1){
				return TRUE;
			}else{
				return FALSE;
			}
		}else{
			return TRUE;
		}

	}

	function set_active_menu($page) 
	{ 
	
		$fields = $this->db->field_data('user_level');

		foreach($fields as $field){
			if (($field->name != 'level_id') and ($field->name != 'nama'))
			{  
				if($field->name == $page){
					$set_active[$field->name] = "active";
				}else{
					$set_active[$field->name] = "";
				} 
			}
		}
 
		return $set_active;
	}

}