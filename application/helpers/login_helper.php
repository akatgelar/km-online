<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

# Cek login
if ( ! function_exists('is_login'))
{
	function is_login()
	{
		//print_r(get_instance()->session->userdata('privilage'));
		
		$token = get_instance()->session->userdata('token');
		
		if ($token){
			$row = get_instance()->mdl_auth->get_userByToken($token);
			
			if ($row->num_rows() > 0){
				return TRUE;
			}else{
				return FALSE;
			}
		}
		
		return FALSE;
	}
}

# get Logged as
if ( ! function_exists('logged_as'))
{
	function logged_as()
	{
		return get_instance()->session->userdata('logged_as');
	}
}

# get User ID
if ( ! function_exists('get_userid'))
{
	function get_userid()
	{
		return get_instance()->session->userdata('userid');
	}
}
 

# get user level
if ( ! function_exists('get_userlevel'))
{
	function get_userlevel()
	{
		return get_instance()->session->userdata('userlevel');
	}
}

if ( ! function_exists('redirect_back'))
{
    function redirect_back()
    {
        if(isset($_SERVER['HTTP_REFERER']))
        {
            header('Location: '.$_SERVER['HTTP_REFERER']);
        }
        else
        {
            header('Location: http://'.$_SERVER['SERVER_NAME']);
        }
        exit;
    }
}

