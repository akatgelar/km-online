<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends My_Controller {


	function __construct() 
	{
		parent::__construct();
		$this->load->model('mdl_user', 'mdl_user');
	}


	public function index()
	{
		$data['can_access'] = $this->can_access();
		$data['can_view'] 	= $this->can_view();
		$data['can_insert'] = $this->can_insert();
		$data['can_update'] = $this->can_update();
		$data['can_delete'] = $this->can_delete();

		$this->open('user'); 
		$data['alert'] = $this->alert($this->session->flashdata('alert'));
		$data['results'] = $this->mdl_user->get_data();
		$this->load->view('user/user_list', $data);
		$this->close();
	}

	function preview($id) 
	{
	
		$data['can_access'] = $this->can_access();
		$data['can_view'] 	= $this->can_view();
		$data['can_insert'] = $this->can_insert();
		$data['can_update'] = $this->can_update();
		$data['can_delete'] = $this->can_delete(); 
		
		$data['results'] = $this->mdl_user->get_data_where($id);
		$this->load->view('user/user_preview', $data);
		 
	}

	function add()
	{
		if ($this->can_insert() == FALSE){
			redirect(site_url().'admin/login/failed'); 
		}
		
		$data['can_view'] 	= $this->can_view();

		$this->open('user'); 
		$data['alert'] = $this->alert($this->session->flashdata('alert'));
		$data['level'] = $this->mdl_user->get_level();
		$this->load->view('user/user_add',$data); 
		$this->close();
	}
	

	function insert() 
	{
		if ($this->can_insert() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
      
		$this->form_validation->set_rules('level_id', 'level_id', 'required');
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
     
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('alert', "0,Field harus diisi!");		
			redirect_back(); 
		}
		else
		{	
			// $config['upload_path'] =  $_SERVER['DOCUMENT_ROOT'].'/telkom-km-online/assets/upload/profile/';
			$config['upload_path'] =  '/var/www/html/assets/upload/profile/';
			$config['allowed_types'] = '*';
			$config['max_size']  = '10000'; 
			$this->load->library('upload', $config);

			$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
			$tz_object = new DateTimeZone($tz_string); 
			$datetime = new DateTime(); 
			$datetime->setTimezone($tz_object);  
			
			if ( !$this->upload->do_upload())
			{
				$data['level_id'] = $this->input->post('level_id');
				$data['username'] = $this->input->post('username');
				$data['password'] = md5($this->input->post('password'));
				$data['password_plan'] = $this->input->post('password');
				$data['cdate'] = $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
				$data['cuid'] = get_instance()->session->userdata('user_id');  
				$this->mdl_user->insert($data); 
			} 
			else
			{	
				$upload_data = $this->upload->data(); 
				$nama_lama =  $upload_data['file_name'];
				$nama_baru =  'user_'. date("Y.m.d") . '-' . date("H.i.s") .'-'.  $upload_data['file_ext'];
				//upload gede
				rename($config['upload_path'] . $nama_lama, $config['upload_path'] . $nama_baru);

				//upload thumb 
				$fileNameResize = $config['upload_path'].$upload_data['file_name'];
				$resize = array(
					"width"         => 100,
					"height"        => 100,
					"quality"        => '100%',
					"source_image"    => $config['upload_path'] . $nama_baru,
					"new_image"        => $config['upload_path'] . 'thumb/' . $nama_baru
				);
				$this->image_lib->initialize($resize); 
				if(!$this->image_lib->resize())					
					die($this->image_lib->display_errors());
			 
				$data['foto'] = $nama_baru;
				$data['level_id'] = $this->input->post('level_id');
				$data['username'] = $this->input->post('username');
				$data['password'] = md5($this->input->post('password'));
				$data['password_plan'] = $this->input->post('password');
				$data['cdate'] = $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
				$data['cuid'] = get_instance()->session->userdata('user_id');  
				$this->mdl_user->insert($data); 
			}

			$this->session->set_flashdata('alert', "1,Data berhasil ditambah.");
			redirect(site_url().'user');

		}
	}


	function edit($id) 
	{
		if ($this->can_update() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
		
		$data['can_view'] 	= $this->can_view();
		
		$this->open('user'); 
		$data['alert'] = $this->alert($this->session->flashdata('alert'));
		$data['level'] = $this->mdl_user->get_level();
		$data['results'] = $this->mdl_user->get_data_where($id);
		$this->load->view('user/user_edit', $data);
		$this->close();
	}
	
	
	function update() 
	{
 
		if ($this->can_update() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
    
		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('level_id', 'level_id', 'required');
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('alert', "0,Field harus diisi!");	
			redirect_back(); 
		}
		else
		{	
			// $config['upload_path'] =  $_SERVER['DOCUMENT_ROOT'].'/telkom-km-online/assets/upload/profile/';
			$config['upload_path'] =  '/var/www/html/frontend/assets/upload/profile/';
			$config['allowed_types'] = '*';
			$config['max_size']  = '10000'; 
			$this->load->library('upload', $config);

			$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
			$tz_object = new DateTimeZone($tz_string); 
			$datetime = new DateTime(); 
			$datetime->setTimezone($tz_object);  
			
			if ( !$this->upload->do_upload())
			{ 
				$data['id'] = $this->input->post('id'); 
				$data['level_id'] = $this->input->post('level_id');
				$data['username'] = $this->input->post('username');
				$data['password'] = md5($this->input->post('password'));
				$data['password_plan'] = $this->input->post('password');
				$data['mdate'] = $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
				$data['muid'] = get_instance()->session->userdata('user_id');  
				$this->mdl_user->update($data['id'], $data); 
			} 
			else
			{	
				$upload_data = $this->upload->data(); 
				$nama_lama =  $upload_data['file_name'];
				$nama_baru =  'user_'. date("Y.m.d") . '-' . date("H.i.s") .'-'.  $upload_data['file_ext'];

				//upload gede
				rename($config['upload_path'] . $nama_lama, $config['upload_path'] . $nama_baru);
				
				//upload thumb 
				$fileNameResize = $config['upload_path'].$upload_data['file_name'];
				$resize = array(
					"width"         => 100,
					"height"        => 100,
					"quality"        => '100%',
					"source_image"    => $config['upload_path'] . $nama_baru,
					"new_image"        => $config['upload_path'] . 'thumb/' . $nama_baru
				);
				$this->image_lib->initialize($resize); 
				if(!$this->image_lib->resize())					
					die($this->image_lib->display_errors());
			 
				$data['id'] = $this->input->post('id');
				$data['foto'] = $nama_baru;
				$data['level_id'] = $this->input->post('level_id');
				$data['username'] = $this->input->post('username');
				$data['password'] = md5($this->input->post('password'));
				$data['password_plan'] = $this->input->post('password');
				$data['mdate'] = $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
				$data['muid'] = get_instance()->session->userdata('user_id');  
				$this->mdl_user->update($data['id'], $data); 
			}

			$this->session->set_flashdata('alert', "1,Data berhasil diubah.");	
			redirect(site_url().'user');

		}
	}
	

	function delete($id) 
	{
		if ($this->can_delete() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
  
		$this->mdl_user->delete($id);
		$this->session->set_flashdata('alert', "1,Data berhasil dihapus");		
		redirect(site_url().'user'); 
	}

			
	function active($id_artikel) 
	{ 
		$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
		$tz_object = new DateTimeZone($tz_string); 
		$datetime = new DateTime(); 
		$datetime->setTimezone($tz_object);  
		 
		$data['id'] 				= $id_artikel;
		$data['is_active'] 			= '1';
		$data['muid'] 				= get_instance()->session->userdata('user_id'); 
		$data['mdate'] 				= $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
		 
		$this->mdl_user->update($data['id'], $data);
		$this->session->set_flashdata('alert', "1,Data telah berhasil diaktifkan.");	
		redirect(site_url().'user'); 
	}

	function nonactive($id_artikel) 
	{
		$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
		$tz_object = new DateTimeZone($tz_string); 
		$datetime = new DateTime(); 
		$datetime->setTimezone($tz_object);   
		
		$data['id'] 				= $id_artikel;
		$data['is_active'] 			= '0';
		$data['muid'] 				= get_instance()->session->userdata('user_id'); 
		$data['mdate'] 				= $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
		 
		$this->mdl_user->update($data['id'], $data);
		$this->session->set_flashdata('alert', "1,Data telah berhasil dinonaktifkan.");	
		redirect(site_url().'user'); 
	}
	
	



}
