<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bidang extends My_Controller {


	function __construct() 
	{
		parent::__construct();
		$this->load->model('mdl_bidang', 'mdl_bidang'); 
		$this->load->model('mdl_bidang_kategori', 'mdl_bidang_kategori'); 
	}


	public function index()
	{
		$data['can_access'] = $this->can_access();
		$data['can_view'] 	= $this->can_view();
		$data['can_insert'] = $this->can_insert();
		$data['can_update'] = $this->can_update();
		$data['can_delete'] = $this->can_delete();

		$this->open('bidang'); 
		$data['alert'] = $this->alert($this->session->flashdata('alert'));
		$data['results'] = $this->mdl_bidang->get_data();
		$this->load->view('bidang/bidang_list', $data);
		$this->close();
	}

	function preview($id) 
	{
	
		$data['can_access'] = $this->can_access();
		$data['can_view'] 	= $this->can_view();
		$data['can_insert'] = $this->can_insert();
		$data['can_update'] = $this->can_update();
		$data['can_delete'] = $this->can_delete(); 
		
		$this->open('bidang'); 
		$data['results'] = $this->mdl_bidang->get_data_where($id);
		$this->load->view('bidang/bidang_preview', $data);
		$this->close();
		 
	}

	function add()
	{
		if ($this->can_insert() == FALSE){
			redirect(site_url().'admin/login/failed'); 
		}
		
		$data['can_view'] 	= $this->can_view();

		$this->open('bidang');  
		$data['kategori'] = $this->mdl_bidang_kategori->get_data();
		$data['alert'] = $this->alert($this->session->flashdata('alert'));
		$this->load->view('bidang/bidang_add',$data); 
		$this->close();
	}
	

	function insert() 
	{
		if ($this->can_insert() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
      
		$this->form_validation->set_rules('kategori_id', 'kategori_id', 'required'); 
		$this->form_validation->set_rules('nama_pendek', 'nama_pendek', 'required'); 
		$this->form_validation->set_rules('nama_panjang', 'nama_panjang', 'required'); 

		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('alert', "0,Field harus diisi!");		
			redirect_back(); 
		}
		else
		{	 

			$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
			$tz_object = new DateTimeZone($tz_string); 
			$datetime = new DateTime(); 
			$datetime->setTimezone($tz_object);  
			 
			$data['kategori_id'] = $this->input->post('kategori_id');  
			$data['nama_pendek'] = $this->input->post('nama_pendek');  
			$data['nama_panjang'] = $this->input->post('nama_panjang');
			$data['keterangan'] = $this->input->post('keterangan');
			$data['cdate'] = $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
			$data['cuid'] = get_instance()->session->userdata('user_id');  
			$this->mdl_bidang->insert($data); 
			  
			$this->session->set_flashdata('alert', "1,Data berhasil ditambah.");
			redirect(site_url().'bidang');

		}
	}


	function edit($id) 
	{
		if ($this->can_update() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
		
		$data['can_view'] 	= $this->can_view();
		
		$this->open('bidang');  
		$data['alert'] = $this->alert($this->session->flashdata('alert'));
		$data['kategori'] = $this->mdl_bidang_kategori->get_data();
		$data['results'] = $this->mdl_bidang->get_data_where($id);
		$this->load->view('bidang/bidang_edit', $data);
		$this->close();
	}
	
	
	function update() 
	{
 
		if ($this->can_update() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
    
		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('kategori_id', 'kategori_id', 'required'); 
		$this->form_validation->set_rules('nama_pendek', 'nama_pendek', 'required'); 
		$this->form_validation->set_rules('nama_panjang', 'nama_panjang', 'required'); 
     
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('alert', "0,Field harus diisi!");	
			redirect_back(); 
		}
		else
		{	 

			$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
			$tz_object = new DateTimeZone($tz_string); 
			$datetime = new DateTime(); 
			$datetime->setTimezone($tz_object);  
			 
			$data['id'] = $this->input->post('id');   
			$data['kategori_id'] = $this->input->post('kategori_id');  
			$data['nama_pendek'] = $this->input->post('nama_pendek');  
			$data['nama_panjang'] = $this->input->post('nama_panjang');  
			$data['keterangan'] = $this->input->post('keterangan');
			$data['mdate'] = $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
			$data['muid'] = get_instance()->session->userdata('user_id');  
			$this->mdl_bidang->update($data['id'], $data); 
			 
			$this->session->set_flashdata('alert', "1,Data berhasil diperbaharui.");	
			redirect(site_url().'bidang');

		}
	}
	

	function delete($id) 
	{
		if ($this->can_delete() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
  
		$this->mdl_bidang->delete($id);
		$this->session->set_flashdata('alert', "1,Data berhasil dihapus");		
		redirect(site_url().'bidang'); 
	}

			
	function active($id_artikel) 
	{ 
		$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
		$tz_object = new DateTimeZone($tz_string); 
		$datetime = new DateTime(); 
		$datetime->setTimezone($tz_object);  
		 
		$data['id'] 				= $id_artikel;
		$data['is_active'] 			= '1';
		$data['muid'] 				= get_instance()->session->userdata('user_id'); 
		$data['mdate'] 				= $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
		 
		$this->mdl_bidang->update($data['id'], $data);
		$this->session->set_flashdata('alert', "1,Data telah berhasil diaktifkan.");	
		redirect(site_url().'bidang'); 
	}

	function nonactive($id_artikel) 
	{
		$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
		$tz_object = new DateTimeZone($tz_string); 
		$datetime = new DateTime(); 
		$datetime->setTimezone($tz_object);   
		
		$data['id'] 				= $id_artikel;
		$data['is_active'] 			= '0';
		$data['muid'] 				= get_instance()->session->userdata('user_id'); 
		$data['mdate'] 				= $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
		 
		$this->mdl_bidang->update($data['id'], $data);
		$this->session->set_flashdata('alert', "1,Data telah berhasil dinonaktifkan.");	
		redirect(site_url().'bidang'); 
	}
	
	



}
