<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends My_Controller {


	function __construct() 
	{
		parent::__construct();
		$this->load->model('mdl_auth', 'mdl_auth');
		$this->load->model('mdl_bidang_kategori', 'mdl_bidang_kategori');
		$this->load->model('mdl_bidang', 'mdl_bidang');
		$this->load->model('mdl_home', 'mdl_home');
	}


	public function index()
	{
		$this->open('home'); 
		$data = [];
		$data['results'] = [];
		$kategories = $this->mdl_bidang_kategori->get_data()->result();
		foreach($kategories as $ktgr){ 
			$bidang = $this->mdl_bidang->get_data_where_kategori($ktgr->id)->result();
			$temp_bdg = [];
			foreach($bidang as $bdg){
				array_push($temp_bdg, $bdg);
			} 

			$temp_ktgr['id'] = $ktgr->id;
			$temp_ktgr['nama_panjang'] = $ktgr->nama_panjang;
			$temp_ktgr['nama_pendek'] = $ktgr->nama_pendek;
			$temp_ktgr['keterangan'] = $ktgr->keterangan;  
			$temp_ktgr['bidang'] = $temp_bdg;
			array_push($data['results'], $temp_ktgr); 
		}
		// print_r($data['results']);
		// $data['article'] = $this->mdl_home->get_count_article()[0]; 
		// $data['news'] = $this->mdl_home->get_count_news()[0];
		// $data['subscribe'] = $this->mdl_home->get_count_subscribe()[0];
		$this->load->view('home/home_list', $data);
		$this->close();
	}

	 
}
