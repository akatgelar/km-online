<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends My_Controller {


	function __construct() 
	{
		parent::__construct();
		$this->load->model('mdl_user', 'mdl_user');
	}


	public function index()
	{
		$data['can_access'] = $this->can_access();
		$data['can_view'] 	= $this->can_view();
		$data['can_insert'] = $this->can_insert();
		$data['can_update'] = $this->can_update();
		$data['can_delete'] = $this->can_delete();

		$this->open(''); 
		$data['alert'] = $this->alert($this->session->flashdata('alert'));
		$data['cuid'] = get_instance()->session->userdata('user_id');  
		$data['results'] = $this->mdl_user->get_data_where($data['cuid']);
		$data['level'] = $this->mdl_user->get_level($data['cuid']);
		$this->load->view('profile/profile_list', $data);
		$this->close();
	}

	
	function update_foto() 
	{
 
		if ($this->can_update() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
    
		$this->form_validation->set_rules('id', 'id', 'required');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('alert', "0,Field harus diisi!");	
			redirect_back(); 
		}
		else
		{	
			// $config['upload_path'] =  $_SERVER['DOCUMENT_ROOT'].'/telkom-km-online/assets/upload/profile/';
			$config['upload_path'] =  '/var/www/html/assets/upload/profile/';
			$config['allowed_types'] = '*';
			$config['max_size']  = '10000'; 
			$this->load->library('upload', $config);

			$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
			$tz_object = new DateTimeZone($tz_string); 
			$datetime = new DateTime(); 
			$datetime->setTimezone($tz_object);  
			
			if ($this->upload->do_upload())
			{	
				$upload_data = $this->upload->data(); 
				$nama_lama =  $upload_data['file_name'];
				$nama_baru =  'user_'. date("Y.m.d") . '-' . date("H.i.s") .'-'.  $upload_data['file_ext'];

				//upload gede
				rename($config['upload_path'] . $nama_lama, $config['upload_path'] . $nama_baru);
				
				//upload thumb 
				$fileNameResize = $config['upload_path'].$upload_data['file_name'];
				$resize = array(
					"width"         => 100,
					"height"        => 100,
					"quality"        => '100%',
					"source_image"    => $config['upload_path'] . $nama_baru,
					"new_image"        => $config['upload_path'] . 'thumb/' . $nama_baru
				);
				$this->image_lib->initialize($resize); 
				if(!$this->image_lib->resize())					
					die($this->image_lib->display_errors());
			 
				$data['id'] = $this->input->post('id');
				$data['foto'] = $nama_baru;
				$data['mdate'] = $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
				$data['muid'] = get_instance()->session->userdata('user_id');  
				$this->mdl_user->update($data['id'], $data); 
			}

			$this->session->set_flashdata('alert', "1,Data berhasil diperbaharui.");	
			redirect(site_url().'profile');

		}
	}

	function update_password() 
	{
 
		if ($this->can_update() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
    
		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('alert', "0,Field harus diisi!");	
			redirect_back(); 
		}
		else
		{	
			
			$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
			$tz_object = new DateTimeZone($tz_string); 
			$datetime = new DateTime(); 
			$datetime->setTimezone($tz_object);  

			$data['id'] = $this->input->post('id'); 
			$data['username'] = $this->input->post('username');
			$data['password'] = md5($this->input->post('password'));
			$data['password_plan'] = $this->input->post('password');
			$data['mdate'] = $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
			$data['muid'] = get_instance()->session->userdata('user_id');  
			$this->mdl_user->update($data['id'], $data); 
			
			$this->session->set_flashdata('alert', "1,Data berhasil diperbaharui.");	
			redirect(site_url().'profile');

		}
	}
	

	
	



}
