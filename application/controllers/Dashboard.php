<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends My_Controller {


	function __construct() 
	{
		parent::__construct();
		$this->load->model('mdl_auth', 'mdl_auth');
		$this->load->model('mdl_dashboard', 'mdl_dashboard');
	}


	public function index()
	{
		$this->open('dashboard'); 
		$data = []; 
		$this->load->view('dashboard/dashboard_list', $data);
		$this->close();
	}

	function tinymce_upload() {
		// $config['upload_path'] =  $_SERVER['DOCUMENT_ROOT'].'/telkom-km-online/assets/upload/img/';
		$config['upload_path'] =  '/var/www/html/assets/upload/img/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = 0;
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('file')) {
            $this->output->set_header('HTTP/1.0 500 Server Error');
            exit;
        } else {
            $file = $this->upload->data();
            $this->output
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode(['location' => base_url().'assets/upload/img/'.$file['file_name']]))
                ->_display();
            exit;
        }
    }
}
