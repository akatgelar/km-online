<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Km extends My_Controller {


	function __construct() 
	{
		parent::__construct();
		$this->load->model('mdl_km', 'mdl_km');  
		$this->load->model('mdl_km_nilai', 'mdl_km_nilai');  
		$this->load->model('mdl_periode', 'mdl_periode');  
		$this->load->model('mdl_tahun', 'mdl_tahun');  
		$this->load->model('mdl_bidang', 'mdl_bidang');  
	}


	public function index()
	{
		$data['can_access'] = $this->can_access();
		$data['can_view'] 	= $this->can_view();
		$data['can_insert'] = $this->can_insert();
		$data['can_update'] = $this->can_update();
		$data['can_delete'] = $this->can_delete();

		$this->open('km'); 
		$data['alert'] = $this->alert($this->session->flashdata('alert'));
		$data['results'] = $this->mdl_km->get_data();
		$this->load->view('km/km_list', $data);
		$this->close();
	}

	function preview($id) 
	{
	
		$data['can_access'] = $this->can_access();
		$data['can_view'] 	= $this->can_view();
		$data['can_insert'] = $this->can_insert();
		$data['can_update'] = $this->can_update();
		$data['can_delete'] = $this->can_delete(); 
		
		$this->open('km'); 
		$data['km'] = $this->mdl_km->get_data_where($id)->result_array(); 
		$data['detail'] = $this->mdl_km->get_data_detail($id);
		$data['periode'] = $this->mdl_km->get_data_periode($data['km'][0]['periode_id']);
		$data['periode2'] = [];
		foreach($data['periode']->result() as $per){
			$temp = $this->mdl_km->get_data_nilai($id, $per->id);
			$temp2['periode_bulan_id'] = $per->id;
			$temp2['nama_bulan'] = $per->nama_bulan;
			$temp2['nilai'] = $temp->result(); 
			array_push($data['periode2'], $temp2);
		} 
		$this->load->view('km/km_preview', $data);
		$this->close();
		 
	}

	function add()
	{
		if ($this->can_insert() == FALSE){
			redirect(site_url().'admin/login/failed'); 
		}
		
		$data['can_view'] 	= $this->can_view();

		$this->open('km');  
		$data['bidang'] = $this->mdl_bidang->get_data();
		$data['periode'] = $this->mdl_periode->get_data();
		$data['tahun'] = $this->mdl_tahun->get_data();
		$data['alert'] = $this->alert($this->session->flashdata('alert'));
		$this->load->view('km/km_add',$data); 
		$this->close();
	}
	 
    function getPeriode(){
        $tahun_id = $_GET['tahun_id'];
        $periode   = $this->db->get_where('periode',array('tahun_id'=>$tahun_id)); 
		echo '<select name="periode_id" id="periode_id" class="form-control"  onchange="loadBulan()">';
		echo '<option value="">-- Pilih Periode --</option> ';
        foreach ($periode->result() as $row)
        {
            echo "<option value='$row->id'>$row->nama_periode</option>";
        }
        echo "</select></div>";
	}

	
    function getBulan(){
        $periode_id = $_GET['periode_id'];
        $bulan   = $this->db->get_where('periode_bulan',array('periode_id'=>$periode_id)); 
        foreach ($bulan->result() as $row)
        { 
			echo $row->id;
			echo "|";
			echo $row->nama_bulan;
			echo " , ";
        } 
	}
	
	function insert() 
	{
		if ($this->can_insert() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
      
		$this->form_validation->set_rules('bidang_id', 'bidang_id', 'required'); 
		$this->form_validation->set_rules('tahun_id', 'tahun_id', 'required'); 
		$this->form_validation->set_rules('periode_id', 'periode_id', 'required'); 

		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('alert', "0,Field harus diisi!");		
			redirect_back(); 
		}
		else
		{	 

			$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
			$tz_object = new DateTimeZone($tz_string); 
			$datetime = new DateTime(); 
			$datetime->setTimezone($tz_object);  
			 
			$val = $this->input->post(); 
			// print_r($val);
			$val['km_detail'] = $val['km_detail']['km_detail'];  

			$data['bidang_id'] = $val['bidang_id'];  
			$data['tahun_id'] = $val['tahun_id'];  
			$data['periode_id'] = $val['periode_id'];      
			$data['status'] = 'draft';   
			$data['cdate'] = $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
			$data['cuid'] = get_instance()->session->userdata('user_id');  
			$km_id = $this->mdl_km->insert_km($data); 

			$parent0 = NULL;
			$parent1 = 0;
			$parent2 = 0; 
			$parent3 = 0; 
			foreach($val['km_detail'] as $row){

				$temp['km_id'] = $km_id;
				$temp['level'] = $row['level'];
				$temp['kpi'] = $row['kpi'];
				$temp['weight'] = $row['weight'];
				$temp['unit'] = $row['unit'];

				// print_r($temp);
				if($temp['level'] == 1){
					$temp['parent'] = $parent0;
					$km_detail_id = $this->mdl_km->insert_km_detail($temp); 
					$parent1 = $km_detail_id;
				}else if($temp['level'] == 2){
					$temp['parent'] = $parent1;
					$km_detail_id = $this->mdl_km->insert_km_detail($temp); 
					$parent2 = $km_detail_id;
				}else if($temp['level'] == 3){
					$temp['parent'] = $parent2;
					$km_detail_id = $this->mdl_km->insert_km_detail($temp); 
					$parent3 = $km_detail_id; 
				}else if($temp['level'] == 4){
					$temp['parent'] = $parent3;
					$km_detail_id = $this->mdl_km->insert_km_detail($temp);  
				}

				$temp1['km_id'] = $km_id;
				$temp1['km_detail_id'] = $km_detail_id;
				$temp1['periode_bulan_id'] = $val['bulan_id1'];
				$temp1['target'] = $row['target1']; 
				// print_r($temp1);
				$km_nilai1 = $this->mdl_km_nilai->insert($temp1);  

				$temp2['km_id'] = $km_id;
				$temp2['km_detail_id'] = $km_detail_id;
				$temp2['periode_bulan_id'] = $val['bulan_id2'];
				$temp2['target'] = $row['target2']; 
				// print_r($temp2);
				$km_nilai2 = $this->mdl_km_nilai->insert($temp2);  

				$temp3['km_id'] = $km_id;
				$temp3['km_detail_id'] = $km_detail_id;
				$temp3['periode_bulan_id'] = $val['bulan_id3'];
				$temp3['target'] = $row['target3']; 
				// print_r($temp3);
				$km_nilai3 = $this->mdl_km_nilai->insert($temp3);  
			}
			  
			$this->session->set_flashdata('alert', "1,Data berhasil ditambah.");
			redirect(site_url().'km');

		}
	}

	function copy($id) 
	{
		if ($this->can_update() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
		
		$data['can_view'] 	= $this->can_view();
		$data['can_edit'] 	= $this->can_view();
				
		$this->open('km'); 
		$data['alert'] = $this->alert($this->session->flashdata('alert'));
		
		$data['km'] = $this->mdl_km->get_data_where($id); 
		$data['detail'] = $this->mdl_km->get_data_detail($id); 
		
		$data['bidang'] = $this->mdl_bidang->get_data();
		$data['tahun'] = $this->mdl_tahun->get_data();
		$data['periode'] = $this->db->get_where('periode',array('tahun_id'=>$data['km']->row_array()['tahun_id'])); 
		$data['bulan']  = $this->db->get_where('periode_bulan',array('periode_id'=>$data['km']->row_array()['periode_id'])); 
		
		$data['nilai'] = [];  
		foreach($data['detail']->result() as $detail){ 
			$temp = get_object_vars($detail);
			$temp['periode_bulan_id1'] = '';
			$temp['nama_bulan1'] = '';
			$temp['target1'] = '';
			$temp['periode_bulan_id2'] = '';
			$temp['nama_bulan2'] = '';
			$temp['target2'] = '';
			$temp['periode_bulan_id3'] = '';
			$temp['nama_bulan3'] = '';
			$temp['target3'] = '';

			$index=0;
			foreach($data['bulan']->result() as $bulan){
				$index+=1;
				$value = $this->mdl_km->get_data_detail_nilai($detail->id, $bulan->id); 
				$temp['periode_bulan_id'.$index] = $bulan->id;
				$temp['nama_bulan'.$index] = $bulan->nama_bulan;
				$temp['target'.$index] = $value->row_array()['target'];  
			} 
			array_push($data['nilai'], $temp); 
		} 
		// print_r($data['nilai']);
		$this->load->view('km/km_copy', $data);
		$this->close();
	}


	function edit($id) 
	{
		if ($this->can_update() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
		
		$data['can_view'] 	= $this->can_view();
		$data['can_edit'] 	= $this->can_view();
				
		$this->open('km'); 
		$data['alert'] = $this->alert($this->session->flashdata('alert'));
		
		$data['km'] = $this->mdl_km->get_data_where($id); 
		$data['detail'] = $this->mdl_km->get_data_detail($id); 
		
		$data['bidang'] = $this->mdl_bidang->get_data();
		$data['tahun'] = $this->mdl_tahun->get_data();
		$data['periode'] = $this->db->get_where('periode',array('tahun_id'=>$data['km']->row_array()['tahun_id'])); 
		$data['bulan']  = $this->db->get_where('periode_bulan',array('periode_id'=>$data['km']->row_array()['periode_id'])); 
		
		$data['nilai'] = [];  
		foreach($data['detail']->result() as $detail){ 
			$temp = get_object_vars($detail);
			$temp['periode_bulan_id1'] = '';
			$temp['nama_bulan1'] = '';
			$temp['target1'] = '';
			$temp['periode_bulan_id2'] = '';
			$temp['nama_bulan2'] = '';
			$temp['target2'] = '';
			$temp['periode_bulan_id3'] = '';
			$temp['nama_bulan3'] = '';
			$temp['target3'] = '';

			$index=0;
			foreach($data['bulan']->result() as $bulan){
				$index+=1;
				$value = $this->mdl_km->get_data_detail_nilai($detail->id, $bulan->id); 
				$temp['periode_bulan_id'.$index] = $bulan->id;
				$temp['nama_bulan'.$index] = $bulan->nama_bulan;
				$temp['target'.$index] = $value->row_array()['target'];  
			} 
			array_push($data['nilai'], $temp); 
		} 
		// print_r($data['nilai']);
		$this->load->view('km/km_edit', $data);
		$this->close();
	}
	
	
	function update() 
	{
 
		if ($this->can_update() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
    
		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('bidang_id', 'bidang_id', 'required'); 
		$this->form_validation->set_rules('tahun_id', 'tahun_id', 'required'); 
		$this->form_validation->set_rules('periode_id', 'periode_id', 'required'); 
     
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('alert', "0,Field harus diisi!");	
			redirect_back(); 
		}
		else
		{	 

			$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
			$tz_object = new DateTimeZone($tz_string); 
			$datetime = new DateTime(); 
			$datetime->setTimezone($tz_object);  
			 
			$val = $this->input->post(); 
			// print_r($val);
			$val['km_detail'] = $val['km_detail']['km_detail'];  

			$data['id'] = $val['id'];  
			$data['bidang_id'] = $val['bidang_id'];  
			$data['tahun_id'] = $val['tahun_id'];  
			$data['periode_id'] = $val['periode_id'];      
			// $data['status'] = 'draft';   
			$data['mdate'] = $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
			$data['muid'] = get_instance()->session->userdata('user_id');  
			$this->mdl_km->update_km($data['id'], $data); 
			$this->mdl_km->delete_km_detail($data['id']); 
			$this->mdl_km->delete_km_nilai($data['id']); 
			$km_id = $data['id'];

			$parent0 = NULL;
			$parent1 = 0;
			$parent2 = 0; 
			$parent3 = 0; 
			foreach($val['km_detail'] as $row){

				$temp['km_id'] = $km_id;
				$temp['level'] = $row['level'];
				$temp['kpi'] = $row['kpi'];
				$temp['weight'] = $row['weight'];
				$temp['unit'] = $row['unit'];

				// print_r($temp);
				if($temp['level'] == 1){
					$temp['parent'] = $parent0;
					$km_detail_id = $this->mdl_km->insert_km_detail($temp); 
					$parent1 = $km_detail_id;
				}else if($temp['level'] == 2){
					$temp['parent'] = $parent1;
					$km_detail_id = $this->mdl_km->insert_km_detail($temp); 
					$parent2 = $km_detail_id;
				}else if($temp['level'] == 3){
					$temp['parent'] = $parent2;
					$km_detail_id = $this->mdl_km->insert_km_detail($temp); 
					$parent3 = $km_detail_id; 
				}else if($temp['level'] == 4){
					$temp['parent'] = $parent3;
					$km_detail_id = $this->mdl_km->insert_km_detail($temp);  
				}

				$temp1['km_id'] = $km_id;
				$temp1['km_detail_id'] = $km_detail_id;
				$temp1['periode_bulan_id'] = $val['bulan_id1'];
				$temp1['target'] = $row['target1']; 
				// print_r($temp1);
				$km_nilai1 = $this->mdl_km_nilai->insert($temp1);  

				$temp2['km_id'] = $km_id;
				$temp2['km_detail_id'] = $km_detail_id;
				$temp2['periode_bulan_id'] = $val['bulan_id2'];
				$temp2['target'] = $row['target2']; 
				// print_r($temp2);
				$km_nilai2 = $this->mdl_km_nilai->insert($temp2);  

				$temp3['km_id'] = $km_id;
				$temp3['km_detail_id'] = $km_detail_id;
				$temp3['periode_bulan_id'] = $val['bulan_id3'];
				$temp3['target'] = $row['target3']; 
				// print_r($temp3);
				$km_nilai3 = $this->mdl_km_nilai->insert($temp3);  
			}
			  
			$this->session->set_flashdata('alert', "1,Data berhasil ditambah.");
			redirect(site_url().'km');

		}
	}
	

	function delete($id) 
	{
		if ($this->can_delete() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
  
		$this->mdl_km->delete($id); 
		$this->session->set_flashdata('alert', "1,Data berhasil dihapus");		
		redirect(site_url().'km'); 
	}

			
	function active($id) 
	{ 
		$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
		$tz_object = new DateTimeZone($tz_string); 
		$datetime = new DateTime(); 
		$datetime->setTimezone($tz_object);  
		 
		$data['id'] 				= $id;
		$data['status'] 			= 'saved';
		$data['muid'] 				= get_instance()->session->userdata('user_id'); 
		$data['mdate'] 				= $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
		 
		$this->mdl_km->update_km($data['id'], $data);
		$this->session->set_flashdata('alert', "1,Data telah berhasil diaktifkan.");	
		redirect(site_url().'km'); 
	}

	function nonactive($id) 
	{
		$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
		$tz_object = new DateTimeZone($tz_string); 
		$datetime = new DateTime(); 
		$datetime->setTimezone($tz_object);   
		
		$data['id'] 				= $id;
		$data['status'] 			= 'draft';
		$data['muid'] 				= get_instance()->session->userdata('user_id'); 
		$data['mdate'] 				= $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
		 
		$this->mdl_km->update_km($data['id'], $data);
		$this->session->set_flashdata('alert', "1,Data telah berhasil dinonaktifkan.");	
		redirect(site_url().'km'); 
	}
	
	



}
