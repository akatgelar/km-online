<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller
{

	
	function __construct() 
	{
		parent::__construct();
		$this->load->model('mdl_auth', 'mdl_auth');
	}


	function index() 
	{
		# load view 
		$this->login();
	}


	function login() 
	{
 		
		if (is_login() == TRUE)
		{
			redirect(site_url().'home');
		}

		 
		# Data
		$data['username'] = $this->input->post('username');
		$data['password'] = $this->input->post('password');
		$data['message'] = '';

		# set rules validation
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');

		# set message validation
		$this->form_validation->set_message('required', 'Field Harus Diisi!');

		//cek submit
		if($this->input->post('submit')) 
		{
			//validasi field
			if ($this->form_validation->run() == FALSE)
			{		
				// validasi gagal
				$data['message'] = 'Username dan Password harus diisi'; 
				$this->load->view('auth/login', $data);
			}
			else
			{					 
				// validasi berhasil
				// cek user
				$result = $this->mdl_auth->get_user($data['username'], $data['password']);

				if ($result->num_rows() > 0)
				{  
					// login sukses
					# set token
					$token = md5($result->row()->id . time());
					$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
					$tz_object = new DateTimeZone($tz_string); 
						
					$datetime = new DateTime(); 
					$datetime->setTimezone($tz_object);  
					$datetime = $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
					
					$this->mdl_auth->set_token($result->row()->id, $token, $datetime);
					$this->session->set_userdata('token', $token);
 
					// set logged as to session 
					$user_id = $result->row()->id;
					$username = $result->row()->username;  
					$bidang_id = $result->row()->bidang_id;  
					$level = $this->mdl_auth->get_logged_level($result->row()->id); 
					$last_access = $this->mdl_auth->get_token($result->row()->id);
 
					$this->session->set_userdata('user_id', $user_id);  
					$this->session->set_userdata('bidang_id', $bidang_id); 
					$this->session->set_userdata('username', $username); 
					$this->session->set_userdata('userlevel', $level);
					$this->session->set_userdata('last_access', $last_access);
					$this->session->set_userdata('alert', '');

					redirect(site_url().'home');
				}
				else
				{
					// login gagal
					$data['message'] = 'Akses ditolak!';
					$this->load->view('auth/login', $data);

				}
			}
			 
		}
		else
		{
			$data['message'] = '';		
			$this->load->view('auth/login', $data);
		}
		
	}

	

	function logout()
	{

		$this->session->sess_destroy();
		$data['message'] = ' ';		
		redirect(site_url().'auth/login', $data);

	}



	function failed()
	{

		$data['message'] = '';		
		$this->load->view('auth/failed', $data);

	}

	

}

