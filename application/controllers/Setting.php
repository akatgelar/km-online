<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends My_Controller {


	function __construct() 
	{
		parent::__construct();
		$this->load->model('mdl_setting', 'mdl_setting');
	}


	public function index()
	{
		$data['can_access'] = $this->can_access();
		$data['can_view'] 	= $this->can_view();
		$data['can_insert'] = $this->can_insert();
		$data['can_update'] = $this->can_update();
		$data['can_delete'] = $this->can_delete();

		$this->open('setting'); 
		$data['alert'] = $this->alert($this->session->flashdata('alert'));
		$data['results'] = $this->mdl_setting->get_data();
		$this->load->view('setting/setting_list', $data);
		$this->close();
	}

	function edit($id) 
	{
		if ($this->can_update() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
		
		$data['can_view'] 	= $this->can_view();
		
		$this->open('setting'); 
		$data['alert'] = $this->alert($this->session->flashdata('alert'));
		$data['results'] = $this->mdl_setting->get_data_where($id);
		$this->load->view('setting/setting_edit', $data);
		$this->close();
	}
	
	
	function update() 
	{
 
		if ($this->can_update() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
    
		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('kode', 'kode', 'required');
		$this->form_validation->set_rules('value', 'value', 'required');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('alert', "0,Field harus diisi!");	
			redirect_back(); 
		}
		else
		{	
			$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
			$tz_object = new DateTimeZone($tz_string); 
			$datetime = new DateTime(); 
			$datetime->setTimezone($tz_object); 

			$data['id'] = $this->input->post('id'); 
			$data['kode'] = $this->input->post('kode');
			$data['value'] = strip_tags($this->input->post('value'));
			$data['keterangan'] = $this->input->post('keterangan');
			$data['mdate'] = $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
			$data['muid'] = get_instance()->session->userdata('setting_id');  
			$this->mdl_setting->update($data['id'], $data); 
		
			$this->session->set_flashdata('alert', "1,Data berhasil diperbaharui.");	
			redirect(site_url().'setting');

		}
	}
	

	function delete($id) 
	{
		if ($this->can_delete() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
  
		$this->mdl_setting->delete($id);
		$this->session->set_flashdata('alert', "1,Data berhasil dihapus");		
		redirect(site_url().'setting'); 
	}

			
	
	



}
