<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Km_nilai extends My_Controller {


	function __construct() 
	{
		parent::__construct();
		$this->load->model('mdl_km_nilai', 'mdl_km_nilai');  
		$this->load->model('mdl_periode', 'mdl_periode');  
		$this->load->model('mdl_periode_bulan', 'mdl_periode_bulan');  
		$this->load->model('mdl_bidang', 'mdl_bidang');  
	}


	public function index()
	{
		$data['can_access'] = $this->can_access();
		$data['can_view'] 	= $this->can_view();
		$data['can_insert'] = $this->can_insert();
		$data['can_update'] = $this->can_update();
		$data['can_delete'] = $this->can_delete();

		$this->open('km_nilai'); 
		$data['alert'] = $this->alert($this->session->flashdata('alert'));
		$data['results'] = $this->mdl_km_nilai->get_data();
		$this->load->view('km_nilai/km_nilai_list', $data);
		$this->close();
	}

	function preview($id) 
	{
	
		$data['can_access'] = $this->can_access();
		$data['can_view'] 	= $this->can_view();
		$data['can_insert'] = $this->can_insert();
		$data['can_update'] = $this->can_update();
		$data['can_delete'] = $this->can_delete(); 
		
		$this->open('km_nilai'); 
		$data['km'] = $this->mdl_km_nilai->get_data_where($id)->result_array(); 
		$data['detail'] = $this->mdl_km_nilai->get_data_detail($id);
		$data['periode'] = $this->mdl_km_nilai->get_data_periode($data['km'][0]['periode_id']);
		$data['periode2'] = [];
		foreach($data['periode']->result() as $per){
			$temp = $this->mdl_km_nilai->get_data_nilai($id, $per->id);
			$temp2['periode_bulan_id'] = $per->id;
			$temp2['nama_bulan'] = $per->nama_bulan;
			$temp2['nilai'] = $temp->result(); 
			array_push($data['periode2'], $temp2);
		} 
		$this->load->view('km_nilai/km_nilai_preview', $data);
		$this->close();
		 
	}

	function add()
	{
		if ($this->can_insert() == FALSE){
			redirect(site_url().'admin/login/failed'); 
		}
		
		$data['can_view'] 	= $this->can_view();

		$this->open('km_nilai');  
		$data['bidang'] = $this->mdl_bidang->get_data();
		$data['periode'] = $this->mdl_periode->get_data();
		$data['periode_bulan'] = $this->mdl_periode_bulan->get_data();
		$data['alert'] = $this->alert($this->session->flashdata('alert'));
		$this->load->view('km_nilai/km_nilai_add',$data); 
		$this->close();
	}
	

	function insert() 
	{
		if ($this->can_insert() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
      
		$this->form_validation->set_rules('kategori_id', 'kategori_id', 'required'); 
		$this->form_validation->set_rules('nama_pendek', 'nama_pendek', 'required'); 
		$this->form_validation->set_rules('nama_panjang', 'nama_panjang', 'required'); 

		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('alert', "0,Field harus diisi!");		
			redirect_back(); 
		}
		else
		{	 

			$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
			$tz_object = new DateTimeZone($tz_string); 
			$datetime = new DateTime(); 
			$datetime->setTimezone($tz_object);  
			 
			$data['kategori_id'] = $this->input->post('kategori_id');  
			$data['nama_pendek'] = $this->input->post('nama_pendek');  
			$data['nama_panjang'] = $this->input->post('nama_panjang');
			$data['keterangan'] = $this->input->post('keterangan');
			$data['cdate'] = $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
			$data['cuid'] = get_instance()->session->userdata('user_id');  
			$this->mdl_km_nilai->insert($data); 
			  
			$this->session->set_flashdata('alert', "1,Data berhasil ditambah.");
			redirect(site_url().'km_nilai');

		}
	}


	function edit($id) 
	{
		$data['can_access'] = $this->can_access();
		$data['can_view'] 	= $this->can_view();
		$data['can_insert'] = $this->can_insert();
		$data['can_update'] = $this->can_update();
		$data['can_delete'] = $this->can_delete(); 

		if ($this->can_update() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
		  
		// $this->open('km_nilai');  
		// $data['alert'] = $this->alert($this->session->flashdata('alert'));
		// $data['kategori'] = $this->mdl_km_nilai_kategori->get_data();
		// $data['results'] = $this->mdl_km_nilai->get_data_where($id);
		// $this->load->view('km_nilai/km_nilai_edit', $data);
		// $this->close();

		$this->open('km_nilai'); 
		$data['alert'] = $this->alert($this->session->flashdata('alert'));
		$data['km'] = $this->mdl_km_nilai->get_data_where($id)->result_array(); 
		$data['detail'] = $this->mdl_km_nilai->get_data_detail($id);
		$data['periode'] = $this->mdl_km_nilai->get_data_periode($data['km'][0]['periode_id']);
		$data['periode2'] = [];
		foreach($data['periode']->result() as $per){
			$temp = $this->mdl_km_nilai->get_data_nilai($id, $per->id);
			$temp2['periode_bulan_id'] = $per->id;
			$temp2['nama_bulan'] = $per->nama_bulan;
			$temp2['nilai'] = $temp->result(); 
			array_push($data['periode2'], $temp2);
		} 
		$this->load->view('km_nilai/km_nilai_edit', $data);
		$this->close();

	}
	
	
	function update() 
	{ 

		if ($this->can_update() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
    
		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('result', 'result', 'required'); 
		$this->form_validation->set_rules('score', 'score', 'required'); 
		$this->form_validation->set_rules('ach', 'ach', 'required'); 
     
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('alert', "0,Field harus diisi!");	
			redirect_back(); 
		}
		else
		{	 

			$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
			$tz_object = new DateTimeZone($tz_string); 
			$datetime = new DateTime(); 
			$datetime->setTimezone($tz_object);  
			 
			$data['id'] = $this->input->post('id');   
			$data['result'] = $this->input->post('result');  
			$data['score'] = $this->input->post('score');  
			$data['ach'] = $this->input->post('ach');   
			$data['mdate'] = $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
			$data['muid'] = get_instance()->session->userdata('user_id');  
			$this->mdl_km_nilai->update($data['id'], $data); 
			 
			$this->session->set_flashdata('alert', "1,Data berhasil diperbaharui.");	
			echo "sukses";
			// redirect(site_url().'km_nilai');

		}
	}
	

	function delete($id) 
	{
		if ($this->can_delete() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
  
		$this->mdl_km_nilai->delete($id);
		$this->session->set_flashdata('alert', "1,Data berhasil dihapus");		
		redirect(site_url().'km_nilai'); 
	}

			
	function active($id_artikel) 
	{ 
		$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
		$tz_object = new DateTimeZone($tz_string); 
		$datetime = new DateTime(); 
		$datetime->setTimezone($tz_object);  
		 
		$data['id'] 				= $id_artikel;
		$data['is_active'] 			= '1';
		$data['muid'] 				= get_instance()->session->userdata('user_id'); 
		$data['mdate'] 				= $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
		 
		$this->mdl_km_nilai->update($data['id'], $data);
		$this->session->set_flashdata('alert', "1,Data telah berhasil diaktifkan.");	
		redirect(site_url().'km_nilai'); 
	}

	function nonactive($id_artikel) 
	{
		$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
		$tz_object = new DateTimeZone($tz_string); 
		$datetime = new DateTime(); 
		$datetime->setTimezone($tz_object);   
		
		$data['id'] 				= $id_artikel;
		$data['is_active'] 			= '0';
		$data['muid'] 				= get_instance()->session->userdata('user_id'); 
		$data['mdate'] 				= $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
		 
		$this->mdl_km_nilai->update($data['id'], $data);
		$this->session->set_flashdata('alert', "1,Data telah berhasil dinonaktifkan.");	
		redirect(site_url().'km_nilai'); 
	}
	
	



}
