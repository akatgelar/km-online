<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periode extends My_Controller {


	function __construct() 
	{
		parent::__construct();
		$this->load->model('mdl_periode', 'mdl_periode'); 
		$this->load->model('mdl_tahun', 'mdl_tahun'); 
	}


	public function index()
	{
		$data['can_access'] = $this->can_access();
		$data['can_view'] 	= $this->can_view();
		$data['can_insert'] = $this->can_insert();
		$data['can_update'] = $this->can_update();
		$data['can_delete'] = $this->can_delete();

		$this->open('periode'); 
		$data['alert'] = $this->alert($this->session->flashdata('alert'));
		$data['results'] = $this->mdl_periode->get_data();
		$this->load->view('periode/periode_list', $data);
		$this->close();
	}

	function preview($id) 
	{
	
		$data['can_access'] = $this->can_access();
		$data['can_view'] 	= $this->can_view();
		$data['can_insert'] = $this->can_insert();
		$data['can_update'] = $this->can_update();
		$data['can_delete'] = $this->can_delete(); 
		
		$this->open('periode'); 
		$data['results'] = $this->mdl_periode->get_data_where($id);
		$this->load->view('periode/periode_preview', $data);
		$this->close();
		 
	}

	function add()
	{
		if ($this->can_insert() == FALSE){
			redirect(site_url().'admin/login/failed'); 
		}
		
		$data['can_view'] 	= $this->can_view();

		$this->open('periode');  
		$data['tahun'] = $this->mdl_tahun->get_data();
		$data['alert'] = $this->alert($this->session->flashdata('alert'));
		$this->load->view('periode/periode_add',$data); 
		$this->close();
	}
	

	function insert() 
	{
		if ($this->can_insert() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
      
		$this->form_validation->set_rules('tahun_id', 'tahun_id', 'required'); 
		$this->form_validation->set_rules('nama_periode', 'nama_periode', 'required');  

		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('alert', "0,Field harus diisi!");		
			redirect_back(); 
		}
		else
		{	 

			$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
			$tz_object = new DateTimeZone($tz_string); 
			$datetime = new DateTime(); 
			$datetime->setTimezone($tz_object);  
			 
			$data['tahun_id'] = $this->input->post('tahun_id');  
			$data['nama_periode'] = $this->input->post('nama_periode');   
			$data['keterangan'] = $this->input->post('keterangan');
			$data['cdate'] = $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
			$data['cuid'] = get_instance()->session->userdata('user_id');  
			$this->mdl_periode->insert($data); 
			  
			$this->session->set_flashdata('alert', "1,Data berhasil ditambah.");
			redirect(site_url().'periode');

		}
	}


	function edit($id) 
	{
		if ($this->can_update() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
		
		$data['can_view'] 	= $this->can_view();
		
		$this->open('periode');  
		$data['alert'] = $this->alert($this->session->flashdata('alert'));
		$data['tahun'] = $this->mdl_tahun->get_data();
		$data['results'] = $this->mdl_periode->get_data_where($id);
		$this->load->view('periode/periode_edit', $data);
		$this->close();
	}
	
	
	function update() 
	{
 
		if ($this->can_update() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
    
		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('tahun_id', 'tahun_id', 'required'); 
		$this->form_validation->set_rules('nama_periode', 'nama_periode', 'required');  
     
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('alert', "0,Field harus diisi!");	
			redirect_back(); 
		}
		else
		{	 

			$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
			$tz_object = new DateTimeZone($tz_string); 
			$datetime = new DateTime(); 
			$datetime->setTimezone($tz_object);  
			 
			$data['id'] = $this->input->post('id');   
			$data['tahun_id'] = $this->input->post('tahun_id');  
			$data['nama_periode'] = $this->input->post('nama_periode');   
			$data['keterangan'] = $this->input->post('keterangan');
			$data['mdate'] = $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
			$data['muid'] = get_instance()->session->userdata('user_id');  
			$this->mdl_periode->update($data['id'], $data); 
			 
			$this->session->set_flashdata('alert', "1,Data berhasil diperbaharui.");	
			redirect(site_url().'periode');

		}
	}
	

	function delete($id) 
	{
		if ($this->can_delete() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
  
		$this->mdl_periode->delete($id);
		$this->session->set_flashdata('alert', "1,Data berhasil dihapus");		
		redirect(site_url().'periode'); 
	}

			
	function active($id_artikel) 
	{ 
		$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
		$tz_object = new DateTimeZone($tz_string); 
		$datetime = new DateTime(); 
		$datetime->setTimezone($tz_object);  
		 
		$data['id'] 				= $id_artikel;
		$data['is_active'] 			= '1';
		$data['muid'] 				= get_instance()->session->userdata('user_id'); 
		$data['mdate'] 				= $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
		 
		$this->mdl_periode->update($data['id'], $data);
		$this->session->set_flashdata('alert', "1,Data telah berhasil diaktifkan.");	
		redirect(site_url().'periode'); 
	}

	function nonactive($id_artikel) 
	{
		$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
		$tz_object = new DateTimeZone($tz_string); 
		$datetime = new DateTime(); 
		$datetime->setTimezone($tz_object);   
		
		$data['id'] 				= $id_artikel;
		$data['is_active'] 			= '0';
		$data['muid'] 				= get_instance()->session->userdata('user_id'); 
		$data['mdate'] 				= $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
		 
		$this->mdl_periode->update($data['id'], $data);
		$this->session->set_flashdata('alert', "1,Data telah berhasil dinonaktifkan.");	
		redirect(site_url().'periode'); 
	}
	
	



}
