<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evidence extends My_Controller {


	function __construct() 
	{
		parent::__construct();
		$this->load->model('mdl_evidence', 'mdl_evidence'); 
	}


	public function index()
	{
		$data['can_access'] = $this->can_access();
		$data['can_view'] 	= $this->can_view();
		$data['can_insert'] = $this->can_insert();
		$data['can_update'] = $this->can_update();
		$data['can_delete'] = $this->can_delete();

		$this->open('evidence');  
		$data['alert'] = $this->alert($this->session->flashdata('alert'));
		$data['results'] = $this->mdl_evidence->get_data();
		$this->load->view('evidence/evidence_list', $data);
		$this->close();
	}

	function preview($id) 
	{
	
		$data['can_access'] = $this->can_access();
		$data['can_view'] 	= $this->can_view();
		$data['can_insert'] = $this->can_insert();
		$data['can_update'] = $this->can_update();
		$data['can_delete'] = $this->can_delete(); 
		
		$this->open('evidence');  
		$data['results'] = $this->mdl_evidence->get_data_where($id);
		$this->load->view('evidence/evidence_preview', $data);
		$this->close();
		 
	}
 

	function insert() 
	{
		if ($this->can_insert() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
        
		$this->form_validation->set_rules('upload_nama', 'upload_nama', 'required'); 
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('alert', "0,Field harus diisi!");		
			redirect_back(); 
		}
		else
		{	
			// $config['upload_path'] =  $_SERVER['DOCUMENT_ROOT'].'/telkom-km-online/assets/upload/evidence/';
			$config['upload_path'] =  '/var/www/html/assets/upload/evidence/';
			$config['allowed_types'] = '*';
			$config['max_size']  = '100000'; 
			$this->load->library('upload', $config);

			$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
			$tz_object = new DateTimeZone($tz_string); 
			$datetime = new DateTime(); 
			$datetime->setTimezone($tz_object);  

			// print_r($this->input->post());
			// print_r($this->upload->do_upload());

			$this->db->select('evidence.*');
			$this->db->from('evidence');   
			$this->db->where('evidence.km_id', $this->input->post('upload_km_id'));   
			$this->db->where('evidence.km_detail_id', $this->input->post('upload_km_detail_id'));   
			$ada = $this->db->get()->result_array();
			// print(count($ada));
			

			if ( !$this->upload->do_upload())
			{
				// echo "gagal upload";
				$data['km_id'] = $this->input->post('upload_km_id');  
				$data['km_detail_id'] = $this->input->post('upload_km_detail_id');  
				$data['nama'] = $this->input->post('upload_nama');
				$data['file_type'] = $this->input->post('upload_file_type');
				$data['cdate'] = $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
				$data['cuid'] = get_instance()->session->userdata('user_id');  

				if(count($ada) > 0){ 
					$this->mdl_evidence->update($data['km_id'], $data['km_detail_id'], $data); 
				}else{ 
					$this->mdl_evidence->insert($data); 
				}
				
			} 
			else
			{	
				// echo "berhasil upload";
				$upload_data = $this->upload->data(); 
				$nama_lama =  $upload_data['file_name'];
				$nama_baru =  'evidence_'. date("Y.m.d") . '-' . date("H.i.s") .'-'.  $upload_data['file_ext'];
				//upload gede
				rename($config['upload_path'] . $nama_lama, $config['upload_path'] . $nama_baru);
  
				$data['file_path'] = $nama_baru;
				$data['km_id'] = $this->input->post('upload_km_id');  
				$data['km_detail_id'] = $this->input->post('upload_km_detail_id');  
				$data['nama'] = $this->input->post('upload_nama');
				$data['file_type'] = $this->input->post('upload_file_type');
				$data['cdate'] = $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
				$data['cuid'] = get_instance()->session->userdata('user_id');  
				
				if(count($ada) > 0){ 
					$this->mdl_evidence->update($data['km_id'], $data['km_detail_id'], $data); 
				}else{ 
					$this->mdl_evidence->insert($data); 
				}
			}

			$this->session->set_flashdata('alert', "1,Data berhasil ditambah.");
			// redirect(site_url().'evidence');
			redirect_back(); 

		}
	}

 

	function delete($id) 
	{
		if ($this->can_delete() == FALSE){
			redirect(site_url().'admin/login/failed');
		}
  
		$this->mdl_evidence->delete($id);
		$this->session->set_flashdata('alert', "1,Data berhasil dihapus");		
		redirect(site_url().'evidence'); 
	}

			 



}
