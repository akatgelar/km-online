                                      
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">

                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- <div class="page-header">
                                        <div class="page-header-title">
                                            <h4>user</h4>
                                        </div>
                                        <div class="page-header-breadcrumb">
                                            <ul class="breadcrumb-title">
                                                <li class="breadcrumb-item">
                                                    <a href="<?php echo site_url();?>">
                                                        <i class="icofont icofont-home"></i>
                                                    </a>
                                                </li>
                                                <li class="breadcrumb-item"><a href="#!">user</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div> -->
                                            
						            <?php if($alert){echo $alert;}?>

                                    <?php $row = $results->row_array();?>	
                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                    
                                                <div class="card">
                                                    <?php
                                                        $attributes = array('name' => 'user', 'type'=>'post', 'id' => 'user', 'class'=>'block-content form');
                                                        echo form_open_multipart('profile/update_foto', $attributes);
                                                    ?> 
                                                    <input type="hidden" id="id"  name="id" placeholder="id" value="<?php echo $row['id'];?>"> 
                                                        
                                                    <div class="card-header contact-user center">
                                                        <img class="img-circle" id="img" name="img" width="300px" height="300px" src="<?php echo str_replace('backend','frontend',base_url());?>assets/upload/profile/<?php echo $row['foto']; ?>" alt="contact-user">
                                                    </div>
                                                    <div class="card-block">
                                                        <input type="file" id="userfile" name="userfile" class="form-control" placeholder="Gambar"  value="<?php echo $row['foto'];?>" onchange="document.getElementById('img').src = window.URL.createObjectURL(this.files[0])"/>    
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <button class="btn btn-primary" id="send"  name="send" type="submit" onclick="javascript:kirim();">
                                                            <i class="fa fa-save"></i>
                                                            Upload Foto
                                                        </button>
                                                        <button class="btn btn-danger" type="button" onclick="javascript:batal();">
                                                            <i class="fa fa-times"></i>
                                                            Batal
                                                        </button>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <br>
                                                    </div>
                                                    </form> 
                                                </div>
                                            </div>

                                            
                                            <div class="col-sm-8">

                                                <div class="card">
                                                    <div class="card-block">
                                                        <?php
                                                            $attributes = array('name' => 'user', 'type'=>'post', 'id' => 'user', 'class'=>'block-content form');
                                                            echo form_open_multipart('profile/update_password', $attributes);
                                                        ?> 
                                                        <input type="hidden" id="id"  name="id" placeholder="id" value="<?php echo $row['id'];?>"> 
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label">Level</label>
                                                            <div class="col-sm-10">
                                                                <select name="level_id" id="level_id" class="form-control" disabled="true">
                                                                    <option value="">-- Pilih Level --</option>
                                                                    <?php $i=0; foreach($level->result() as $rows) {$i++;?>
                                                                    <option value="<?php echo $rows->id;?>" <?php if($row['level_id'] == $rows->id){echo "selected ";}?>><?php echo $rows->nama;?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label">Username</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" id="username" name="username" class="form-control" placeholder="Username" value="<?php echo $row['username'];?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label">Password</label>
                                                            <div class="col-sm-10">
                                                                <input type="password" id="password" name="password" class="form-control" placeholder="Password" value="<?php echo $row['password_plan'];?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <br>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label"></label>
                                                            <div class="col-sm-10" style="text-align: right;">
                                                                <button class="btn btn-primary" id="send"  name="send" type="submit" onclick="javascript:kirim();">
                                                                    <i class="fa fa-save"></i>
                                                                    Simpan
                                                                </button>
                                                                <button class="btn btn-danger" type="button" onclick="javascript:batal();">
                                                                    <i class="fa fa-times"></i>
                                                                    Batal
                                                                </button>
                                                            </div>
                                                        </div>
                                                        </form> 
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript" src="<?php echo base_url();?>assets/assets/js/jquery.colorbox-min.js"></script>
                    
                    <script type="text/javascript">
                        $(function() {
                            var colorbox_params = {
                                reposition:true,
                                scalePhotos:true,
                                scrolling:false,
                                previous:'<i class="icon-arrow-left"></i>',
                                next:'<i class="icon-arrow-right"></i>',
                                close:'<i class="icon-remove"></i>',
                                current:'{current} of {total}',
                                maxWidth:'100%',
                                maxHeight:'100%',
                                onOpen:function(){
                                    document.body.style.overflow = 'hidden';
                                },
                                onClosed:function(){
                                    document.body.style.overflow = 'auto';
                                },
                                onComplete:function(){
                                    $.colorbox.resize();
                                }
                            };

                            $('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
				            $("#cboxLoadingGraphic").append("<i class='icon-spinner orange'></i>");//let's add a custom loading icon

                        });
                        
                        function add(){
                            //editor1.innerHTML = "My new text!";
                            document.location.href = '<?php echo site_url() . "user/add/";?>';
                        }
                        
                    </script>	