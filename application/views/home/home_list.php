                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">

                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- <div class="page-header">
                                        <div class="page-header-title">
                                            <h4>Home</h4>
                                        </div>
                                        <div class="page-header-breadcrumb">
                                            <ul class="breadcrumb-title">
                                                <li class="breadcrumb-item">
                                                    <a href="<?php echo site_url();?>">
                                                        <i class="icofont icofont-home"></i>
                                                    </a>
                                                </li>
                                                <li class="breadcrumb-item"><a href="#!">Home</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div> -->

                                    <div class="page-body">
                                        <div class="row"> 
                                            
                                            <?php $i=0; foreach($results as $row) {$i++;?>
                                                <div class="col-lg-12">
                                                    <div class="card" style="background-color: transparent;">
                                                        <div class="card-header">
                                                            <div class="card-header-left">
                                                                <h5><?php echo $row['nama_pendek'];?> - <?php echo $row['nama_panjang'];?></h5>
                                                            </div>
                                                            <div class="card-header-right">
                                                                <!-- <i class="icofont icofont-rounded-down"></i> -->
                                                                <!-- <i class="icofont icofont-refresh"></i> -->
                                                                <!-- <i class="icofont icofont-close-circled"></i> -->
                                                            </div>
                                                        </div>
                                                        <div class="card-block center"> 
                                                            <div class="row"> 
                                                            
                                                                <?php $j=0; foreach($row['bidang'] as $row2) {$j++;?>
                                                                    <div class="col-lg-4">
                                                                        <div class="card">
                                                                            <div class="card-header" style="text-align: left; min-height: 100px;"> 
                                                                                <h5><?php echo $row2->nama_pendek;?></h5>
                                                                                <span><b><?php echo $row2->nama_panjang;?></b></span> 
                                                                            </div>
                                                                            <div class="card-block center"> 
                                                                                <h3 style="margin: 0; position: absolute; top: 55%; left: 50%; transform: translate(-50%, -50%); ">Nilai KM</h3>
                                                                                <canvas id="pieChart<?php echo $row2->id;?>" width="400" height="400"></canvas> 
                                                                                <script>
                                                                                    $(document).ready(function(){
                                                                                        /*Pie chart*/ 
                                                                                        var pieElem = document.getElementById("pieChart<?php echo $row2->id;?>");
                                                                                        var random = Math.floor(Math.random() * 100); 

                                                                                        var myPieChart = new Chart(pieElem, {
                                                                                            type: 'doughnut',
                                                                                            data: { 
                                                                                                labels: [
                                                                                                    'Selesai', 'Belum Selesai'
                                                                                                ], 
                                                                                                datasets: [{
                                                                                                    data: [random, 100-random], 
                                                                                                    backgroundColor: [
                                                                                                    '#A2A0FB',
                                                                                                    '#E6E6E6', 
                                                                                                    ], 
                                                                                                }]
                                                                                            },
                                                                                            options: {
                                                                                                cutoutPercentage: 80,
                                                                                                title: {
                                                                                                    display: false,
                                                                                                    text: 'Nilai KM',
                                                                                                    position: 'bottom'
                                                                                                },
                                                                                                legend: {
                                                                                                    display: false, 
                                                                                                },
                                                                                                tooltips: {
                                                                                                    enabled: true,
                                                                                                    callbacks: {
                                                                                                    label: function(tooltipItem, data) {
                                                                                                        return data['labels'][tooltipItem['index']] + ': ' + data['datasets'][0]['data'][tooltipItem['index']] + '%';
                                                                                                    }
                                                                                                    } 
                                                                                                },
                                                                                                plugins: {
                                                                                                    labels: {
                                                                                                    render: 'percentage',
                                                                                                    fontColor: ['#ffffff', ''],
                                                                                                    precision: 2
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        });
                                                                                    });
                                                                                </script>
                                                                            </div>
                                                                            <div class="card-block left">
                                                                                <a href="#">View full report</a>
                                                                            </div> 
                                                                        </div>
                                                                    </div> 
                                                                <?php } ?>

                                                            </div>
                                                        </div> 
                                                    </div>
                                                </div> 
                                            <?php } ?>
											 
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- <div id="styleSelector"></div>
                            </div> -->
                        </div>
                    </div>

                    <canvas id="pieChart" width="400" height="400"></canvas> 
                    
                    <!-- Chart js -->
                    <script type="text/javascript" src="<?php echo base_url();?>assets/bower_components/chart.js/js/Chart.js"></script>
                    
                    