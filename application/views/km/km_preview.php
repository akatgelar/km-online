                                        
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">

                            <div class="main-body">
                                <div class="page-wrapper">
                                            

                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5>Kelola Data KM</h5>
                                                        <span>Di halaman ini, Anda dapat mengelola data bidang yang ada di website Anda.</span>
                                                        <div class="card-header-right">
                                                            <i class="icofont icofont-rounded-down"></i>
                                                            <i class="icofont icofont-refresh"></i>
                                                            <i class="icofont icofont-close-circled"></i>
                                                        </div>
                                                    </div>
                                                    <div class="card-block">
														<table class="table table-bordered nowrap" style="font-size: smaller;">
															<thead>
																<tr>
																	<th width="5%" style="background-color: #000; color: #fff; vertical-align: middle;" rowspan="2">No</th>
																	<th style="background-color: #000; color: #fff; vertical-align: middle;" rowspan="2" colspan="2">KPI</th>
																	<th width="10%" style="background-color: #1F4E78; color: #fff; vertical-align: middle;" rowspan="2">Weight</th>
																	<th width="10%" style="background-color: #1F4E78; color: #fff; vertical-align: middle;" rowspan="2">Unit</th> 
																	<th width="30%" style="background-color: #9BC2E6; color: #000; text-align: center;" colspan="3">Target</th>  
																</tr>
																<tr>
																	<?php foreach($periode->result() as $row) { ?>
																		<th width="10%" style="background-color: #9BC2E6; color: #000; text-align: center;"><?php echo $row->nama_bulan;?></th> 
																	<?php } ?>  
																</tr>
															</thead>
															<tbody>
																<?php $level1=0; $level2=0; $level3=0; $level4=0;?>
																<?php foreach($detail->result() as $row) { ?>
																	<?php if($row->level == 1) { $level1++;?>	 
																	<tr>
																		<td style="background-color: #C00000; color: #fff;"><?php echo integerToRoman($level1); ?></th>
																		<td style="background-color: #C00000; color: #fff;" colspan="2"><?php echo $row->kpi; ?></td>
																		<td style="background-color: #C00000; color: #fff;"><?php echo $row->weight; ?></td>
																		<td style="background-color: #C00000; color: #fff;"><?php echo $row->unit; ?></td>
																		<?php foreach($periode2 as $row2) { ?> 
																			<?php if(count($row2['nilai']) > 0) { ?> 
																				<!-- <?php foreach($row2['nilai'] as $row3) { ?> -->
																					<?php if($row->id == $row3->km_detail_id) { ?> 
																						<td style="background-color: #C00000; color: #fff;"><?php echo $row3->target; ?></td>
																						<!-- <td style="background-color: #C00000; color: #fff;"><?php echo $row3->result; ?></td> 
																						<td style="background-color: #C00000; color: #fff;">.</td> 
																						<td style="background-color: #C00000; color: #fff;">.</td> -->
																					<?php } ?>  
																				<!-- <?php } ?>   -->
																			<?php } else {?>  
																				<td style="background-color: #C00000; color: #fff;">.</td>
																				<!-- <td style="background-color: #C00000; color: #fff;">.</td> 
																				<td style="background-color: #C00000; color: #fff;">.</td> 
																				<td style="background-color: #C00000; color: #fff;">.</td> -->
																			<?php } ?>   
																		<?php } ?> 
																	</tr>  		 
																	<?php } ?>  
																	<?php if($row->level == 2) { $level2++; $level3=0;?>	 
																	<tr>
																		<td><?php echo $level2; ?></th>
																		<td width="200" colspan="2" style="white-space: pre-wrap;"><?php echo $row->kpi; ?></td>
																		<td><?php echo $row->weight; ?></td>
																		<td><?php echo $row->unit; ?></td>
																		<?php foreach($periode2 as $row2) { ?> 
																			<?php if(count($row2['nilai']) > 0) { ?> 
																				<!-- <?php foreach($row2['nilai'] as $row3) { ?> -->
																					<?php if($row->id == $row3->km_detail_id) { ?> 
																						<td><?php echo $row3->target; ?></td>
																						<!-- <td><?php echo $row3->result; ?></td> 
																						<td>.</td> 
																						<td>.</td> -->
																					<?php } ?>  
																				<!-- <?php } ?>   -->
																			<?php } else {?>  
																				<td>.</td>
																				<!-- <td>.</td>
																				<td>.</td>
																				<td>.</td>  -->
																			<?php } ?>    
																		<?php } ?> 
																	</tr>  		 
																	<?php } ?>  
																	<?php if($row->level == 3) { $level3++; $level4=0;?>	 
																	<tr>
																		<td></th>
																		<td><?php echo integerToAlphabet($level3-1); ?>.</td>
																		<td style="white-space: pre-wrap;"><?php echo $row->kpi; ?></td>
																		<td><?php echo $row->weight; ?></td>
																		<td><?php echo $row->unit; ?></td>
																		<?php foreach($periode2 as $row2) { ?> 
																			<?php if(count($row2['nilai']) > 0) { ?> 
																				<!-- <?php foreach($row2['nilai'] as $row3) { ?> -->
																					<?php if($row->id == $row3->km_detail_id) { ?> 
																						<td><?php echo $row3->target; ?></td>
																						<!-- <td><?php echo $row3->result; ?></td> 
																						<td>.</td> 
																						<td>.</td> -->
																					<?php } ?>  
																				<!-- <?php } ?>   -->
																			<?php } else {?>  
																				<td>.</td>
																				<!-- <td>.</td>
																				<td>.</td>
																				<td>.</td>  -->
																			<?php } ?>    
																		<?php } ?> 
																	</tr>  		 
																	<?php } ?>  
																	<?php if($row->level == 4) { $level4++;?>	 
																	<tr>
																		<td></th>
																		<td width="20"></td>
																		<td width="250" style="white-space: pre-wrap;"><?php echo $level4; ?>. <?php echo $row->kpi; ?></td>
																		<td><?php echo $row->weight; ?></td>
																		<td><?php echo $row->unit; ?></td>
																		<?php foreach($periode2 as $row2) { ?> 
																			<!-- <?php if(count($row2['nilai']) > 0) { ?>  -->
																				<?php foreach($row2['nilai'] as $row3) { ?>
																					<?php if($row->id == $row3->km_detail_id) { ?> 
																						<td><?php echo $row3->target; ?></td>
																						<!-- <td><?php echo $row3->result; ?></td> 
																						<td>.</td> 
																						<td>.</td> -->
																					<?php } ?>  
																				<!-- <?php } ?>   -->
																			<?php } else {?>  
																				<td>.</td>
																				<!-- <td>.</td>
																				<td>.</td>
																				<td>.</td>  -->
																			<?php } ?>    
																		<?php } ?> 
																	</tr>  		 
																	<?php } ?>  
																<?php } ?>
															</tbody>
														</table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    
