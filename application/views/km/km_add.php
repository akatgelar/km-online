                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">

                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- <div class="page-header">
                                        <div class="page-header-title">
                                            <h4>km</h4>
                                        </div>
                                        <div class="page-header-breadcrumb">
                                            <ul class="breadcrumb-title">
                                                <li class="breadcrumb-item">
                                                    <a href="<?php echo site_url();?>">
                                                        <i class="icofont icofont-home"></i>
                                                    </a>
                                                </li>
                                                <li class="breadcrumb-item"><a href="#!">km</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div> -->
                                            
						            <?php if($alert){echo $alert;}?>

                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5>Kelola Data KM</h5>
                                                        <span>Di halaman ini, Anda dapat mengelola data km yang ada di website Anda.</span>
                                                        <div class="card-header-right">
                                                            <i class="icofont icofont-rounded-down"></i>
                                                            <i class="icofont icofont-refresh"></i>
                                                            <i class="icofont icofont-close-circled"></i>
                                                        </div>
                                                    </div>
                                                    <div class="card-block">
                                                    
                                                    <?php
                                                        $attributes = array('name' => 'km', 'type'=>'post', 'id' => 'km', 'class'=>'block-content form');
                                                        echo form_open_multipart('km/insert', $attributes);
                                                    ?> 
                                                        
                                                        <div class="form-group row" style="display:none">
                                                            <label class="col-sm-2 col-form-label">ID</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" id="id"  name="id" class="form-control" placeholder="id"/>  
                                                                <span class="messages"></span>
                                                            </div>
                                                        </div> 
 
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label">Bidang *</label>
                                                            <div class="col-sm-10"> 
                                                                <select name="bidang_id" id="bidang_id" class="form-control">
                                                                    <option value="">-- Pilih Bidang --</option>
                                                                    <?php $i=0; foreach($bidang->result() as $row) {$i++;?>
                                                                    <option value="<?php echo $row->id;?>"><?php echo $row->nama_pendek;?> - <?php echo $row->nama_panjang;?></option>
                                                                    <?php } ?>
                                                                </select>
                                                                <span class="messages"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label">Tahun *</label>
                                                            <div class="col-sm-10"> 
                                                                <select name="tahun_id" id="tahun_id" class="form-control" onchange="loadPeriode()">
                                                                    <option value="">-- Pilih Tahun --</option>
                                                                    <?php $i=0; foreach($tahun->result() as $row) {$i++;?>
                                                                    <option value="<?php echo $row->id;?>"><?php echo $row->tahun;?></option>
                                                                    <?php } ?>
                                                                </select>
                                                                <span class="messages"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label">Periode *</label>
                                                            <div class="col-sm-10" id="periodeArea">  
                                                                <select name="periode_id" id="periode_id" class="form-control" onchange="loadBulan()">
                                                                    <option value="">-- Pilih Periode --</option> 
                                                                </select> 
                                                                <span class="messages"></span>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="bulan_id1" id="bulan_id1" placeholder="Target" class="form-control">
                                                        <input type="hidden" name="bulan_id2" id="bulan_id2" placeholder="Target" class="form-control">
                                                        <input type="hidden" name="bulan_id3" id="bulan_id3" placeholder="Target" class="form-control">
                                                                            
                                                        
                                                        <div class="dt-responsive table-responsive">
                                                            <table class="table table-striped table-bordered nowrap">
                                                                <thead>
                                                                    <tr id="judul">
                                                                        <th width="20%" style="background-color: #000; color: #fff; vertical-align: middle;">Level</th>
                                                                        <th style="background-color: #000; color: #fff; vertical-align: middle;">KPI</th>
                                                                        <th width="10%" style="background-color: #1F4E78; color: #fff; vertical-align: middle;">Weight</th>
                                                                        <th width="10%" style="background-color: #1F4E78; color: #fff; vertical-align: middle;">Unit</th> 
                                                                        <th width="10%" style="background-color: #9BC2E6; color: #000; vertical-align: middle;" id="bulan1"></th> 
                                                                        <th width="10%" style="background-color: #9BC2E6; color: #000; vertical-align: middle;" id="bulan2"></th> 
                                                                        <th width="10%" style="background-color: #9BC2E6; color: #000; vertical-align: middle;" id="bulan3"></th> 
                                                                    </tr>
                                                                </thead>
                                                                <tbody> 
                                                                    <tr id="km_detail">
                                                                        <td>
                                                                            <select name="level" id="level" class="form-control">
                                                                                <option value="">-- Pilih Level --</option>
                                                                                <option value="1">Level 1</option>
                                                                                <option value="2">Level 2</option>
                                                                                <option value="3">Level 3</option>
                                                                                <option value="4">Level 4</option> 
                                                                            </select>
                                                                            <span class="messages"></span>
                                                                        </td> 
                                                                        <td>
                                                                            <input type="text" name="kpi" id="kpi" placeholder="KPI" class="form-control">
                                                                            <span class="messages"></span>
                                                                        </td> 
                                                                        <td>
                                                                            <input type="text" name="weight" id="weight" placeholder="Weight" class="form-control">
                                                                            <span class="messages"></span>
                                                                        </td> 
                                                                        <td>
                                                                            <input type="text" name="unit" id="unit" placeholder="Unit" class="form-control">
                                                                            <span class="messages"></span>
                                                                        </td>  
                                                                        <td>
                                                                            <input type="text" name="target1" id="target1" placeholder="Target" class="form-control">
                                                                            <span class="messages"></span>
                                                                        </td>  
                                                                        <td>
                                                                            <input type="text" name="target2" id="target2" placeholder="Target" class="form-control">
                                                                            <span class="messages"></span>
                                                                        </td>  
                                                                        <td>
                                                                            <input type="text" name="target3" id="target3" placeholder="Target" class="form-control">
                                                                            <span class="messages"></span>
                                                                        </td>  
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            
                                                            <a href="javascript:void(0)" class="btn btn-mini btn-success" id="plus5">Add More</a>
                                                            <a href="javascript:void(0)" class="btn btn-mini btn-warning" id="minus5">Remove</a>
                                                        </div>
  
                                                        <br>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label"></label>
                                                            <div class="col-sm-10" style="text-align: right;">
                                                                <button class="btn btn-primary" id="send"  name="send" type="button" onclick="javascript:kirim();">
                                                                    <i class="fa fa-save"></i>
                                                                    Simpan
                                                                </button>
                                                                <button class="btn btn-danger" type="button" onclick="javascript:batal();">
                                                                    <i class="fa fa-times"></i>
                                                                    Batal
                                                                </button>
                                                            </div>
                                                        </div>
                                                        
                                                    </form> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script src="<?php echo base_url();?>assets/assets/js/dynamic-form.js"></script>

                    <script>
                        $(document).ready(function() {
                            var km_detail =  $("#km_detail").dynamicForm("#km_detail","#plus5", "#minus5", {
                                limit:100,
                                formPrefix : "km_detail",
                                normalizeFullForm : false
                            });

                            // km_detail.inject([{p_name: 'Hemant',quantity: '123',remarks: 'testing remark'},{p_name: 'Harshal',quantity: '123',remarks: 'testing remark'}]);

                            $("#km_detail #minus5").on('click', function(){
                                var initDynamicId = $(this).closest('#km_detail').parent().find("[id^='km_detail']").length;
                                if (initDynamicId === 2) {
                                    $(this).closest('#km_detail').next().find('#minus5').hide();
                                }
                                $(this).closest('#km_detail').remove();
                            });
  
                            $('form').on('submit', function(event){
                                var values = {};
                                $.each($('form').serializeArray(), function(i, field) {
                                    values[field.name] = field.value;
                                });
                                console.log(values)
                                // console.log(values.km_detail.length())
                                console.log(values['bidang_id'])
                                console.log(values['km_detail'])
                                // event.preventDefault();
                            })
                        });
                    </script>
                    
                    <script type="text/javascript">
	
                        function batal(){
                            document.location.href = '<?php echo site_url().'km'?>';
                        }
                        
                        function kirim(){  
                            console.log('succes');
                            document.getElementById("km").submit();// Form submission
                            // var values = {};
                            // $.each($('form').serializeArray(), function(i, field) {
                            //     values[field.name] = field.value;
                            // });
                            // console.log(values)
                        } 


                        function loadPeriode()
                        {
                            var tahun_id = $("#tahun_id").val();
                            console.log(tahun_id);
                            $.ajax({
                                type:'GET',
                                url:"<?php echo base_url(); ?>index.php/km/getPeriode",
                                data:"tahun_id=" + tahun_id,
                                success: function(html)
                                { 
                                    $("#periodeArea").html(html);
                                }
                            }); 
                        }

                        function loadBulan()
                        { 
                            var periode_id = $("#periode_id").val();
                            console.log(periode_id);
                            $.ajax({
                                type:'GET',
                                url:"<?php echo base_url(); ?>index.php/km/getBulan",
                                data:"periode_id=" + periode_id,
                                success: function(html)
                                { 
                                    console.log(html);
                                    html_str = html.split(" , ");
                                    document.getElementById("bulan_id1").value = html_str[0].split("|")[0];
                                    document.getElementById("bulan_id2").value = html_str[1].split("|")[0];
                                    document.getElementById("bulan_id3").value = html_str[2].split("|")[0];
                                    document.getElementById("bulan1").innerHTML = html_str[0].split("|")[1];
                                    document.getElementById("bulan2").innerHTML = html_str[1].split("|")[1];
                                    document.getElementById("bulan3").innerHTML = html_str[2].split("|")[1];
                                    // $("#periodeArea").html(html);
                                }
                            }); 
                        }
                          
                    </script>	