<!DOCTYPE html>
<html lang="en">

<head>
    <title>KM ONLINE DIT | Login</title>
    <!-- HTML5 Shim and Respond.js IE9 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="Phoenixcoded">
    <meta name="keywords" content=", Flat ui, Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="Phoenixcoded">
    <!-- Favicon icon -->

    <link rel="icon" href="<?php echo base_url();?>assets/assets/images/icon_telkom.png" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets//bower_components/bootstrap/css/bootstrap.min.css">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets/icon/themify-icons/themify-icons.css">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets/icon/icofont/css/icofont.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets/css/color/color-2.css">
    <!-- color .css -->

</head>

<body class="fix-menu" style="background: white;">
    <section class="login p-fixed d-flex text-center common-img-bg">
        <!-- Container-fluid starts -->
        <!-- <div class="container-fluid"> -->
            <div class="row" style="width: 100%; height: 100%;">
                <div class="col-sm-6" style="background-image: url('<?php echo base_url(); ?>assets/assets/images/background-2.png'); background-size: cover;">
                </div>
                <div class="col-sm-6">
                    <!-- Authentication card start -->
                    <div style="position: relative; float: right;">
                        <img width="80px" src="<?php echo base_url();?>assets/assets/images/telkom.png" alt="telkom.png">
                    </div>
                    <div class="login-card card-block auth-body" style="margin-left: 40px; margin-right: 40px; margin-top: 100px;"> 
                        
						<div class="text-center">
                            <h2><b>KM ONINE DIT</b></h2>
                            <span>Welcome back! Please login to your account.</span>
						</div>
						<div class="auth-box">
							<form action="<?php echo site_url();?>auth/login" class="form-signin" method="post">
							 
							 
                            <?php if ($message){ ?>
							<div class="alert alert-danger icons-alert" style="margin-bottom:0px;">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<i class="icofont icofont-close-line-circled"></i>
								</button>
								<p><?php echo $message;?></p>
							</div> 
                            <?php } ?>
                            
							<hr/>
							<div class="input-group">
								<input type="username" name="username" id="username" class="form-control" placeholder="Username">
								<span class="md-line"></span>
							</div>
							<div class="input-group">
								<input type="password" name="password" id="password" class="form-control" placeholder="Password">
								<span class="md-line"></span>
							</div>
							<div class="row m-t-25 text-left">
								<div class="col-sm-7 col-xs-12">
									<div class="checkbox-fade fade-in-primary">
										<label>
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
											<span class="text-inverse">Remember me</span>
										</label>
									</div>
								</div>
								<div class="col-sm-5 col-xs-12 forgot-phone text-right">
									<a href="<?php echo site_url();?>forget-password" class="text-right f-w-600 text-inverse"> Forgot Password</a>
								</div>
							</div>
							<div class="row m-t-30">
								<div class="col-md-6">
									<input type="submit" name="submit" class="btn btn-inverse btn-md btn-block waves-effect text-center m-b-20" value="Login" />
								</div>
								<div class="col-md-6">
									<input type="button" name="submit" class="btn btn-default btn-md btn-block waves-effect text-center m-b-20" value="Sign Up" />
								</div>
							</div>
							</form>
						</div>
                        <!-- end of form -->
                    </div>
                    <!-- Authentication card end -->
                </div>
                <!-- end of col-sm-12 -->
            </div>
            <!-- end of row -->
        <!-- </div> -->
        <!-- end of container-fluid -->
    </section>
    <!-- Warning Section Starts -->
    <!-- Older IE warning message -->

    <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script type="text/javascript" src="<?php echo base_url();?>assets//bower_components/jquery/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets//bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets//bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets//bower_components/bootstrap/js/bootstrap.min.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?php echo base_url();?>assets//bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="<?php echo base_url();?>assets//bower_components/modernizr/js/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets//bower_components/modernizr/js/css-scrollbars.js"></script>
    <!-- i18next.min.js -->
    <script type="text/javascript" src="<?php echo base_url();?>assets//bower_components/i18next/js/i18next.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets//bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets//bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets//bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
    <!-- Custom js -->
    <!--<script type="text/javascript" src="assets/js/script.js"></script>-->
    <!---- color js --->
    <script type="text/javascript" src="<?php echo base_url();?>assets/assets/js/common-pages.js"></script>
</body>

</html>
