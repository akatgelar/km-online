<!DOCTYPE html>
<html lang="en">

<head>
    <title>KM ONLINE DIT | Admin</title>
    <!-- HTML5 Shim and Respond.js IE9 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
      <!-- Meta -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
      <meta name="description" content="Phoenixcoded">
      <meta name="keywords" content=", Flat ui, Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
      <meta name="author" content="Phoenixcoded">
      <!-- Favicon icon -->
      <link rel="icon" href="<?php echo base_url();?>assets/assets/images/icon_telkom.png" type="image/x-icon">
      <!-- Google font-->
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
      <!-- Required Fremwork -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bower_components/bootstrap/css/bootstrap.min.css">
      <!-- themify icon -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets/icon/themify-icons/themify-icons.css">
      <!-- ico font -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets/icon/icofont/css/icofont.css">
      <!-- Font Awesome -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets/icon/font-awesome/css/font-awesome.min.css"><!-- flag icon framework css -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets/pages/flag-icon/flag-icon.min.css">
      <!-- Menu-Search css -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets/pages/menu-search/css/component.css">
      <!-- Horizontal-Timeline css -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets/pages/dashboard/horizontal-timeline/css/style.css">
      <!-- amchart css -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets/pages/dashboard/amchart/css/amchart.css">
      <!-- flag icon framework css -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets/pages/flag-icon/flag-icon.min.css">
      <!-- light-box css -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bower_components/ekko-lightbox/css/ekko-lightbox.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bower_components/lightbox2/css/lightbox.css">
      <!-- Data Table Css -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets/pages/data-table/css/buttons.dataTables.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
      <!-- Style.css -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets/css/style.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets/css/colorbox.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets/css/color/color-2.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets/scss/color/color-2.scss">
      <!--color css-->

      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bower_components/chartist/css/chartist.css">

      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets/css/linearicons.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets/css/simple-line-icons.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets/css/ionicons.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets/css/jquery.mCustomScrollbar.css">

      <!-- Required Jqurey -->
      <script type="text/javascript" src="<?php echo base_url();?>assets/bower_components/jquery/js/jquery.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>assets/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>assets/bower_components/popper.js/js/popper.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>assets/bower_components/bootstrap/js/bootstrap.min.js"></script>
      

  </head>

  <?php $row = $users->row_array();?>	
  <body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div></div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <!-- Menu header start -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">

            <nav class="navbar header-navbar pcoded-header iscollapsed" header-theme="theme4" pcoded-header-position="fixed">
                <div class="navbar-wrapper">
                    <div class="navbar-logo" style="justify-content: left; padding-left: 20px;">
                        <a class="mobile-menu" id="mobile-collapse" href="#!">
                            <i class="ti-menu"></i>
                        </a>
                        <a class="mobile-search morphsearch-search" href="#">
                            <i class="ti-search"></i>
                        </a>
                        <a href="<?php echo base_url();?>" style="float: left;">
                            <!-- <img style="height:40px;" class="img-fluid" src="<?php echo base_url();?>assets/assets/images/salman_iconwhite.png" alt="Theme-Logo" /> -->
                            <span style="font-size: inherit;"><b>KM ONLINE DIT</b></span>
                        </a>
                        <a class="mobile-options">
                            <i class="ti-more"></i>
                        </a>
                    </div>
                    <div class="navbar-container container-fluid">
                        <div>
                            <ul class="nav-left">
                                <li>
                                    <div class="sidebar_toggle"><a href="javascript:void(0)"><i class="ti-menu"></i></a></div>
                                </li>
                            </ul>
                            <ul class="nav-right">
                                <li class="user-profile header-notification">
                                    <a href="#!">
                                        <span><?php echo $username;?></span>
                                        <i class="ti-angle-down"></i>
                                        <img style="margin-left: 10px;" src="<?php echo base_url();?>assets/upload/profile/<?php echo $row['foto'];?>" alt="User-Profile-Image">
                                        
                                    </a>
                                    <ul class="show-notification profile-notification">
                                        <li>
                                            <a href="<?php echo base_url();?>profile">
                                                <i class="ti-user"></i> Profile
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url();?>auth/logout">
                                                <i class="ti-layout-sidebar-left"></i> Logout
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
         
            <div class="pcoded-main-container" style="margin-top: 56px;">
                <div class="pcoded-wrapper">