                    
                    <?php $row = $users->row_array();?>	
                    <nav class="pcoded-navbar" pcoded-header-position="fixed" navbar-theme="theme4" active-item-theme="theme5" sub-item-theme="theme2" active-item-style="style0" pcoded-navbar-position="fixed" style="top: auto;">
                        <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
                        <div class="pcoded-inner-navbar main-menu" > 
                            <!-- <div class="pcoded-navigatio-lavel" data-i18n="nav.category.navigation" menu-title-theme="theme5">Menu</div> -->
                            <ul class="pcoded-item pcoded-left-item">
                                <?php 
                                    if ($privilage['home'][0] == 1){   
                                        if ($set_active['home']){ 
                                            echo "<li class='".$set_active['home']."'>"; 
                                        }else {
                                            echo "<li>";
                                        }
                                            echo '<a href="'.site_url().'home">'; 
                                            echo '<span class="pcoded-micon"><i class="ti-home"></i></span>';
                                            echo '<span class="pcoded-mtext">Home</span>';
                                            echo '<span class="pcoded-mcaret"></span>';
                                            echo '</a>';
                                        echo "</li>";
                                    } 
                                ?> 
                                <?php 
                                    if ($privilage['km'][0] == 1){   
                                        if ($set_active['km']){ 
                                            echo "<li class='".$set_active['km']."'>"; 
                                        }else {
                                            echo "<li>";
                                        }
                                            echo '<a href="'.site_url().'km">'; 
                                            echo '<span class="pcoded-micon"><i class="ti-files"></i></span>';
                                            echo '<span class="pcoded-mtext">Pembuatan KM Bidang</span>';
                                            echo '<span class="pcoded-mcaret"></span>';
                                            echo '</a>';
                                        echo "</li>";
                                    } 
                                ?> 
                                <?php 
                                    if ($privilage['km_nilai'][0] == 1){   
                                        if ($set_active['km_nilai']){ 
                                            echo "<li class='".$set_active['km_nilai']."'>"; 
                                        }else {
                                            echo "<li>";
                                        }
                                            echo '<a href="'.site_url().'km_nilai">'; 
                                            echo '<span class="pcoded-micon"><i class="ti-pencil-alt"></i></span>';
                                            echo '<span class="pcoded-mtext">Penilaian KM Bidang</span>';
                                            echo '<span class="pcoded-mcaret"></span>';
                                            echo '</a>';
                                        echo "</li>";
                                    } 
                                ?>  
                                <?php 
                                    if ($privilage['dashboard'][0] == 1){   
                                        if ($set_active['dashboard']){ 
                                            echo "<li class='".$set_active['dashboard']."'>"; 
                                        }else {
                                            echo "<li>";
                                        }
                                            echo '<a href="'.site_url().'dashboard">'; 
                                            echo '<span class="pcoded-micon"><i class="ti-bar-chart-alt"></i></span>';
                                            echo '<span class="pcoded-mtext">Dashboard</span>';
                                            echo '<span class="pcoded-mcaret"></span>';
                                            echo '</a>';
                                        echo "</li>";
                                    } 
                                ?>  
                            </ul>
                             
                            <div class="pcoded-navigatio-lavel" data-i18n="nav.category.navigation" menu-title-theme="theme5">Master Data</div>
                                 
                            <ul class="pcoded-item pcoded-left-item">
                                <?php 
                                    if ($privilage['evidence'][0] == 1){   
                                        if ($set_active['evidence']){ 
                                            echo "<li class='".$set_active['evidence']."'>"; 
                                        }else {
                                            echo "<li>";
                                        }
                                            echo '<a href="'.site_url().'evidence">'; 
                                            echo '<span class="pcoded-micon"><i class="fa fa-upload"></i></span>';
                                            echo '<span class="pcoded-mtext">Evidence</span>';
                                            echo '<span class="pcoded-mcaret"></span>';
                                            echo '</a>';
                                        echo "</li>";
                                    } 
                                ?>  
                                <?php 
                                    if ($privilage['bidang_kategori'][0] == 1){   
                                        if ($set_active['bidang_kategori']){ 
                                            echo "<li class='".$set_active['bidang_kategori']."'>"; 
                                        }else {
                                            echo "<li>";
                                        }
                                            echo '<a href="'.site_url().'bidang_kategori">'; 
                                            echo '<span class="pcoded-micon"><i class="ti-layout-grid2-alt"></i></span>';
                                            echo '<span class="pcoded-mtext">Bidang Kategori</span>';
                                            echo '<span class="pcoded-mcaret"></span>';
                                            echo '</a>';
                                        echo "</li>";
                                    } 
                                ?>  
                                <?php 
                                    if ($privilage['bidang'][0] == 1){   
                                        if ($set_active['bidang']){ 
                                            echo "<li class='".$set_active['bidang']."'>"; 
                                        }else {
                                            echo "<li>";
                                        }
                                            echo '<a href="'.site_url().'bidang">'; 
                                            echo '<span class="pcoded-micon"><i class="ti-layout-grid3-alt"></i></span>';
                                            echo '<span class="pcoded-mtext">Bidang</span>';
                                            echo '<span class="pcoded-mcaret"></span>';
                                            echo '</a>';
                                        echo "</li>";
                                    } 
                                ?> 
                                <?php 
                                    if ($privilage['tahun'][0] == 1){   
                                        if ($set_active['tahun']){ 
                                            echo "<li class='".$set_active['tahun']."'>"; 
                                        }else {
                                            echo "<li>";
                                        }
                                            echo '<a href="'.site_url().'tahun">'; 
                                            echo '<span class="pcoded-micon"><i class="ti-calendar"></i></span>';
                                            echo '<span class="pcoded-mtext">Tahun</span>';
                                            echo '<span class="pcoded-mcaret"></span>';
                                            echo '</a>';
                                        echo "</li>";
                                    } 
                                ?>   
                                <?php 
                                    if ($privilage['periode'][0] == 1){   
                                        if ($set_active['periode']){ 
                                            echo "<li class='".$set_active['periode']."'>"; 
                                        }else {
                                            echo "<li>";
                                        }
                                            echo '<a href="'.site_url().'periode">'; 
                                            echo '<span class="pcoded-micon"><i class="ti-time"></i></span>';
                                            echo '<span class="pcoded-mtext">Periode</span>';
                                            echo '<span class="pcoded-mcaret"></span>';
                                            echo '</a>';
                                        echo "</li>";
                                    } 
                                ?>   
                                <?php 
                                    if ($privilage['periode_bulan'][0] == 1){   
                                        if ($set_active['periode_bulan']){ 
                                            echo "<li class='".$set_active['periode_bulan']."'>"; 
                                        }else {
                                            echo "<li>";
                                        }
                                            echo '<a href="'.site_url().'periode_bulan">'; 
                                            echo '<span class="pcoded-micon"><i class="ti-alarm-clock"></i></span>';
                                            echo '<span class="pcoded-mtext">Periode Bulan</span>';
                                            echo '<span class="pcoded-mcaret"></span>';
                                            echo '</a>';
                                        echo "</li>";
                                    } 
                                ?>   
                                <?php 
                                    if ($privilage['user'][0] == 1){   
                                        if ($set_active['user']){ 
                                            echo "<li class='".$set_active['user']."'>"; 
                                        }else {
                                            echo "<li>";
                                        }
                                            echo '<a href="'.site_url().'user">'; 
                                            echo '<span class="pcoded-micon"><i class="ti-user"></i></span>';
                                            echo '<span class="pcoded-mtext">User Admin</span>';
                                            echo '<span class="pcoded-mcaret"></span>';
                                            echo '</a>';
                                        echo "</li>";
                                    } 
                                ?>  
                                <li style="height: 100px;">
                                </li>
                            </ul>
                        </div>
                    </nav>