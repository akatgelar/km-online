                                      
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">

                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- <div class="page-header">
                                        <div class="page-header-title">
                                            <h4>user</h4>
                                        </div>
                                        <div class="page-header-breadcrumb">
                                            <ul class="breadcrumb-title">
                                                <li class="breadcrumb-item">
                                                    <a href="<?php echo site_url();?>">
                                                        <i class="icofont icofont-home"></i>
                                                    </a>
                                                </li>
                                                <li class="breadcrumb-item"><a href="#!">user</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div> -->
                                            
						            <?php if($alert){echo $alert;}?>

                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5>Kelola Data User Admin</h5>
                                                        <span>Di halaman ini, Anda dapat mengelola data user admin yang ada di website Anda.</span>
                                                        <div class="card-header-right">
                                                            <i class="icofont icofont-rounded-down"></i>
                                                            <i class="icofont icofont-refresh"></i>
                                                            <i class="icofont icofont-close-circled"></i>
                                                        </div>
                                                    </div>
                                                    <div class="card-block">
                                                    
                                                        <?php 
                                                            if ($can_insert == TRUE){?>
                                                                <button class="btn btn-primary" onclick="javascript:add();" >
                                                                    <i class="fa fa-plus"></i>&nbsp;
                                                                    Tambah Data
                                                                </button>
                                                                <br/>
                                                            <?php }
                                                        ?>
                                                        <hr/>
                                                        <div class="dt-responsive table-responsive">
                                                            <table id="simpletable" class="table table-striped table-bordered nowrap">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="center border-checkbox-section" style="width:10px; padding-right:0px; vertical-align: middle;">
                                                                            <div class="border-checkbox-group border-checkbox-group-primary">
                                                                                <input class="border-checkbox" type="checkbox" id="checkbox">
                                                                                <label class="border-checkbox-label" for="checkbox"></label>
                                                                            </div>
                                                                        </th>  
                                                                        <th style="vertical-align: middle;" class="left">Username</th>
                                                                        <th style="vertical-align: middle;" class="left">Level</th>
                                                                        <th style="vertical-align: middle;" class="center">Gambar</th>
                                                                        <th style="vertical-align: middle;" class="center">Last Login</th>
                                                                        <th style="vertical-align: middle;" class="center">Aksi</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php $i=0; foreach($results->result() as $row) {$i++;?>
                                                                    <tr>
                                                                        <td class="center border-checkbox-section" style="width:10px; padding-right:0px;">
                                                                            <div class="border-checkbox-group border-checkbox-group-primary">
                                                                                <input class="border-checkbox" type="checkbox" id="checkbox<?php echo $i;?>">
                                                                                <label class="border-checkbox-label" for="checkbox<?php echo $i;?>"></label>
                                                                            </div>
                                                                        </td>  
                                                                        <td><?php echo $row->username;?></td>
                                                                        <td><?php echo $row->nama;?></td>
                                                                        <td class="center"><img style="width:100px;" src="<?php echo base_url();?>assets/upload/profile/thumb/<?php echo $row->foto;?>"/></td>
                                                                        <td class="center"><?php if($row->datetime){echo indo_tgl_jam($row->datetime);}?></td>
                                                                        <td class="center">
                                                                            <?php 
                                                                                if ($can_update == TRUE){
                                                                                    echo '<a class="btn btn-warning btn-mini center" style="padding-right: 5px;" href="' . site_url() . "user/edit/" . $row->id . '" title="Edit"><i class="fa fa-pencil"></i></a>';
                                                                                }
                                                                                echo "&nbsp; ";
                                                                                if ($can_delete == TRUE){
                                                                                    echo '<a class="btn btn-danger btn-mini center" style="padding-right: 5px;" href="' . site_url() . "user/delete/" . $row->id . '" title="Delete" onclick="return  confirm(\'Yakin akan menghapus data ini?\')"><i class="fa fa-trash"></i></a>';
                                                                                }
                                                                            ?> 
                                                                        </td>
                                                                    </tr>
								                                <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript" src="<?php echo base_url();?>assets/assets/js/jquery.colorbox-min.js"></script>
                    
                    <script type="text/javascript">
                        $(function() {
                            var colorbox_params = {
                                reposition:true,
                                scalePhotos:true,
                                scrolling:false,
                                previous:'<i class="icon-arrow-left"></i>',
                                next:'<i class="icon-arrow-right"></i>',
                                close:'<i class="icon-remove"></i>',
                                current:'{current} of {total}',
                                maxWidth:'100%',
                                maxHeight:'100%',
                                onOpen:function(){
                                    document.body.style.overflow = 'hidden';
                                },
                                onClosed:function(){
                                    document.body.style.overflow = 'auto';
                                },
                                onComplete:function(){
                                    $.colorbox.resize();
                                }
                            };

                            $('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
				            $("#cboxLoadingGraphic").append("<i class='icon-spinner orange'></i>");//let's add a custom loading icon

                        });
                        
                        function add(){
                            //editor1.innerHTML = "My new text!";
                            document.location.href = '<?php echo site_url() . "user/add/";?>';
                        }
                        
                    </script>	