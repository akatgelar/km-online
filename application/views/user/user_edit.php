<div class="pcoded-content">
                        <div class="pcoded-inner-content">

                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- <div class="page-header">
                                        <div class="page-header-title">
                                            <h4>user</h4>
                                        </div>
                                        <div class="page-header-breadcrumb">
                                            <ul class="breadcrumb-title">
                                                <li class="breadcrumb-item">
                                                    <a href="<?php echo site_url();?>">
                                                        <i class="icofont icofont-home"></i>
                                                    </a>
                                                </li>
                                                <li class="breadcrumb-item"><a href="#!">user</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div> -->
                                            
						            <?php if($alert){echo $alert;}?>

                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5>Kelola Data User Admin</h5>
                                                        <span>Di halaman ini, Anda dapat mengelola data user admin yang ada di website Anda.</span>
                                                        <div class="card-header-right">
                                                            <i class="icofont icofont-rounded-down"></i>
                                                            <i class="icofont icofont-refresh"></i>
                                                            <i class="icofont icofont-close-circled"></i>
                                                        </div>
                                                    </div>
                                                    <div class="card-block">
                                                    
                                                    <?php
                                                        $attributes = array('name' => 'user', 'type'=>'post', 'id' => 'user', 'class'=>'block-content form');
                                                        echo form_open_multipart('user/update', $attributes);
                                                    ?> 
                                                    <?php $row = $results->row_array();?>	

                                                        
                                                        <div class="form-group row" style="display:none">
                                                            <label class="col-sm-2 col-form-label">ID</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" id="id"  name="id" placeholder="id" value="<?php echo $row['id'];?>"> 
                                                                <span class="messages"></span>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label">Level *</label>
                                                            <div class="col-sm-10">
                                                                <select name="level_id" id="level_id" class="form-control">
                                                                    <option value="">-- Pilih Level --</option>
                                                                    <?php $i=0; foreach($level->result() as $rows) {$i++;?>
                                                                    <option value="<?php echo $rows->id;?>" <?php if($row['level_id'] == $rows->id){echo "selected ";}?>><?php echo $rows->nama;?></option>
                                                                    <?php } ?>
                                                                </select>
                                                                <span class="messages"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label">Username *</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" id="username" name="username" class="form-control" placeholder="Username" value="<?php echo $row['username'];?>">
                                                                <span class="messages"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label">Password *</label>
                                                            <div class="col-sm-10">
                                                                <input type="password" id="password" name="password" class="form-control" placeholder="Password" value="<?php echo $row['password_plan'];?>">
                                                                <span class="messages"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label">Gambar</label>
                                                            <div class="col-sm-10">
                                                                <input type="file" id="userfile" name="userfile" class="form-control" placeholder="Gambar"  value="<?php echo $row['foto'];?>"/>
                                                                <span class="messages"></span>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label"></label>
                                                            <div class="col-sm-10" style="text-align: right;">
                                                                <button class="btn btn-primary" id="send"  name="send" type="submit" onclick="javascript:kirim();">
                                                                    <i class="fa fa-save"></i>
                                                                    Simpan
                                                                </button>
                                                                <button class="btn btn-danger" type="button" onclick="javascript:batal();">
                                                                    <i class="fa fa-times"></i>
                                                                    Batal
                                                                </button>
                                                            </div>
                                                        </div>
                                                        
                                                    </form> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

 
                    <script type="text/javascript">
    

                        function batal(){
                            document.location.href = '<?php echo site_url().'user'?>';
                        }
                        
                        function kirim(){
                        } 

                        $(document).ready(function() {

                            validate.extend(validate.validators.datetime, {

                                parse: function(value, options) {

                                    return +moment.utc(value);
                                },
                                // Input is a unix timestamp
                                format: function(value, options) {

                                    var format = options.dateOnly ? "DD/MM/YYYY" : "DD/MM/YYYY";
                                    return moment.utc(value).format(format);
                                }
                            });

                            // These are the constraints used to validate the form
                            var constraints = {
                                level_id: {
                                    presence: true,
                                },
                                username: {
                                    presence: true,
                                },
                                password: {
                                    presence: true,
                                }
                            };

                            // Hook up the form so we can prevent it from being posted
                            var form = document.querySelector("form#user");
                            form.addEventListener("submit", function(ev) {

                                ev.preventDefault();
                                handleFormSubmit(form);
                            });

                            // Hook up the inputs to validate on the fly
                            var inputs = document.querySelectorAll("input, textarea, select")
                            for (var i = 0; i < inputs.length; ++i) {

                                inputs.item(i).addEventListener("change", function(ev) {

                                    var errors = validate(form, constraints) || {};
                                    showErrorsForInput(this, errors[this.name]);

                                });
                            }

                            function handleFormSubmit(form, input) {


                                // validate the form aainst the constraints
                                var errors = validate(form, constraints);
                                // then we update the form to reflect the results
                                showErrors(form, errors || {});
                                if (!errors) {

                                    showSuccess();
                                }
                            }

                            // Updates the inputs with the validation errors
                            function showErrors(form, errors) {

                                // We loop through all the inputs and show the errors for that input
                                _.each(form.querySelectorAll("input[name], select[name]"), function(input) {
                                    // Since the errors can be null if no errors were found we need to handle
                                    // that
                                    showErrorsForInput(input, errors && errors[input.name]);
                                });
                            }

                            // Shows the errors for a specific input
                            function showErrorsForInput(input, errors) {
                                // This is the root of the input

                                var formGroup = closestParent(input.parentNode, "form-group")
                                    // Find where the error messages will be insert into
                                    ,
                                    messages = formGroup.querySelector(".messages");
                                // First we remove any old messages and resets the classes
                                resetFormGroup(formGroup);
                                // If we have errors
                                if (errors) {
                                    // we first mark the group has having errors
                                    formGroup.classList.add("has-error");
                                    // then we append all the errors
                                    _.each(errors, function(error) {

                                        addError(messages, error, input);
                                    });
                                } else {
                                    // otherwise we simply mark it as success
                                    formGroup.classList.add("has-success");
                                }
                            }

                            // Recusively finds the closest parent that has the specified class
                            function closestParent(child, className) {
                                if (!child || child == document) {
                                    return null;
                                }
                                if (child.classList.contains(className)) {
                                    return child;
                                } else {
                                    return closestParent(child.parentNode, className);
                                }
                            }

                            function resetFormGroup(formGroup) {

                                // Remove the success and error classes
                                formGroup.classList.remove("has-error");
                                formGroup.classList.remove("has-success");
                                // and remove any old messages
                                _.each(formGroup.querySelectorAll(".text-danger"), function(el) {
                                    el.parentNode.removeChild(el);
                                });
                            }

                            // Adds the specified error with the following markup
                            // <p class="help-block error">[message]</p>
                            function addError(messages, error, input) {

                                var block = document.createElement("p");
                                block.classList.add("text-danger");
                                block.classList.add("error");
                                block.innerText = error;
                                messages.appendChild(block);
                                $(input).addClass("input-danger");
                            }

                            function showSuccess() {

                                // We made it \:D/
                                // alert("Success!");
                                console.log('succes');
                                document.getElementById("user").submit();// Form submission
                            }

                        });
                    </script>	