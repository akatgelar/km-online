                                      
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">

                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- <div class="page-header">
                                        <div class="page-header-title">
                                            <h4>periode</h4>
                                        </div>
                                        <div class="page-header-breadcrumb">
                                            <ul class="breadcrumb-title">
                                                <li class="breadcrumb-item">
                                                    <a href="<?php echo site_url();?>">
                                                        <i class="icofont icofont-home"></i>
                                                    </a>
                                                </li>
                                                <li class="breadcrumb-item"><a href="#!">periode</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div> -->
                                            
						            <?php if($alert){echo $alert;}?>

                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5>Kelola Data Periode</h5>
                                                        <span>Di halaman ini, Anda dapat mengelola data periode yang ada di website Anda.</span>
                                                        <div class="card-header-right">
                                                            <i class="icofont icofont-rounded-down"></i>
                                                            <i class="icofont icofont-refresh"></i>
                                                            <i class="icofont icofont-close-circled"></i>
                                                        </div>
                                                    </div>
                                                    <div class="card-block">
                                                    
                                                        <?php 
                                                            if ($can_insert == TRUE){?>
                                                                <button class="btn btn-primary" onclick="javascript:add();" >
                                                                    <i class="fa fa-plus"></i>&nbsp;
                                                                    Tambah Data
                                                                </button>
                                                                <br/>
                                                            <?php }
                                                        ?>
                                                        <hr/>
                                                        <div class="dt-responsive table-responsive">
                                                            <table id="simpletable" class="table table-striped table-bordered nowrap">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="center border-checkbox-section" style="width:10px; padding-right:0px; vertical-align: middle;">
                                                                            <div class="border-checkbox-group border-checkbox-group-primary">
                                                                                <input class="border-checkbox" type="checkbox" id="checkbox">
                                                                                <label class="border-checkbox-label" for="checkbox"></label>
                                                                            </div>
                                                                        </th>   
                                                                        <th style="vertical-align: middle;" class="left">Tahun</th>
                                                                        <th style="vertical-align: middle;" class="left">Nama Periode</th> 
                                                                        <th style="vertical-align: middle;" class="center">Keterangan</th>
                                                                        <th style="vertical-align: middle;" class="center">Aksi</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php $i=0; foreach($results->result() as $row) {$i++;?>
                                                                    <tr>
                                                                        <td class="center border-checkbox-section" style="width:10px; padding-right:0px;">
                                                                            <div class="border-checkbox-group border-checkbox-group-primary">
                                                                                <input class="border-checkbox" type="checkbox" id="checkbox<?php echo $i;?>">
                                                                                <label class="border-checkbox-label" for="checkbox<?php echo $i;?>"></label>
                                                                            </div>
                                                                        </td>   
                                                                        <td><b><?php echo $row->tahun;?></b></td>
                                                                        <td><?php echo $row->nama_periode;?></td> 
                                                                        <td><?php echo $row->keterangan;?></td> 
                                                                        <td class="center">
                                                                            <?php 
                                                                                if (1==1){
                                                                                    echo '<a class="btn btn-info btn-mini center" style="padding-right: 5px;" href="' . site_url() . "periode/preview/" . $row->id . '" title="Preview" data-rel="colorbox" ><i class="fa fa-eye"></i></a>';
                                                                                }
                                                                                echo "&nbsp; ";
                                                                                if ($can_update == TRUE){
                                                                                    echo '<a class="btn btn-warning btn-mini center" style="padding-right: 5px;" href="' . site_url() . "periode/edit/" . $row->id . '" title="Edit"><i class="fa fa-pencil"></i></a>';
                                                                                }
                                                                                echo "&nbsp; ";
                                                                                if ($can_delete == TRUE){
                                                                                    echo '<a class="btn btn-danger btn-mini center" style="padding-right: 5px;" href="' . site_url() . "periode/delete/" . $row->id . '" title="Delete" onclick="return  confirm(\'Yakin akan menghapus data ini?\')"><i class="fa fa-trash"></i></a>';
                                                                                }
                                                                            ?> 
                                                                        </td>
                                                                    </tr>
								                                <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript">
                        
                        function add(){
                            //editor1.innerHTML = "My new text!";
                            document.location.href = '<?php echo site_url() . "periode/add/";?>';
                        }
                        
                    </script>	