                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">

                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- <div class="page-header">
                                        <div class="page-header-title">
                                            <h4>bidang</h4>
                                        </div>
                                        <div class="page-header-breadcrumb">
                                            <ul class="breadcrumb-title">
                                                <li class="breadcrumb-item">
                                                    <a href="<?php echo site_url();?>">
                                                        <i class="icofont icofont-home"></i>
                                                    </a>
                                                </li>
                                                <li class="breadcrumb-item"><a href="#!">bidang</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div> -->
                                            
						            <?php if($alert){echo $alert;}?>

                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5>Kelola Data KM Nilai</h5>
                                                        <span>Di halaman ini, Anda dapat mengelola data bidang yang ada di website Anda.</span>
                                                        <div class="card-header-right">
                                                            <i class="icofont icofont-rounded-down"></i>
                                                            <i class="icofont icofont-refresh"></i>
                                                            <i class="icofont icofont-close-circled"></i>
                                                        </div>
                                                    </div>
                                                    <div class="card-block">
														<table class="table table-striped table-bordered" id="example" style="font-size: smaller;" width="100%">
															<thead>
																<tr>
																	<th width="5%" style="background-color: #000; color: #fff; vertical-align: middle;" rowspan="2">No</th>
																	<th width="40%" style="background-color: #000; color: #fff; vertical-align: middle;" rowspan="2" colspan="2">KPI</th>
																	<th width="10%" style="background-color: #1F4E78; color: #fff; vertical-align: middle;" rowspan="2">Weight</th>
																	<th width="10%" style="background-color: #1F4E78; color: #fff; vertical-align: middle;" rowspan="2">Unit</th>
																	<?php foreach($periode->result() as $row) { ?>
																		<th style="background-color: #9BC2E6; color: #000; text-align: center;" colspan="4"><?php echo $row->nama_bulan;?></th> 
																	<?php } ?>  
																	<th width="15%" style="background-color: #1F4E78; color: #fff; vertical-align: middle;" rowspan="2">Total</th>
																	<th width="15%" style="background-color: #1F4E78; color: #fff; vertical-align: middle;" rowspan="2">Evidence</th>
																</tr>
																<tr>
																	<?php foreach($periode->result() as $row) { ?>
																		<th width="5%" style="background-color: #9BC2E6; color: #000;">Target</th> 
																		<th width="5%" style="background-color: #9BC2E6; color: #000;">Result</th> 
																		<th width="5%" style="background-color: #9BC2E6; color: #000;">Score</th> 
																		<th width="5%" style="background-color: #9BC2E6; color: #000;">Ach</th> 
																	<?php } ?>  
																</tr>
															</thead>
															<tbody>
                                                            <?php $level1=0; $level2=0; $level3=0; $level4=0;?>
																<?php foreach($detail->result() as $row) { ?>
																	<?php if($row->level == 1) { $level1++;?>	 
																	<tr>
																		<td style="background-color: #C00000; color: #fff;"><?php echo integerToRoman($level1); ?></th>
																		<td style="background-color: #C00000; color: #fff;" colspan="2"><?php echo $row->kpi; ?></td>
																		<td style="background-color: #C00000; color: #fff;"><?php echo $row->weight; ?></td>
																		<td style="background-color: #C00000; color: #fff;"><?php echo $row->unit; ?></td>
																		<?php foreach($periode2 as $row2) { ?>  
                                                                            <?php foreach($row2['nilai'] as $row3) { ?>
                                                                                <?php if($row->id == $row3->km_detail_id) { ?> 
                                                                                    <td style="background-color: #C00000; color: #fff;"><?php echo $row3->target; ?></td>
                                                                                    <td style="background-color: #C00000; color: #fff;">
                                                                                        <!-- <a 
                                                                                            id="link<?php echo $row3->id;?>"
                                                                                            href="#" data-toggle="modal" data-target="#result-modal"  style="color: white; <?php if($row3->result !== NULL) { echo "display: none;";}?>" 
                                                                                            data-id="<?php echo $row3->id;?>"
                                                                                            data-km_id="<?php echo $row3->km_id;?>"
                                                                                            data-km_detail_id="<?php echo $row3->km_detail_id;?>"
                                                                                            data-weight="<?php echo $row->weight;?>"
                                                                                            data-targets="<?php echo $row3->target;?>"
                                                                                            class="open-AddBookDialog1"> 
                                                                                            <i class="fa fa-pencil"></i>
                                                                                        </a>
                                                                                        <span id="result<?php echo $row3->id;?>" style="text-decoration: underline; cursor: pointer;"
                                                                                            href="#" data-toggle="modal" data-target="#result-modal"  style="color: white;" 
                                                                                            data-id="<?php echo $row3->id;?>"
                                                                                            data-km_id="<?php echo $row3->km_id;?>"
                                                                                            data-km_detail_id="<?php echo $row3->km_detail_id;?>"
                                                                                            data-weight="<?php echo $row->weight;?>"
                                                                                            data-targets="<?php echo $row3->target;?>"
                                                                                            class="open-AddBookDialog2"><?php echo $row3->result;?>
                                                                                        </span> -->
                                                                                    </td> 
                                                                                    <td style="background-color: #C00000; color: #fff;">
                                                                                        <!-- <span id="score<?php echo $row3->id;?>"><?php echo $row3->score;?></span> -->
                                                                                    </td> 
                                                                                    <td style="background-color: #C00000; color: #fff;">
                                                                                        <!-- <span id="ach<?php echo $row3->id;?>"><?php echo $row3->ach;?></span> -->
                                                                                    </td>
                                                                                <?php } ?>  
                                                                            <?php } ?>    
																		<?php } ?>    
                                                                        <td style="background-color: #C00000; color: #fff;"><span id="total<?php echo $row->id;?>"></span></td>
                                                                        <td style="background-color: #C00000; color: #fff;">
                                                                            <!-- <a href="#" data-toggle="modal" data-target="#upload-modal"  style="color: white;"> 
                                                                                <i class="fa fa-upload"></i>
                                                                            </a> -->
                                                                            <a href="<?php echo base_url()?>assets/upload/evidence/<?php echo $row->file_path;?>" target="_blank"> 
																				<?php 
																					if($row->file_type == "DOCX") { 
																						echo '<i class="fa fa-file-word-o"></i>';
																					}else if($row->file_type == "XLSX"){
																						echo '<i class="fa fa-file-excel-o"></i>';
																					}else if($row->file_type == "PPTX"){
																						echo '<i class="fa fa-file-powerpoint-o"></i>';
																					}else if($row->file_type == "PDF"){
																						echo '<i class="fa fa-file-pdf-o"></i>';
																					}else if($row->file_type == "RAR"){
																						echo '<i class="fa fa-file-zip-o"></i>';
																					}else if($row->file_type == "Image"){
																						echo '<i class="fa fa-file-pcture-o"></i>';
																					}else if($row->file_type == "Lainnya"){
																						echo '<i class="fa fa-file-o"></i>';
																					}
																				?>
                                                                            </a>
                                                                        </td>  
																	</tr>  		 
																	<?php } ?>  
																	<?php if($row->level == 2) { $level2++; $level3=0; $km_id=0; $km_detail_id=0;?>	 
																	<tr>
																		<td><?php echo $level2; ?></th>
																		<td width="200" colspan="2" style="white-space: pre-wrap;"><?php echo $row->kpi; ?></td>
																		<td><?php echo $row->weight; ?></td>
																		<td><?php echo $row->unit; ?></td>
																		<?php foreach($periode2 as $row2) { ?>  
                                                                            <?php foreach($row2['nilai'] as $row3) { ?>
                                                                                <?php if($row->id == $row3->km_detail_id) { ?> 
                                                                                    <td><?php echo $row3->target; ?></td>
                                                                                    <td>
                                                                                        <a 
                                                                                            id="link<?php echo $row3->id;?>"
                                                                                            href="#" data-toggle="modal" data-target="#result-modal"  style="color: black; <?php if($row3->result !== NULL) { echo "display: none;";}?>"
                                                                                            data-id="<?php echo $row3->id;?>"
                                                                                            data-km_id="<?php echo $row3->km_id;?>"
                                                                                            data-km_detail_id="<?php echo $row3->km_detail_id;?>"
                                                                                            data-periode_bulan_id="<?php echo $row3->periode_bulan_id;?>"
                                                                                            data-weight="<?php echo $row->weight;?>"
                                                                                            data-targets="<?php echo $row3->target;?>"
                                                                                            class="open-AddBookDialog1"> 
                                                                                            <i class="fa fa-pencil"></i>
                                                                                        </a>
                                                                                        <span id="result<?php echo $row3->id;?>" style="text-decoration: underline; cursor: pointer;"
                                                                                            href="#" data-toggle="modal" data-target="#result-modal"  style="color: white;" 
                                                                                            data-id="<?php echo $row3->id;?>"
                                                                                            data-km_id="<?php echo $row3->km_id;?>"
                                                                                            data-km_detail_id="<?php echo $row3->km_detail_id;?>"
                                                                                            data-weight="<?php echo $row->weight;?>"
                                                                                            data-targets="<?php echo $row3->target;?>"
                                                                                            class="open-AddBookDialog2"><?php echo $row3->result;?>
                                                                                        </span>
                                                                                    </td>  
                                                                                    <td><span id="score<?php echo $row3->id;?>"><?php echo $row3->score;?></span></td> 
                                                                                    <td><span id="ach<?php echo $row3->id;?>"><?php echo $row3->ach;?></span></td>
                                                                                    <?php $km_id = $row3->km_id; $km_detail_id = $row3->km_detail_id;?>
                                                                                <?php } ?>  
                                                                            <?php } ?>    
																		<?php } ?> 
                                                                        <td><span id="total<?php echo $row->id;?>"></span></td>
                                                                        <td>
                                                                            <a 
                                                                                id="link<?php echo $row3->id;?>"
                                                                                href="#" data-toggle="modal" data-target="#upload-modal"  style="color: black;"
                                                                                data-km_id="<?php echo $km_id;?>"
                                                                                data-km_detail_id="<?php echo $km_detail_id;?>"
                                                                                class="open-Upload1"> 
                                                                                <i class="fa fa-upload"></i>
                                                                            </a> | 
                                                                            <a href="<?php echo base_url()?>assets/upload/evidence/<?php echo $row->file_path;?>" target="_blank"> 
																				<?php 
																					if($row->file_type == "DOCX") { 
																						echo '<i class="fa fa-file-word-o"></i>';
																					}else if($row->file_type == "XLSX"){
																						echo '<i class="fa fa-file-excel-o"></i>';
																					}else if($row->file_type == "PPTX"){
																						echo '<i class="fa fa-file-powerpoint-o"></i>';
																					}else if($row->file_type == "PDF"){
																						echo '<i class="fa fa-file-pdf-o"></i>';
																					}else if($row->file_type == "RAR"){
																						echo '<i class="fa fa-file-zip-o"></i>';
																					}else if($row->file_type == "Image"){
																						echo '<i class="fa fa-file-pcture-o"></i>';
																					}else if($row->file_type == "Lainnya"){
																						echo '<i class="fa fa-file-o"></i>';
																					}
																				?>
                                                                            </a>
                                                                        </td>  
																	</tr>  		 
																	<?php } ?>  
																	<?php if($row->level == 3) { $level3++; $level4=0; $km_id=0; $km_detail_id=0;?>	 
																	<tr>
																		<td></th>
																		<td width="20"><?php echo integerToAlphabet($level3-1); ?>.</td>
																		<td width="250" style="white-space: pre-wrap;"><?php echo $row->kpi; ?></td>
																		<td><?php echo $row->weight; ?></td>
																		<td><?php echo $row->unit; ?></td>
																		<?php foreach($periode2 as $row2) { ?>  
                                                                            <?php foreach($row2['nilai'] as $row3) { ?>
                                                                                <?php if($row->id == $row3->km_detail_id) { ?> 
                                                                                    <td><?php echo $row3->target; ?></td>
                                                                                    <td>
                                                                                        <a 
                                                                                            id="link<?php echo $row3->id;?>"
                                                                                            href="#" data-toggle="modal" data-target="#result-modal"  style="color: black; <?php if($row3->result !== NULL) { echo "display: none;";}?>"
                                                                                            data-id="<?php echo $row3->id;?>"
                                                                                            data-km_id="<?php echo $row3->km_id;?>"
                                                                                            data-km_detail_id="<?php echo $row3->km_detail_id;?>"
                                                                                            data-periode_bulan_id="<?php echo $row3->periode_bulan_id;?>"
                                                                                            data-weight="<?php echo $row->weight;?>"
                                                                                            data-targets="<?php echo $row3->target;?>"
                                                                                            class="open-AddBookDialog1"
                                                                                            > 
                                                                                            <i class="fa fa-pencil"></i>
                                                                                        </a>
                                                                                        <span id="result<?php echo $row3->id;?>" style="text-decoration: underline; cursor: pointer;"
                                                                                            href="#" data-toggle="modal" data-target="#result-modal"  style="color: white;" 
                                                                                            data-id="<?php echo $row3->id;?>"
                                                                                            data-km_id="<?php echo $row3->km_id;?>"
                                                                                            data-km_detail_id="<?php echo $row3->km_detail_id;?>"
                                                                                            data-weight="<?php echo $row->weight;?>"
                                                                                            data-targets="<?php echo $row3->target;?>"
                                                                                            class="open-AddBookDialog2"><?php echo $row3->result;?>
                                                                                        </span>
                                                                                    </td> 
                                                                                    <td><span id="score<?php echo $row3->id;?>"><?php echo $row3->score;?></span></td> 
                                                                                    <td><span id="ach<?php echo $row3->id;?>"><?php echo $row3->ach;?></span></td>
                                                                                    <?php $km_id = $row3->km_id; $km_detail_id = $row3->km_detail_id;?>
                                                                                <?php } ?>  
                                                                            <?php } ?>    
																		<?php } ?> 
                                                                        <td><span id="total<?php echo $row->id;?>"></span></td>
                                                                        <td>
                                                                            <a 
                                                                                id="link<?php echo $row3->id;?>"
                                                                                href="#" data-toggle="modal" data-target="#upload-modal"  style="color: black;"
                                                                                data-km_id="<?php echo $km_id;?>"
                                                                                data-km_detail_id="<?php echo $km_detail_id;?>"
                                                                                class="open-Upload1"> 
                                                                                <i class="fa fa-upload"></i>
                                                                            </a> | 
                                                                            <a href="<?php echo base_url()?>assets/upload/evidence/<?php echo $row->file_path;?>" target="_blank"> 
																				<?php 
																					if($row->file_type == "DOCX") { 
																						echo '<i class="fa fa-file-word-o"></i>';
																					}else if($row->file_type == "XLSX"){
																						echo '<i class="fa fa-file-excel-o"></i>';
																					}else if($row->file_type == "PPTX"){
																						echo '<i class="fa fa-file-powerpoint-o"></i>';
																					}else if($row->file_type == "PDF"){
																						echo '<i class="fa fa-file-pdf-o"></i>';
																					}else if($row->file_type == "RAR"){
																						echo '<i class="fa fa-file-zip-o"></i>';
																					}else if($row->file_type == "Image"){
																						echo '<i class="fa fa-file-pcture-o"></i>';
																					}else if($row->file_type == "Lainnya"){
																						echo '<i class="fa fa-file-o"></i>';
																					}
																				?>
                                                                            </a> 
                                                                        </td>  
																	</tr>  		 
																	<?php } ?>  
																	<?php if($row->level == 4) { $level4++; $km_id=0; $km_detail_id=0;?>	 
																	<tr>
																		<td></th> 
																		<td width="20"></td>
																		<td width="250" style="white-space: pre-wrap;"><?php echo $level4; ?>. <?php echo $row->kpi; ?></td>
																		<td><?php echo $row->weight; ?></td>
																		<td><?php echo $row->unit; ?></td>
																		<?php foreach($periode2 as $row2) { ?>  
                                                                            <?php foreach($row2['nilai'] as $row3) { ?>
                                                                                <?php if($row->id == $row3->km_detail_id) { ?> 
                                                                                    <td><?php echo $row3->target; ?></td>
                                                                                    <td>
                                                                                        <a 
                                                                                            id="link<?php echo $row3->id;?>"
                                                                                            href="#" data-toggle="modal" data-target="#result-modal"  style="color: black; <?php if($row3->result !== NULL) { echo "display: none;";}?>"
                                                                                            data-id="<?php echo $row3->id;?>"
                                                                                            data-km_id="<?php echo $row3->km_id;?>"
                                                                                            data-km_detail_id="<?php echo $row3->km_detail_id;?>"
                                                                                            data-periode_bulan_id="<?php echo $row3->periode_bulan_id;?>"
                                                                                            data-weight="<?php echo $row->weight;?>"
                                                                                            data-targets="<?php echo $row3->target;?>"
                                                                                            class="open-AddBookDialog1"> 
                                                                                            <i class="fa fa-pencil"></i>
                                                                                        </a>
                                                                                        <span id="result<?php echo $row3->id;?>" style="text-decoration: underline; cursor: pointer;"
                                                                                            href="#" data-toggle="modal" data-target="#result-modal"  style="color: white;" 
                                                                                            data-id="<?php echo $row3->id;?>"
                                                                                            data-km_id="<?php echo $row3->km_id;?>"
                                                                                            data-km_detail_id="<?php echo $row3->km_detail_id;?>"
                                                                                            data-weight="<?php echo $row->weight;?>"
                                                                                            data-targets="<?php echo $row3->target;?>"
                                                                                            class="open-AddBookDialog2"><?php echo $row3->result;?>
                                                                                        </span>
                                                                                    </td> 
                                                                                    <td><span id="score<?php echo $row3->id;?>"><?php echo $row3->score;?></span></td> 
                                                                                    <td><span id="ach<?php echo $row3->id;?>"><?php echo $row3->ach;?></span></td>
                                                                                    <?php $km_id = $row3->km_id; $km_detail_id = $row3->km_detail_id;?>
                                                                                <?php } ?>   
																			<?php } ?>    
																		<?php } ?> 
                                                                        <td><span id="total<?php echo $row->id;?>"></span></td>
                                                                        <td>
                                                                            <a 
                                                                                id="link<?php echo $row3->id;?>"
                                                                                href="#" data-toggle="modal" data-target="#upload-modal"  style="color: black;"
                                                                                data-km_id="<?php echo $km_id;?>"
                                                                                data-km_detail_id="<?php echo $km_detail_id;?>"
                                                                                class="open-Upload1"> 
                                                                                <i class="fa fa-upload"></i>
                                                                            </a> | 
                                                                            <a href="<?php echo base_url()?>assets/upload/evidence/<?php echo $row->file_path;?>" target="_blank"> 
																				<?php 
																					if($row->file_type == "DOCX") { 
																						echo '<i class="fa fa-file-word-o"></i>';
																					}else if($row->file_type == "XLSX"){
																						echo '<i class="fa fa-file-excel-o"></i>';
																					}else if($row->file_type == "PPTX"){
																						echo '<i class="fa fa-file-powerpoint-o"></i>';
																					}else if($row->file_type == "PDF"){
																						echo '<i class="fa fa-file-pdf-o"></i>';
																					}else if($row->file_type == "RAR"){
																						echo '<i class="fa fa-file-zip-o"></i>';
																					}else if($row->file_type == "Image"){
																						echo '<i class="fa fa-file-pcture-o"></i>';
																					}else if($row->file_type == "Lainnya"){
																						echo '<i class="fa fa-file-o"></i>';
																					}
																				?>
                                                                            </a>
                                                                        </td>  
																	</tr>  		 
																	<?php } ?>  
                                                                <?php } ?>
                                                                <tr>
                                                                    <td colspan="3">Total</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td colspan="12"></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
															</tbody>
														</table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="result-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content"> 
                                <div class="modal-body p-b-0">
                                    <form id="result" name="result">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div>
                                                    <label class="form-control-label">Result</label>
                                                    <input type="hidden" id="modal-id" name="modal-id" class="form-control" placeholder="Result">
                                                    <input type="hidden" id="modal-km_id" name="modal-km_id" class="form-control" placeholder="Result">
                                                    <input type="hidden" id="modal-km_detail_id" name="modal-km_detail_id" class="form-control" placeholder="Result">
                                                    <input type="hidden" id="modal-periode_bulan_id" name="modal-periode_bulan_id" class="form-control" placeholder="Result">
                                                    <input type="hidden" id="modal-weight" name="modal-weight" class="form-control" placeholder="Result">
                                                    <input type="hidden" id="modal-targets" name="modal-targets" class="form-control" placeholder="Result">
                                                    <input type="text" id="modal-result" name="modal-result" class="form-control" placeholder="Result">
                                                    <br>
                                                </div>
                                            </div>
                                        </div>  
                                        <div class="row">
                                            <div class="col-sm-12" style="text-align: right;">
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                                <button type="button" class="btn btn-success" data-dismiss="modal" onclick="javascript:hitung();">Simpan</button>
                                                <br>
                                                <br>
                                            </div>
                                        </div>
                                    </form>
                                </div>  
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="upload-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content"> 
                                <div class="modal-body p-b-0">
                                    <?php
                                        $attributes = array('name' => 'evidence', 'type'=>'post', 'id' => 'evidence', 'class'=>'block-content form');
                                        echo form_open_multipart('evidence/insert', $attributes);
                                    ?> 
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div> 
                                                    <label class="form-control-label">Nama</label>
                                                    <input type="hidden" id="upload_km_id" name="upload_km_id" class="form-control" placeholder="Nama" value="">
                                                    <input type="hidden" id="upload_km_detail_id" name="upload_km_detail_id" class="form-control" placeholder="Nama" value="">
                                                    <input type="text" id="upload_nama" name="upload_nama" class="form-control" placeholder="Nama">
                                                    <br> 
                                                </div>
                                                <div> 
                                                    <label class="form-control-label">File Type</label>
                                                    <select name="upload_file_type" id="upload_file_type" class="form-control">
                                                        <option value="">-- Pilih File Type --</option> 
                                                        <option value="DOCX">DOCX</option> 
                                                        <option value="XLSX">XLSX</option> 
                                                        <option value="PPTX">PPTX</option> 
                                                        <option value="PDF">PDF</option> 
                                                        <option value="RAR">RAR</option> 
                                                        <option value="Image">Image</option> 
                                                        <option value="Lainnya">Lainnya</option> 
                                                    </select>
                                                    <br> 
                                                </div>
                                                <div> 
                                                    <label class="form-control-label">File </label>
                                                    <input type="file" id="userfile" name="userfile" class="form-control" placeholder="File">
                                                    <br> 
                                                </div>
                                            </div>
                                        </div>  
                                        <div class="row">
                                            <div class="col-sm-12" style="text-align: right;">
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                                <button type="submit" class="btn btn-success">Upload</button>
                                                <br>
                                                <br>
                                            </div>
                                        </div>
                                    </form>
                                </div>  
                            </div>
                        </div>
                    </div>


                    <script src="<?php echo base_url();?>assets/assets/pages/wysiwyg-editor/js/tinymce.min.js"></script>
                    <!-- <script src="<?php echo base_url();?>assets/assets/pages/wysiwyg-editor/wysiwyg-editor.js"></script> -->
                    <script type="text/javascript">
     
                        function batal(){
                            document.location.href = '<?php echo site_url().'bidang'?>';
                        }
                         

                        function hitung(){
                            console.log("hitung");

                            var id = document.getElementById("modal-id").value;
                            var km_id = document.getElementById("modal-km_id").value;
                            var km_detail_id = document.getElementById("modal-km_detail_id").value;
                            var periode_bulan_id = document.getElementById("modal-periode_bulan_id").value;
                            var result = document.getElementById("modal-result").value;
                            var weight = document.getElementById("modal-weight").value;
                            var targets = document.getElementById("modal-targets").value;
                            console.log(id);
                            console.log(km_id);
                            console.log(km_detail_id);
                            console.log(periode_bulan_id);
                            console.log(result);
                            console.log(weight);
                            console.log(targets);

                            var score1 = Number(result) * 100 / Number(targets);
                            var score2 = Math.max(Math.min(Number(score1), 105), 95)
                            var ach = Number(weight) * Number(score2) / 100;
                            console.log("====")
                            console.log(score1)
                            console.log(score2)
                            console.log(ach)
 
                            $("#result"+id).text(parseFloat(result).toFixed(2));
                            $("#score"+id).text(parseFloat(score2).toFixed(2));
                            $("#ach"+id).text(parseFloat(ach).toFixed(2));

                            $('#link'+id).css('display', 'none');

                            $.post(
                                '<?php echo base_url(); ?>km_nilai/update',
                                {"id": id ,"result" : result ,"score" :score2 , "ach" :ach },
                                function(response, status){
                                    console.log(response);
                                    console.log(status);
                                });

                            // $.ajax({
                            //     type: "POST", 
                            //     url: '<?php echo base_url(); ?>km_nilai/update',
                            //     contentType: 'application/json',
                            //     async: false, 
                            //     data: JSON.stringify({"id": id ,"result" : result ,"score" :score2 , "ach" :ach }),
                            //     success: function (data ) {
                            //         console.log(data );
                            //     }
                            // });
                        }

                        $(document).on("click", ".open-AddBookDialog1", function () {
                            console.log("ke satu");
                            var id = $(this).data('id');
                            $(".modal-body #modal-id").val(  $(this).data('id') );
                            $(".modal-body #modal-km_id").val(  $(this).data('km_id') );
                            $(".modal-body #modal-km_detail_id").val(  $(this).data('km_detail_id') );
                            $(".modal-body #modal-periode_bulan_id").val(  $(this).data('periode_bulan_id') );
                            $(".modal-body #modal-weight").val(  $(this).data('weight') );
                            $(".modal-body #modal-targets").val(  $(this).data('targets') );
                            // As pointed out in comments, 
                            // it is unnecessary to have to manually call the modal.
                            // $('#addBookDialog').modal('show');
                        });

                        
                        $(document).on("click", ".open-AddBookDialog2", function () {
                            console.log("ke dua");
                            var id = $(this).data('id');
                            $(".modal-body #modal-id").val(  $(this).data('id') );
                            $(".modal-body #modal-km_id").val(  $(this).data('km_id') );
                            $(".modal-body #modal-km_detail_id").val(  $(this).data('km_detail_id') );
                            $(".modal-body #modal-periode_bulan_id").val(  $(this).data('periode_bulan_id') );
                            $(".modal-body #modal-weight").val(  $(this).data('weight') );
                            $(".modal-body #modal-targets").val(  $(this).data('targets') );
                            // As pointed out in comments, 
                            // it is unnecessary to have to manually call the modal.
                            // $('#addBookDialog').modal('show');
                        });

                        $(document).on("click", ".open-Upload1", function () {
                            console.log("ke satu");
                            var id = $(this).data('id'); 
                            $(".modal-body #upload_km_id").val(  $(this).data('km_id') );
                            $(".modal-body #upload_km_detail_id").val(  $(this).data('km_detail_id') );  
                            // As pointed out in comments, 
                            // it is unnecessary to have to manually call the modal.
                            // $('#addBookDialog').modal('show');
                        });

                    </script>

