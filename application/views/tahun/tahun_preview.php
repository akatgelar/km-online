                                        
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">

                            <div class="main-body">
                                <div class="page-wrapper">
                                            

                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5>Kelola Data Tahun</h5>
                                                        <span>Di halaman ini, Anda dapat mengelola data tahun yang ada di website Anda.</span>
                                                        <div class="card-header-right">
                                                            <i class="icofont icofont-rounded-down"></i>
                                                            <i class="icofont icofont-refresh"></i>
                                                            <i class="icofont icofont-close-circled"></i>
                                                        </div>
                                                    </div>
                                                    <div class="card-block">
														<?php $i=0; foreach($results->result() as $row) {$i++;?>	
															<div class="view-info">
																<div class="row">
																	<div class="col-lg-12">
																		<div class="general-info">
																			<div class="row"> 
																				<div class="col-lg-8 col-xl-8">
																				<div class="table-responsive">
																				<table class="table m-0">
																					<tbody>
																						<tr>
																							<th scope="row">ID</th>
																							<td><?php echo $row->id; ?></td>
																						</tr> 
																						<tr>
																							<th scope="row">Tahun</th>
																							<td><?php echo $row->tahun;?></td>
																						</tr>  
																						<tr>
																							<th scope="row">Keterangan</th>
																							<td><?php echo $row->keterangan; ?></td>
																						</tr>
																						<tr>
																							<th scope="row">Tanggal Diposting</th>
																							<td><?php echo indo_tgl_jam($row->cdate); ?></td>
																						</tr>
																					</tbody>
																				</table>
																				<br>
																				<br>
																				</div>
																				</div>
																				
																				
																				<div class="col-lg-12 col-xl-12">
																					<div class="view-desc">
																						<hr/>
																					</div>
																				</div>
																			</div>
																			<!-- end of row -->
																		</div>
																		<!-- end of general info -->
																	</div>
																	<!-- end of col-lg-12 -->
																</div>
																<!-- end of row -->
															</div>
															
														<?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    
