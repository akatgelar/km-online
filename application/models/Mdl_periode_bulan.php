<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Mdl_periode_bulan extends CI_Model{

	

	function __construct()
	{
		parent::__construct();
	}

	
	function get_data(){

		$this->db->flush_cache();
		$this->db->select('periode_bulan.*, tahun.tahun as tahun, periode.nama_periode as periode');
		$this->db->from('periode_bulan');   
		$this->db->join('periode', 'periode_bulan.periode_id = periode.id');   
		$this->db->join('tahun', 'periode.tahun_id = tahun.id');   
		$this->db->order_by("periode_bulan.id", "DESC");  
		return $this->db->get();

	}
	 
	
	function get_data_where($id){

		$this->db->flush_cache();
		$this->db->select('periode_bulan.*, tahun.tahun as tahun, periode.nama_periode as periode');
		$this->db->from('periode_bulan');   
		$this->db->join('periode', 'periode_bulan.periode_id = periode.id');    
		$this->db->join('tahun', 'periode.tahun_id = tahun.id');  
		$this->db->where('periode_bulan.id', $id);   
		return $this->db->get();

	}
	
		
	function insert($data)
	{

		$this->db->flush_cache();
		$this->db->insert('periode_bulan', $data);

	}
	

	function update($id, $data)
	{
 
		$this->db->flush_cache();
		$this->db->where('id', $id);
		$this->db->update('periode_bulan', $data);

	}
	
	function delete($id)
	{

		$this->db->flush_cache();
		$this->db->delete('periode_bulan', array('id' => $id));

	}
	 
}

