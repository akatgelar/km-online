<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Mdl_user extends CI_Model{

	

	function __construct()
	{
		parent::__construct();
	}

	
	function get_data(){

		$this->db->flush_cache();
		$this->db->select('user.*, user_level.nama, user_token.datetime');
		$this->db->from('user');  
		$this->db->join('user_level', 'user_level.id = user.level_id');
		$this->db->join('user_token', 'user_token.user_id = user.id','left');
		$this->db->order_by("user.id", "DESC");  
		return $this->db->get();

	}
	 
	
	function get_data_where($id){

		$this->db->flush_cache();
		$this->db->select('user.*, user_level.nama, user_token.datetime');
		$this->db->from('user');  
		$this->db->join('user_level', 'user_level.id = user.level_id');
		$this->db->join('user_token', 'user_token.user_id = user.id','left');
		$this->db->where('user.id', $id);   
		return $this->db->get();

	}
	
	
	function get_level(){

		$this->db->flush_cache();
		$this->db->select('user_level.*');
		$this->db->from('user_level');   
		$this->db->order_by("nama", "ASC");  
		return $this->db->get();

	}
	
		
	function insert($data)
	{

		$this->db->flush_cache();
		$this->db->insert('user', $data);

	}
	

	function update($id, $data)
	{
 
		$this->db->flush_cache();
		$this->db->where('id', $id);
		$this->db->update('user', $data);

	}
	
	function delete($id)
	{

		$this->db->flush_cache();
		$this->db->delete('user', array('id' => $id));

	}
	 
}

