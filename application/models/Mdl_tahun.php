<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Mdl_tahun extends CI_Model{

	

	function __construct()
	{
		parent::__construct();
	}

	
	function get_data(){

		$this->db->flush_cache();
		$this->db->select('tahun.*');
		$this->db->from('tahun');   
		$this->db->order_by("tahun.id", "ASC");  
		return $this->db->get();

	}
	 
	
	function get_data_where($id){

		$this->db->flush_cache();
		$this->db->select('tahun.*');
		$this->db->from('tahun');   
		$this->db->where('tahun.id', $id);   
		return $this->db->get();

	}
	
		
	function insert($data)
	{

		$this->db->flush_cache();
		$this->db->insert('tahun', $data);

	}
	

	function update($id, $data)
	{
 
		$this->db->flush_cache();
		$this->db->where('id', $id);
		$this->db->update('tahun', $data);

	}
	
	function delete($id)
	{

		$this->db->flush_cache();
		$this->db->delete('tahun', array('id' => $id));

	}
	 
}

