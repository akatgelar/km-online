<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Mdl_auth extends CI_Model{

	function __construct()
	{
		parent::__construct();
	}

	
	function get_user($username, $password){

		$this->db->flush_cache();
		$this->db->select('user.*');
		$this->db->from('user');
		$this->db->where('username', $username);
		$this->db->where('password', md5($password));
		return $this->db->get();

	}

	

	function set_token($user_id, $token, $datetime){

		$this->db->flush_cache();
		$this->db->where('user_id', $user_id);
		$this->db->delete('user_token');

		$this->db->flush_cache();
		$this->db->set('user_id', $user_id);
		$this->db->set('token', $token);
		$this->db->set('datetime', $datetime);
		$this->db->insert('user_token');

	}
	
	
	function get_userByToken($token){

		$this->db->flush_cache();
		$this->db->where('token', $token);
		return $this->db->get('user_token');

	}

	 
	
	function get_logged_level($user_id){

		$this->db->flush_cache();
		$this->db->select('user.username AS `nama`, user_level.nama AS level, user_level.id as level_id');
		$this->db->from('user'); 
		$this->db->join('user_level', 'user_level.id = user.level_id', 'INNER');
		$this->db->where('user.id', $user_id);
		$query = $this->db->get();
		return $query->row()->level_id;

	}
	 

	
	function get_token($user_id ){

		$this->db->select('user_token.*');
		$this->db->from('user_token');
		$this->db->where('user_id', $user_id);
		$query = $this->db->get();
		return $query->row()->datetime;
	}

	

	

}

