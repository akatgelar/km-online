<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Mdl_bidang extends CI_Model{

	

	function __construct()
	{
		parent::__construct();
	}

	
	function get_data(){

		$this->db->flush_cache();
		$this->db->select('bidang.*, bidang_kategori.nama_pendek as kategori');
		$this->db->from('bidang');   
		$this->db->join('bidang_kategori', 'bidang.kategori_id = bidang_kategori.id');   
		$this->db->order_by("bidang.id", "DESC");  
		return $this->db->get();

	}
	 
	
	function get_data_where($id){

		$this->db->flush_cache();
		$this->db->select('bidang.*, bidang_kategori.nama_pendek as kategori');
		$this->db->from('bidang');   
		$this->db->join('bidang_kategori', 'bidang.kategori_id = bidang_kategori.id');   
		$this->db->where('bidang.id', $id);   
		return $this->db->get();

	}
	
	function get_data_where_kategori($id){

		$this->db->flush_cache();
		$this->db->select('bidang.*, bidang_kategori.nama_pendek as kategori');
		$this->db->from('bidang');   
		$this->db->join('bidang_kategori', 'bidang.kategori_id = bidang_kategori.id');   
		$this->db->where('bidang_kategori.id', $id);   
		return $this->db->get();

	}
		
	function insert($data)
	{

		$this->db->flush_cache();
		$this->db->insert('bidang', $data);

	}
	

	function update($id, $data)
	{
 
		$this->db->flush_cache();
		$this->db->where('id', $id);
		$this->db->update('bidang', $data);

	}
	
	function delete($id)
	{

		$this->db->flush_cache();
		$this->db->delete('bidang', array('id' => $id));

	}
	 
}

