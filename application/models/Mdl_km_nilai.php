<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Mdl_km_nilai extends CI_Model{

	

	function __construct()
	{
		parent::__construct();
	}

	
	function get_data(){

		$this->db->flush_cache();
		$this->db->select('km.id, km.status, km.bidang_id, bidang.nama_pendek, bidang.nama_panjang, 
			km.tahun_id, tahun.tahun,
			km.periode_id, periode.nama_periode, periode.keterangan');
		$this->db->from('km');    
		$this->db->join('bidang', 'bidang.id = km.bidang_id');    
		$this->db->join('tahun', 'tahun.id = km.tahun_id');    
		$this->db->join('periode', 'periode.id = km.periode_id');      
		$this->db->where('km.status', 'saved');      
		
		$data['userlevel'] = get_instance()->session->userdata('userlevel');
		$data['bidang_id'] = get_instance()->session->userdata('bidang_id');
		if($data['userlevel'] !== "ADM"){ 
			$this->db->where('km.bidang_id', $data['bidang_id']);      
		}   
		
		$this->db->order_by("km.bidang_id", "DESC");  
		$this->db->order_by("km.tahun_id", "DESC");  
		$this->db->order_by("km.periode_id", "DESC");  
		$result = $this->db->get(); 
		return $result;

	}
	 
	
	function get_data_where($id){

		$this->db->flush_cache();
		$this->db->select('km.*');
		$this->db->from('km');    
		$this->db->where('km.id', $id);     
		return $this->db->get();

	}

	function get_data_detail($km_id){

		$this->db->flush_cache();
		$this->db->select('km_detail.*, evidence.nama, evidence.file_type, evidence.file_path');
		$this->db->from('km_detail');    
		$this->db->join('evidence', 'km_detail.id = evidence.km_detail_id', 'left');      
		$this->db->where('km_detail.km_id', $km_id);     
		$this->db->order_by("km_detail.id", "ASC");   
		return $this->db->get();

	}

	function get_data_periode($periode_id){

		$this->db->flush_cache();
		$this->db->select('periode_bulan.*');
		$this->db->from('periode_bulan');    
		$this->db->where('periode_bulan.periode_id', $periode_id);     
		return $this->db->get();

	}

	function get_data_nilai($km_id, $periode_bulan_id){

		$this->db->flush_cache();
		$this->db->select('km_nilai.*');
		$this->db->from('km_nilai');    
		$this->db->where('km_nilai.km_id', $km_id);     
		$this->db->where('km_nilai.periode_bulan_id', $periode_bulan_id);     
		return $this->db->get();

	}
	
	function get_data_where_kategori($id){

		$this->db->flush_cache();
		$this->db->select('km.*');
		$this->db->from('km');    
		$this->db->where('km_kategori.id', $id);   
		return $this->db->get();

	}
		
	function insert($data)
	{

		$this->db->flush_cache();
		$this->db->insert('km_nilai', $data);

	}
	

	function update($id, $data)
	{
 
		$this->db->flush_cache();
		$this->db->where('id', $id);
		$this->db->update('km_nilai', $data);

	}
	
	function delete($id)
	{

		$this->db->flush_cache();
		$this->db->delete('km_nilai', array('id' => $id));

	}
	 
}

