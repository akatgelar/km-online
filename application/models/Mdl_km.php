<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Mdl_km extends CI_Model{

	

	function __construct()
	{
		parent::__construct();
	}

	
	function get_data(){

		$this->db->flush_cache();
		$this->db->select('km.id, km.status, km.bidang_id, bidang.nama_pendek, bidang.nama_panjang, 
			km.tahun_id, tahun.tahun,
			km.periode_id, periode.nama_periode, periode.keterangan');
		$this->db->from('km');    
		$this->db->join('bidang', 'bidang.id = km.bidang_id');    
		$this->db->join('tahun', 'tahun.id = km.tahun_id');    
		$this->db->join('periode', 'periode.id = km.periode_id');      
		$this->db->order_by("km.bidang_id", "DESC");  
		$this->db->order_by("km.tahun_id", "DESC");  
		$this->db->order_by("km.periode_id", "DESC");  
		$result = $this->db->get(); 
		// echo $this->db->last_query();
		return $result;

	}
	 
	
	function get_data_where($id){

		$this->db->flush_cache();
		$this->db->select('km.*');
		$this->db->from('km');    
		$this->db->where('km.id', $id);     
		return $this->db->get();

	}

	function get_data_detail($km_id){

		$this->db->flush_cache();
		$this->db->select('km_detail.*');
		$this->db->from('km_detail');    
		$this->db->where('km_detail.km_id', $km_id);    
		$this->db->order_by("km_detail.id", "ASC");   
		return $this->db->get();

	}

	function get_data_periode($periode_id){

		$this->db->flush_cache();
		$this->db->select('periode_bulan.*');
		$this->db->from('periode_bulan');    
		$this->db->where('periode_bulan.periode_id', $periode_id);     
		return $this->db->get();

	}

	function get_data_nilai($km_id, $periode_bulan_id){

		$this->db->flush_cache();
		$this->db->select('km_nilai.*');
		$this->db->from('km_nilai');    
		$this->db->where('km_nilai.km_id', $km_id);     
		$this->db->where('km_nilai.periode_bulan_id', $periode_bulan_id);     
		return $this->db->get();

	}

	
	function get_data_detail_nilai($km_detail_id, $periode_bulan_id){

		$this->db->flush_cache();
		$this->db->select('km_nilai.*');
		$this->db->from('km_nilai');    
		$this->db->where('km_nilai.km_detail_id', $km_detail_id);     
		$this->db->where('km_nilai.periode_bulan_id', $periode_bulan_id);     
		return $this->db->get();

	}
	
	function get_data_where_kategori($id){

		$this->db->flush_cache();
		$this->db->select('km.*');
		$this->db->from('km');    
		$this->db->where('km_kategori.id', $id);   
		return $this->db->get();

	}
		
	function insert_km($data)
	{

		$this->db->flush_cache();
		$this->db->insert('km', $data);
		$insert_id = $this->db->insert_id();

		return  $insert_id;

	}

	function insert_km_detail($data)
	{

		$this->db->flush_cache();
		$this->db->insert('km_detail', $data);
		$insert_id = $this->db->insert_id();

		return  $insert_id;

	}
	

	function update_km($id, $data)
	{
 
		$this->db->flush_cache();
		$this->db->where('id', $id);
		$this->db->update('km', $data);
		

	}
	
	function delete($id)
	{

		$this->db->flush_cache();
		$this->db->delete('km', array('id' => $id));
		$this->db->delete('km_detail', array('km_id' => $id));
		$this->db->delete('km_nilai', array('km_id' => $id));

	}

	
	function delete_km_detail($id)
	{

		$this->db->flush_cache(); 
		$this->db->delete('km_detail', array('km_id' => $id)); 

	}

	
	function delete_km_nilai($id)
	{

		$this->db->flush_cache(); 
		$this->db->delete('km_nilai', array('km_id' => $id));

	}
	 
	 
}

