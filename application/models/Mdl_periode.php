<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Mdl_periode extends CI_Model{

	

	function __construct()
	{
		parent::__construct();
	}

	
	function get_data(){

		$this->db->flush_cache();
		$this->db->select('periode.*, tahun.tahun as tahun');
		$this->db->from('periode');   
		$this->db->join('tahun', 'periode.tahun_id = tahun.id');   
		$this->db->order_by("periode.id", "DESC");  
		return $this->db->get();

	}
	 
	
	function get_data_where($id){

		$this->db->flush_cache();
		$this->db->select('periode.*, tahun.tahun as tahun');
		$this->db->from('periode');   
		$this->db->join('tahun', 'periode.tahun_id = tahun.id');   
		$this->db->where('periode.id', $id);   
		return $this->db->get();

	}
	
		
	function insert($data)
	{

		$this->db->flush_cache();
		$this->db->insert('periode', $data);

	}
	

	function update($id, $data)
	{
 
		$this->db->flush_cache();
		$this->db->where('id', $id);
		$this->db->update('periode', $data);

	}
	
	function delete($id)
	{

		$this->db->flush_cache();
		$this->db->delete('periode', array('id' => $id));

	}
	 
}

