<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Mdl_bidang_kategori extends CI_Model{

	

	function __construct()
	{
		parent::__construct();
	}

	
	function get_data(){

		$this->db->flush_cache();
		$this->db->select('bidang_kategori.*');
		$this->db->from('bidang_kategori');   
		$this->db->order_by("bidang_kategori.id", "ASC");  
		return $this->db->get();

	}
	 
	
	function get_data_where($id){

		$this->db->flush_cache();
		$this->db->select('bidang_kategori.*');
		$this->db->from('bidang_kategori');   
		$this->db->where('bidang_kategori.id', $id);   
		return $this->db->get();

	}
	
		
	function insert($data)
	{

		$this->db->flush_cache();
		$this->db->insert('bidang_kategori', $data);

	}
	

	function update($id, $data)
	{
 
		$this->db->flush_cache();
		$this->db->where('id', $id);
		$this->db->update('bidang_kategori', $data);

	}
	
	function delete($id)
	{

		$this->db->flush_cache();
		$this->db->delete('bidang_kategori', array('id' => $id));

	}
	 
}

