<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Mdl_evidence extends CI_Model{

	

	function __construct()
	{
		parent::__construct();
	}

	
	function get_data(){

		$this->db->flush_cache();
		$this->db->select('evidence.*, km_detail.kpi, bidang.nama_panjang, bidang.nama_pendek, tahun.tahun, periode.nama_periode');
		$this->db->from('evidence');   
		$this->db->join('km', 'evidence.km_id = km.id');    
		$this->db->join('km_detail', 'evidence.km_detail_id = km_detail.id');    
		$this->db->join('bidang', 'km.bidang_id = bidang.id');    
		$this->db->join('tahun', 'km.tahun_id = tahun.id');    
		$this->db->join('periode', 'km.periode_id = periode.id');    
		$this->db->order_by("evidence.id", "DESC");  
		return $this->db->get();

	}
	 
	
	function get_data_where($id){

		$this->db->flush_cache();
		$this->db->select('evidence.*');
		$this->db->from('evidence');   
		$this->db->where('evidence.id', $id);   
		return $this->db->get();

	}
	
		
	function insert($data)
	{

		$this->db->flush_cache();
		$this->db->insert('evidence', $data);

	}
	

	function update($km_id, $km_detail_id, $data)
	{
 
		$this->db->flush_cache();
		$this->db->where('km_id', $km_id);
		$this->db->where('km_detail_id', $km_detail_id);
		$this->db->update('evidence', $data);

	}
	
	function delete($id)
	{

		$this->db->flush_cache();
		$this->db->delete('evidence', array('id' => $id));

	}
	 
}

