/*
 Navicat Premium Data Transfer

 Source Server         : MySQL Local
 Source Server Type    : MySQL
 Source Server Version : 100139
 Source Host           : localhost:3306
 Source Schema         : telkom-km-online

 Target Server Type    : MySQL
 Target Server Version : 100139
 File Encoding         : 65001

 Date: 28/10/2019 05:38:04
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for _km_target
-- ----------------------------
DROP TABLE IF EXISTS `_km_target`;
CREATE TABLE `_km_target`  (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `periode_id` int(4) NULL DEFAULT NULL,
  `bulan_id` int(4) NULL DEFAULT NULL,
  `km_id` int(4) NOT NULL,
  `weight` float NULL DEFAULT NULL,
  `unit` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `target` float NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `cuid` int(4) NULL DEFAULT NULL,
  `cdate` datetime(0) NULL DEFAULT NULL,
  `muid` int(4) NULL DEFAULT NULL,
  `mdate` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 55 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of _km_target
-- ----------------------------
INSERT INTO `_km_target` VALUES (17, 26, 26, 17, NULL, NULL, NULL, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (18, 26, 26, 18, 20, '%', NULL, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (19, 26, 26, 19, NULL, NULL, NULL, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (20, 26, 26, 20, 3, '%', 100, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (21, 26, 26, 21, NULL, NULL, NULL, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (22, 26, 26, 22, 10, '%', 99.9, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (23, 26, 26, 23, 8, '%', 99.9, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (24, 26, 26, 24, 4, '%', 99.9, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (25, 26, 26, 25, NULL, NULL, NULL, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (26, 26, 26, 26, 2, '%', 1, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (27, 26, 26, 27, 2, '%', 100, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (28, 26, 26, 28, NULL, NULL, 100, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (29, 26, 26, 29, 7, '%', 100, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (30, 26, 26, 30, NULL, NULL, NULL, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (31, 26, 26, 31, NULL, NULL, 98, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (32, 26, 26, 32, NULL, NULL, 98, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (33, 26, 26, 33, NULL, NULL, 92, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (34, 26, 26, 34, NULL, NULL, NULL, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (35, 26, 26, 35, 2, '%', 77, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (36, 26, 26, 36, 2, '%', 86, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (37, 26, 26, 37, 2, '%', 90, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (38, 26, 26, 38, 2, '%', 64, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (39, 26, 26, 39, 2, '%', 100, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (40, 26, 26, 40, 3, '%', NULL, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (41, 26, 26, 41, NULL, NULL, 40, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (42, 26, 26, 42, 5, '%', 97, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (43, 26, 26, 43, NULL, NULL, NULL, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (44, 26, 26, 44, 4, '%', 95, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (45, 26, 26, 25, 2, '%', 95, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (46, 26, 26, 46, 3, '%', NULL, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (47, 26, 26, 47, NULL, NULL, NULL, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (48, 26, 26, 48, NULL, NULL, NULL, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (49, 26, 26, 49, 2, '%', 100, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (50, 26, 26, 50, 2, '%', 100, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (51, 26, 26, 51, NULL, NULL, NULL, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (52, 26, 26, 52, 6, '%', NULL, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (53, 26, 26, 53, NULL, NULL, NULL, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `_km_target` VALUES (54, 26, 26, 54, 3, '%', 100, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');

-- ----------------------------
-- Table structure for bidang
-- ----------------------------
DROP TABLE IF EXISTS `bidang`;
CREATE TABLE `bidang`  (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `kategori_id` int(4) NULL DEFAULT NULL,
  `nama_panjang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_pendek` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `is_active` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  `cuid` int(4) NULL DEFAULT NULL,
  `cdate` datetime(0) NULL DEFAULT NULL,
  `muid` int(4) NULL DEFAULT NULL,
  `mdate` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 38 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bidang
-- ----------------------------
INSERT INTO `bidang` VALUES (26, 2, 'CFU/FU SUPPORT & ITSM', 'CIT', NULL, '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `bidang` VALUES (27, 2, 'BSS & CEM PLATFORM OPERATION', 'BPO', NULL, '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `bidang` VALUES (28, 2, 'ENTERPRISE & ANALYTIC PLATFORM OPERATION', 'EPO', NULL, '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `bidang` VALUES (29, 2, 'OSS PLATFORM OPERATION', 'OPO', NULL, '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:13:57');
INSERT INTO `bidang` VALUES (30, 2, 'SERVICE PLATFORM OPERATION', 'SPO', NULL, '1', 1, '2019-05-15 15:03:14', 1, '2019-05-15 15:03:14');
INSERT INTO `bidang` VALUES (31, 3, 'IT PLANNING & ARCHITECTURE', 'IPA', NULL, '1', 1, '2019-05-15 15:03:14', 1, '2019-05-15 15:03:14');
INSERT INTO `bidang` VALUES (32, 3, 'BSS & CEM PLATFORM DEVELOPMENT', 'BPD', NULL, '1', 1, '2019-05-15 15:03:14', 1, '2019-05-15 15:03:14');
INSERT INTO `bidang` VALUES (33, 3, 'ENTERPRISE & ANALYTIC PLATFORM DEVELOPMENT', 'EPD', NULL, '1', 1, '2019-05-15 15:03:14', 1, '2019-05-15 15:03:14');
INSERT INTO `bidang` VALUES (34, 3, 'OSS PLATFORM DEVELOPMENT', 'OPD', NULL, '1', 1, '2019-05-15 15:03:14', 1, '2019-05-15 15:03:14');
INSERT INTO `bidang` VALUES (35, 3, 'SERVICE PLATFORM DEVELOPMENT', 'SPD', NULL, '1', 1, '2019-05-15 15:03:14', 1, '2019-05-15 15:03:14');
INSERT INTO `bidang` VALUES (36, 4, 'GENERAL AFFAIR', 'GAF', NULL, '1', 1, '2019-05-15 15:03:14', 1, '2019-05-15 15:03:14');
INSERT INTO `bidang` VALUES (25, 1, 'Divisi IT', 'DIT', NULL, '1', 1, '2019-05-15 15:03:14', 1, '2019-05-15 15:03:14');

-- ----------------------------
-- Table structure for bidang_kategori
-- ----------------------------
DROP TABLE IF EXISTS `bidang_kategori`;
CREATE TABLE `bidang_kategori`  (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `nama_panjang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_pendek` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `is_active` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  `cuid` int(4) NULL DEFAULT NULL,
  `cdate` datetime(0) NULL DEFAULT NULL,
  `muid` int(4) NULL DEFAULT NULL,
  `mdate` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 31 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bidang_kategori
-- ----------------------------
INSERT INTO `bidang_kategori` VALUES (1, 'Divisi IT', 'DIT', 'Keseluruhan Bidang', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `bidang_kategori` VALUES (2, 'Operation', 'OPR', 'Grup Operation', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `bidang_kategori` VALUES (3, 'Development', 'DEV', 'Grup Development', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `bidang_kategori` VALUES (4, 'General Affair', 'GAF', 'Grup General Affair', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:13:57');

-- ----------------------------
-- Table structure for evidence
-- ----------------------------
DROP TABLE IF EXISTS `evidence`;
CREATE TABLE `evidence`  (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `km_id` int(4) NULL DEFAULT NULL,
  `km_detail_id` int(4) NULL DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `file_type` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `file_path` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `is_active` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  `cuid` int(4) NULL DEFAULT NULL,
  `cdate` datetime(0) NULL DEFAULT NULL,
  `muid` int(4) NULL DEFAULT NULL,
  `mdate` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 49 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of evidence
-- ----------------------------
INSERT INTO `evidence` VALUES (48, 107, 212, 'a', 'XLSX', 'evidence_2019.10.27-22.56.32-.xls', NULL, '0', 1, '2019-10-28 04:56:32', NULL, NULL);
INSERT INTO `evidence` VALUES (44, 107, 210, 'bb', 'PDF', 'evidence_2019.10.27-22.15.51-.pdf', NULL, '0', 1, '2019-10-28 04:15:50', NULL, NULL);

-- ----------------------------
-- Table structure for km
-- ----------------------------
DROP TABLE IF EXISTS `km`;
CREATE TABLE `km`  (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `bidang_id` int(4) NULL DEFAULT NULL,
  `tahun_id` int(255) NULL DEFAULT NULL,
  `periode_id` int(4) NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `status` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cuid` int(4) NULL DEFAULT NULL,
  `cdate` datetime(0) NULL DEFAULT NULL,
  `muid` int(4) NULL DEFAULT NULL,
  `mdate` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 108 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of km
-- ----------------------------
INSERT INTO `km` VALUES (54, 30, 2019, 26, NULL, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-10-28 02:42:58');
INSERT INTO `km` VALUES (55, 30, 2019, 27, NULL, 'draft', 1, '2019-09-03 12:13:22', 1, '2019-10-15 11:13:40');
INSERT INTO `km` VALUES (107, 30, 2019, 28, NULL, 'saved', 1, '2019-10-28 02:45:00', 1, '2019-10-28 02:45:52');
INSERT INTO `km` VALUES (106, 31, 2019, 26, NULL, 'saved', 1, '2019-10-15 09:30:31', 1, '2019-10-15 09:50:22');

-- ----------------------------
-- Table structure for km_detail
-- ----------------------------
DROP TABLE IF EXISTS `km_detail`;
CREATE TABLE `km_detail`  (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `km_id` int(4) NULL DEFAULT NULL,
  `level` int(4) NOT NULL,
  `parent` int(4) NULL DEFAULT NULL,
  `kpi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `weight` float NULL DEFAULT NULL,
  `unit` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `status` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cuid` int(4) NULL DEFAULT NULL,
  `cdate` datetime(0) NULL DEFAULT NULL,
  `muid` int(4) NULL DEFAULT NULL,
  `mdate` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 247 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of km_detail
-- ----------------------------
INSERT INTO `km_detail` VALUES (211, 107, 1, NULL, 'Customer', 0, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (209, 107, 1, NULL, 'Financial', 0, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (207, 54, 1, NULL, 'Learning & Growth', 0, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (208, 54, 2, 207, 'Aktivasi KIPAS Budaya', 3, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (206, 54, 2, 201, 'Compliance SOA/ICOFR/ITGC/Integrated Audit', 6, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (205, 54, 2, 201, 'Management Tindak Lanjut  (Completion of Milestones)', 0, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (204, 54, 2, 201, 'Pelaksanaan Disaster Recovery Test', 2, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (203, 54, 3, 202, 'Penyiapan dokumen pengadaan (justifikasi, ToR, BoQ)', 2, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (202, 54, 2, 201, 'Efektifitas DRP (Daftar Rencana Pengadaan)', 0, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (200, 54, 2, 173, 'Specific Collaborative (Support Needed RAPIM/Transformer)', 3, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (201, 54, 1, NULL, 'Internal Business Process', 0, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (198, 54, 3, 197, 'Task Compliance (Remedy)', 4, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (199, 54, 3, 197, 'Request Fulfillment (myproject.telkom.co.id)', 2, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (197, 54, 2, 173, 'Request Fulfillment ', 0, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (196, 54, 2, 173, 'Incident Resolution Time Compliance', 5, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (195, 54, 2, 173, 'First Call Resolution Time Compliance', 0, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (194, 54, 2, 173, 'Pengelolaan Digital Workplace (Inventory, Update, Dismantling)', 3, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (191, 54, 3, 188, 'MyIndihome Assurance', 2, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (192, 54, 3, 188, 'Billing Process', 2, 'jam', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (193, 54, 2, 173, 'DTP Development for Customer Experience & FAB', 2, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (190, 54, 3, 188, 'TTD Compliance BGES', 2, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (188, 54, 2, 173, 'Business Performance', 0, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (189, 54, 3, 188, 'PS Indihome <= 1 hari ', 2, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (187, 54, 3, 184, 'KPRO', 0, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (184, 54, 2, 173, 'Dashboard Performance', 0, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (185, 54, 3, 184, 'Dashboard Fulfillment', 0, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (186, 54, 3, 184, 'Dashboard Assurance', 0, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (183, 54, 2, 173, 'Security Awareness - Standard Verifikasi Keamanan Aplikasi (Compliance)', 7, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (182, 54, 2, 173, 'SQUADRON Progress Report', 0, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (180, 54, 3, 179, 'Response Time of Service (SQUADRON RToS)', 2, 'detik', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (181, 54, 3, 179, 'Response Time of Service (SLA & OLA Achievement)', 2, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (179, 54, 2, 173, 'Response Time of Service', 0, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (55, 55, 1, NULL, 'Financial', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (56, 55, 2, 17, 'UBIS CoE (O&M + G&A)', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (57, 55, 1, NULL, 'Customer', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (58, 55, 2, 19, 'SLA Information System', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (59, 55, 2, 19, 'Availability of Service', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (60, 55, 3, 21, 'Availability of Service (SQUADRON AoS)', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (61, 55, 3, 21, 'Availability of Service (SLA & OLA Achievement)', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (62, 55, 3, 21, 'Availability of Service (Integration)', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (63, 55, 2, 19, 'Response Time of Service', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (64, 55, 3, 25, 'Response Time of Service (SQUADRON RToS)', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (65, 55, 3, 25, 'Response Time of Service (SLA & OLA Achievement)', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (66, 55, 2, 19, 'SQUADRON Progress Report', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (67, 55, 2, 19, 'Security Awareness - Standard Verifikasi Keamanan Aplikasi (Compliance)', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (68, 55, 2, 19, 'Dashboard Performance', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (69, 55, 3, 30, 'Dashboard Fulfillment', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (70, 55, 3, 30, 'Dashboard Assurance', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (71, 55, 3, 30, 'KPRO', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (72, 55, 2, 19, 'Business Performance', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (73, 55, 3, 34, 'PS Indihome <= 1 hari ', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (74, 55, 3, 34, 'TTD Compliance BGES', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (75, 55, 3, 34, 'MyIndihome Assurance', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (76, 55, 3, 34, 'Billing Process', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (77, 55, 2, 19, 'DTP Development for Customer Experience & FAB', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (78, 55, 2, 19, 'Pengelolaan Digital Workplace (Inventory, Update, Dismantling)', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (79, 55, 2, 19, 'First Call Resolution Time Compliance', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (80, 55, 2, 19, 'Incident Resolution Time Compliance', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (81, 55, 2, 19, 'Request Fulfillment ', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (82, 55, 3, 43, 'Task Compliance (Remedy)', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (83, 55, 3, 43, 'Request Fulfillment (myproject.telkom.co.id)', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (84, 55, 2, 19, 'Specific Collaborative (Support Needed RAPIM/Transformer)', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (85, 55, 1, NULL, 'Internal Business Process', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (86, 55, 2, 47, 'Efektifitas DRP (Daftar Rencana Pengadaan)', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (87, 55, 3, 48, 'Penyiapan dokumen pengadaan (justifikasi, ToR, BoQ)', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (88, 55, 2, 47, 'Pelaksanaan Disaster Recovery Test', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (89, 55, 2, 47, 'Management Tindak Lanjut  (Completion of Milestones)', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (90, 55, 2, 47, 'Compliance SOA/ICOFR/ITGC/Integrated Audit', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (91, 55, 1, NULL, 'Learning & Growth', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (92, 55, 2, 53, 'Aktivasi KIPAS Budaya', NULL, NULL, NULL, NULL, 1, '2019-09-03 12:13:22', 1, '2019-09-03 12:13:22');
INSERT INTO `km_detail` VALUES (210, 107, 2, 209, 'UBIS CoE (O&M + G&A)', 20, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (132, 106, 3, 129, 'Availability of Service (Integration)', 4, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (131, 106, 3, 129, 'Availability of Service (SLA & OLA Achievement)', 8, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (130, 106, 3, 129, 'Availability of Service (SQUADRON AoS)', 10, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (129, 106, 2, 127, 'Availability of Service', 0, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (127, 106, 1, NULL, 'Customer', 0, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (128, 106, 2, 127, 'SLA Information System', 3, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (214, 107, 3, 213, 'Availability of Service (SQUADRON AoS)', 10, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (213, 107, 2, 211, 'Availability of Service', 0, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (212, 107, 2, 211, 'SLA Information System', 3, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (178, 54, 3, 175, 'Availability of Service (Integration)', 4, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (177, 54, 3, 175, 'Availability of Service (SLA & OLA Achievement)', 8, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (176, 54, 3, 175, 'Availability of Service (SQUADRON AoS)', 10, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (174, 54, 2, 173, 'SLA Information System', 3, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (175, 54, 2, 173, 'Availability of Service', 0, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (172, 54, 2, 171, 'UBIS CoE (O&M + G&A)', 20, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (173, 54, 1, NULL, 'Customer', 0, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (171, 54, 1, NULL, 'Financial', 0, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (215, 107, 3, 213, 'Availability of Service (SLA & OLA Achievement)', 8, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (216, 107, 3, 213, 'Availability of Service (Integration)', 4, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (217, 107, 2, 211, 'Response Time of Service', 0, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (218, 107, 3, 217, 'Response Time of Service (SQUADRON RToS)', 2, 'detik', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (219, 107, 3, 217, 'Response Time of Service (SLA & OLA Achievement)', 2, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (220, 107, 2, 211, 'SQUADRON Progress Report', 0, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (221, 107, 2, 211, 'Security Awareness - Standard Verifikasi Keamanan Aplikasi (Compliance)', 7, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (222, 107, 2, 211, 'Dashboard Performance', 0, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (223, 107, 3, 222, 'Dashboard Fulfillment', 0, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (224, 107, 3, 222, 'Dashboard Assurance', 0, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (225, 107, 3, 222, 'KPRO', 0, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (226, 107, 2, 211, 'Business Performance', 0, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (227, 107, 3, 226, 'PS Indihome <= 1 hari ', 2, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (228, 107, 3, 226, 'TTD Compliance BGES', 2, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (229, 107, 3, 226, 'MyIndihome Assurance', 2, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (230, 107, 3, 226, 'Billing Process', 2, 'jam', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (231, 107, 2, 211, 'DTP Development for Customer Experience & FAB', 2, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (232, 107, 2, 211, 'Pengelolaan Digital Workplace (Inventory, Update, Dismantling)', 3, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (233, 107, 2, 211, 'First Call Resolution Time Compliance', 0, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (234, 107, 2, 211, 'Incident Resolution Time Compliance', 5, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (235, 107, 2, 211, 'Request Fulfillment ', 0, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (236, 107, 3, 235, 'Task Compliance (Remedy)', 4, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (237, 107, 3, 235, 'Request Fulfillment (myproject.telkom.co.id)', 2, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (238, 107, 2, 211, 'Specific Collaborative (Support Needed RAPIM/Transformer)', 3, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (239, 107, 1, NULL, 'Internal Business Process', 0, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (240, 107, 2, 239, 'Efektifitas DRP (Daftar Rencana Pengadaan)', 0, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (241, 107, 3, 240, 'Penyiapan dokumen pengadaan (justifikasi, ToR, BoQ)', 2, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (242, 107, 2, 239, 'Pelaksanaan Disaster Recovery Test', 2, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (243, 107, 2, 239, 'Management Tindak Lanjut  (Completion of Milestones)', 0, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (244, 107, 2, 239, 'Compliance SOA/ICOFR/ITGC/Integrated Audit', 6, '%', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (245, 107, 1, NULL, 'Learning & Growth', 0, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_detail` VALUES (246, 107, 2, 245, 'Aktivasi KIPAS Budaya', 3, '%', NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for km_nilai
-- ----------------------------
DROP TABLE IF EXISTS `km_nilai`;
CREATE TABLE `km_nilai`  (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `km_id` int(4) NULL DEFAULT NULL,
  `km_detail_id` int(4) NULL DEFAULT NULL,
  `periode_bulan_id` int(4) NULL DEFAULT NULL,
  `target` float NULL DEFAULT NULL,
  `result` float NULL DEFAULT NULL,
  `score` float(255, 0) NULL DEFAULT NULL,
  `ach` float(255, 0) NULL DEFAULT NULL,
  `evidence_id` int(4) NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `status` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cuid` int(4) NULL DEFAULT NULL,
  `cdate` datetime(0) NULL DEFAULT NULL,
  `muid` int(4) NULL DEFAULT NULL,
  `mdate` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 451 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of km_nilai
-- ----------------------------
INSERT INTO `km_nilai` VALUES (335, 54, 208, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (334, 54, 208, 26, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (333, 54, 207, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (332, 54, 207, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (331, 54, 207, 26, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (330, 54, 206, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (329, 54, 206, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (328, 54, 206, 26, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (327, 54, 205, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (326, 54, 205, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (325, 54, 205, 26, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (324, 54, 204, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (323, 54, 204, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (322, 54, 204, 26, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (321, 54, 203, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (320, 54, 203, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (319, 54, 203, 26, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (318, 54, 202, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (317, 54, 202, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (316, 54, 202, 26, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (315, 54, 201, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (314, 54, 201, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (313, 54, 201, 26, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (312, 54, 200, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (311, 54, 200, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (310, 54, 200, 26, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (309, 54, 199, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (308, 54, 199, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (307, 54, 199, 26, 95, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (306, 54, 198, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (305, 54, 198, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (304, 54, 198, 26, 95, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (303, 54, 197, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (302, 54, 197, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (301, 54, 197, 26, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (300, 54, 196, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (299, 54, 196, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (108, 106, 132, 28, 99.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (107, 106, 132, 27, 99.9, 120, 105, 4, NULL, NULL, NULL, NULL, NULL, 1, '2019-10-15 13:44:14');
INSERT INTO `km_nilai` VALUES (106, 106, 132, 26, 99.9, 22, 95, 4, NULL, NULL, NULL, NULL, NULL, 1, '2019-10-15 12:48:39');
INSERT INTO `km_nilai` VALUES (105, 106, 131, 28, 99.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (104, 106, 131, 27, 99.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (103, 106, 131, 26, 99.9, 100, 100, 8, NULL, NULL, NULL, NULL, NULL, 1, '2019-10-15 13:42:47');
INSERT INTO `km_nilai` VALUES (102, 106, 130, 28, 99.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (101, 106, 130, 27, 99.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (100, 106, 130, 26, 99.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (99, 106, 129, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (98, 106, 129, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (97, 106, 129, 26, 0, 110, 105, 0, NULL, NULL, NULL, NULL, NULL, 1, '2019-10-15 13:01:33');
INSERT INTO `km_nilai` VALUES (96, 106, 128, 28, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (95, 106, 128, 27, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (94, 106, 128, 26, 100, 0, 95, 3, NULL, NULL, NULL, NULL, NULL, 1, '2019-10-15 13:56:44');
INSERT INTO `km_nilai` VALUES (93, 106, 127, 28, 99.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (92, 106, 127, 27, 99.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (91, 106, 127, 26, 99.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2019-10-15 13:01:57');
INSERT INTO `km_nilai` VALUES (298, 54, 196, 26, 97, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (297, 54, 195, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (296, 54, 195, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (295, 54, 195, 26, 40, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (294, 54, 194, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (293, 54, 194, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (292, 54, 194, 26, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (291, 54, 193, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (290, 54, 193, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (289, 54, 193, 26, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (288, 54, 192, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (287, 54, 192, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (286, 54, 192, 26, 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (285, 54, 191, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (284, 54, 191, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (283, 54, 191, 26, 90, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (282, 54, 190, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (281, 54, 190, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (280, 54, 190, 26, 86, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (279, 54, 189, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (278, 54, 189, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (277, 54, 189, 26, 77, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (276, 54, 188, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (275, 54, 188, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (274, 54, 188, 26, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (273, 54, 187, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (272, 54, 187, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (271, 54, 187, 26, 92, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (270, 54, 186, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (269, 54, 186, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (268, 54, 186, 26, 98, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (267, 54, 185, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (266, 54, 185, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (265, 54, 185, 26, 98, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (264, 54, 184, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (263, 54, 184, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (262, 54, 184, 26, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (261, 54, 183, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (260, 54, 183, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (259, 54, 183, 26, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (258, 54, 182, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (257, 54, 182, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (256, 54, 182, 26, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (255, 54, 181, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (254, 54, 181, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (253, 54, 181, 26, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (252, 54, 180, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (251, 54, 180, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (250, 54, 180, 26, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (249, 54, 179, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (248, 54, 179, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (247, 54, 179, 26, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (246, 54, 178, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (245, 54, 178, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (244, 54, 178, 26, 99.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (243, 54, 177, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (242, 54, 177, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (241, 54, 177, 26, 99.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (240, 54, 176, 28, 99.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (239, 54, 176, 27, 99.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (238, 54, 176, 26, 99.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (237, 54, 175, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (236, 54, 175, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (235, 54, 175, 26, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (234, 54, 174, 28, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (233, 54, 174, 27, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (232, 54, 174, 26, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (231, 54, 173, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (230, 54, 173, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (229, 54, 173, 26, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (228, 54, 172, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (227, 54, 172, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (226, 54, 172, 26, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (225, 54, 171, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (224, 54, 171, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (223, 54, 171, 26, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (336, 54, 208, 28, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (337, 107, 209, 32, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (338, 107, 209, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (339, 107, 209, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (340, 107, 210, 32, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (341, 107, 210, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (342, 107, 210, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (343, 107, 211, 32, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (344, 107, 211, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (345, 107, 211, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (346, 107, 212, 32, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (347, 107, 212, 33, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (348, 107, 212, 34, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (349, 107, 213, 32, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (350, 107, 213, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (351, 107, 213, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (352, 107, 214, 32, 99.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (353, 107, 214, 33, 99.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (354, 107, 214, 34, 99.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (355, 107, 215, 32, 99.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (356, 107, 215, 33, 99.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (357, 107, 215, 34, 99.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (358, 107, 216, 32, 99.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (359, 107, 216, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (360, 107, 216, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (361, 107, 217, 32, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (362, 107, 217, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (363, 107, 217, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (364, 107, 218, 32, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (365, 107, 218, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (366, 107, 218, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (367, 107, 219, 32, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (368, 107, 219, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (369, 107, 219, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (370, 107, 220, 32, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (371, 107, 220, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (372, 107, 220, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (373, 107, 221, 32, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (374, 107, 221, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (375, 107, 221, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (376, 107, 222, 32, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (377, 107, 222, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (378, 107, 222, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (379, 107, 223, 32, 98, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (380, 107, 223, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (381, 107, 223, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (382, 107, 224, 32, 98, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (383, 107, 224, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (384, 107, 224, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (385, 107, 225, 32, 92, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (386, 107, 225, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (387, 107, 225, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (388, 107, 226, 32, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (389, 107, 226, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (390, 107, 226, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (391, 107, 227, 32, 77, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (392, 107, 227, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (393, 107, 227, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (394, 107, 228, 32, 86, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (395, 107, 228, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (396, 107, 228, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (397, 107, 229, 32, 90, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (398, 107, 229, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (399, 107, 229, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (400, 107, 230, 32, 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (401, 107, 230, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (402, 107, 230, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (403, 107, 231, 32, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (404, 107, 231, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (405, 107, 231, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (406, 107, 232, 32, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (407, 107, 232, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (408, 107, 232, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (409, 107, 233, 32, 40, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (410, 107, 233, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (411, 107, 233, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (412, 107, 234, 32, 97, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (413, 107, 234, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (414, 107, 234, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (415, 107, 235, 32, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (416, 107, 235, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (417, 107, 235, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (418, 107, 236, 32, 95, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (419, 107, 236, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (420, 107, 236, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (421, 107, 237, 32, 95, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (422, 107, 237, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (423, 107, 237, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (424, 107, 238, 32, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (425, 107, 238, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (426, 107, 238, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (427, 107, 239, 32, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (428, 107, 239, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (429, 107, 239, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (430, 107, 240, 32, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (431, 107, 240, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (432, 107, 240, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (433, 107, 241, 32, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (434, 107, 241, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (435, 107, 241, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (436, 107, 242, 32, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (437, 107, 242, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (438, 107, 242, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (439, 107, 243, 32, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (440, 107, 243, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (441, 107, 243, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (442, 107, 244, 32, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (443, 107, 244, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (444, 107, 244, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (445, 107, 245, 32, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (446, 107, 245, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (447, 107, 245, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (448, 107, 246, 32, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (449, 107, 246, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `km_nilai` VALUES (450, 107, 246, 34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for periode
-- ----------------------------
DROP TABLE IF EXISTS `periode`;
CREATE TABLE `periode`  (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `tahun_id` int(4) NULL DEFAULT NULL,
  `nama_periode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `is_active` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  `cuid` int(4) NULL DEFAULT NULL,
  `cdate` datetime(0) NULL DEFAULT NULL,
  `muid` int(4) NULL DEFAULT NULL,
  `mdate` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 32 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of periode
-- ----------------------------
INSERT INTO `periode` VALUES (26, 2019, 'Q1', 'Quartal 1', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `periode` VALUES (27, 2019, 'Q2', 'Quartal 2', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `periode` VALUES (28, 2019, 'Q3', 'Quartal 3', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `periode` VALUES (29, 2019, 'Q4', 'Quartal 4', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:13:57');

-- ----------------------------
-- Table structure for periode_bulan
-- ----------------------------
DROP TABLE IF EXISTS `periode_bulan`;
CREATE TABLE `periode_bulan`  (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `periode_id` int(4) NULL DEFAULT NULL,
  `nama_bulan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `is_active` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  `cuid` int(4) NULL DEFAULT NULL,
  `cdate` datetime(0) NULL DEFAULT NULL,
  `muid` int(4) NULL DEFAULT NULL,
  `mdate` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 39 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of periode_bulan
-- ----------------------------
INSERT INTO `periode_bulan` VALUES (26, 26, 'Januari', 'Q1 Januari', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `periode_bulan` VALUES (27, 26, 'Februari', 'Q1 Februari', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `periode_bulan` VALUES (28, 26, 'Maret', 'Q1 Maret', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `periode_bulan` VALUES (29, 27, 'April', 'Q2 April', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:13:57');
INSERT INTO `periode_bulan` VALUES (30, 27, 'Mei', 'Q2 Mei', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-15 15:03:14');
INSERT INTO `periode_bulan` VALUES (31, 27, 'Juni', 'Q2 Juni', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-15 15:03:14');
INSERT INTO `periode_bulan` VALUES (32, 28, 'Juli', 'Q3 Juli', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-15 15:03:14');
INSERT INTO `periode_bulan` VALUES (33, 28, 'Agustus', 'Q3 Agustus', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-15 15:03:14');
INSERT INTO `periode_bulan` VALUES (34, 28, 'September', 'Q3 September', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-15 15:03:14');
INSERT INTO `periode_bulan` VALUES (35, 29, 'Oktober', 'Q4 Oktober', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-15 15:03:14');
INSERT INTO `periode_bulan` VALUES (36, 29, 'November', 'Q4 November', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-15 15:03:14');
INSERT INTO `periode_bulan` VALUES (37, 29, 'Desember', 'Q4 Desember', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-15 15:03:14');

-- ----------------------------
-- Table structure for tahun
-- ----------------------------
DROP TABLE IF EXISTS `tahun`;
CREATE TABLE `tahun`  (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `tahun` int(4) NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `is_active` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  `cuid` int(4) NULL DEFAULT NULL,
  `cdate` datetime(0) NULL DEFAULT NULL,
  `muid` int(4) NULL DEFAULT NULL,
  `mdate` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2025 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tahun
-- ----------------------------
INSERT INTO `tahun` VALUES (2018, 2018, 'Dua Ribu Delapan Belas', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `tahun` VALUES (2019, 2019, 'Dua Ribu Sembilan Belas', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `tahun` VALUES (2020, 2020, 'Dua Ribu Dua Puluh', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `tahun` VALUES (2021, 2021, 'Dua Ribu Dua Puluh Satu', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:13:57');
INSERT INTO `tahun` VALUES (2022, 2022, 'Dua Ribu Dua Puluh Dua', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:13:57');
INSERT INTO `tahun` VALUES (2023, 2023, 'Dua Ribu Dua Puluh Tiga', '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:13:57');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` smallint(4) NOT NULL AUTO_INCREMENT,
  `level_id` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `bidang_id` smallint(5) NULL DEFAULT NULL,
  `username` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password_plan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `foto` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `last_login` datetime(0) NOT NULL,
  `cuid` smallint(4) NULL DEFAULT NULL,
  `cdate` datetime(0) NULL DEFAULT NULL,
  `muid` int(4) NULL DEFAULT NULL,
  `mdate` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `level_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 35 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'ADM', NULL, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'Profile_2015.08.16-09.41.20-.png', '0000-00-00 00:00:00', 1, '2015-08-16 14:35:31', 1, '2019-05-18 09:20:48');
INSERT INTO `user` VALUES (13, 'USR', 26, 'admin-cit', '5f4dcc3b5aa765d61d8327deb882cf99', 'password', 'user_2019.10.02-04.39.56-.jpg', '0000-00-00 00:00:00', 1, '2019-10-02 09:39:56', NULL, NULL);
INSERT INTO `user` VALUES (14, 'USR', 27, 'admin-bpo', '5f4dcc3b5aa765d61d8327deb882cf99', 'password', 'user_2019.10.02-04.42.30-.jpg', '0000-00-00 00:00:00', 1, '2019-10-02 09:42:30', NULL, NULL);
INSERT INTO `user` VALUES (15, 'USR', 28, 'admin-epo', '5f4dcc3b5aa765d61d8327deb882cf99', 'password', 'user_2019.10.02-04.43.08-.jpg', '0000-00-00 00:00:00', 1, '2019-10-02 09:43:08', NULL, NULL);
INSERT INTO `user` VALUES (16, 'USR', 29, 'admin-opo', '5f4dcc3b5aa765d61d8327deb882cf99', 'password', 'user_2019.10.02-04.43.39-.jpg', '0000-00-00 00:00:00', 1, '2019-10-02 09:43:39', NULL, NULL);
INSERT INTO `user` VALUES (17, 'USR', 30, 'admin-spo', '5f4dcc3b5aa765d61d8327deb882cf99', 'password', 'user_2019.10.02-04.44.03-.jpg', '0000-00-00 00:00:00', 1, '2019-10-02 09:44:03', NULL, NULL);
INSERT INTO `user` VALUES (18, 'USR', 31, 'admin-ipa', '5f4dcc3b5aa765d61d8327deb882cf99', 'password', 'user_2019.10.02-04.44.42-.jpg', '0000-00-00 00:00:00', 1, '2019-10-02 09:44:42', NULL, NULL);
INSERT INTO `user` VALUES (19, 'USR', 32, 'admin-bpd', '5f4dcc3b5aa765d61d8327deb882cf99', 'password', 'user_2019.10.02-04.45.07-.jpg', '0000-00-00 00:00:00', 1, '2019-10-02 09:45:07', NULL, NULL);
INSERT INTO `user` VALUES (20, 'USR', 33, 'admin-epd', '5f4dcc3b5aa765d61d8327deb882cf99', 'password', 'user_2019.10.02-04.45.31-.jpg', '0000-00-00 00:00:00', 1, '2019-10-02 09:45:31', NULL, NULL);
INSERT INTO `user` VALUES (21, 'USR', 34, 'admin-opd', '5f4dcc3b5aa765d61d8327deb882cf99', 'password', 'user_2019.10.02-04.46.00-.jpg', '0000-00-00 00:00:00', 1, '2019-10-02 09:46:00', NULL, NULL);
INSERT INTO `user` VALUES (22, 'USR', 35, 'admin-spd', '5f4dcc3b5aa765d61d8327deb882cf99', 'password', 'user_2019.10.02-04.46.22-.jpg', '0000-00-00 00:00:00', 1, '2019-10-02 09:46:22', NULL, NULL);
INSERT INTO `user` VALUES (23, 'USR', 36, 'admin-gaf', '5f4dcc3b5aa765d61d8327deb882cf99', 'password', 'user_2019.10.02-04.46.46-.jpg', '0000-00-00 00:00:00', 1, '2019-10-02 09:46:46', NULL, NULL);
INSERT INTO `user` VALUES (24, 'APR', 26, 'approve-cit', '5f4dcc3b5aa765d61d8327deb882cf99', 'password', 'user_2019.10.02-04.47.15-.png', '0000-00-00 00:00:00', 1, '2019-10-02 09:47:15', NULL, NULL);
INSERT INTO `user` VALUES (25, '', NULL, 'approve-cit', '', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (26, '', NULL, 'approve-cit', '', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (27, '', NULL, 'approve-cit', '', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (28, '', NULL, 'approve-cit', '', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (29, '', NULL, 'approve-cit', '', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (30, '', NULL, 'approve-cit', '', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (31, '', NULL, 'approve-cit', '', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (32, '', NULL, 'approve-cit', '', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (33, '', NULL, 'approve-cit', '', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (34, '', NULL, 'approve-cit', '', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for user_level
-- ----------------------------
DROP TABLE IF EXISTS `user_level`;
CREATE TABLE `user_level`  (
  `id` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `home` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '00000',
  `dashboard` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '00000',
  `km` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '00000',
  `km_detail` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '00000',
  `km_nilai` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '00000',
  `evidence` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '00000',
  `bidang` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '00000',
  `bidang_kategori` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '00000',
  `tahun` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '00000',
  `periode` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '00000',
  `periode_bulan` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '00000',
  `setting` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '00000',
  `profile` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '00000',
  `user` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '00000',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_level
-- ----------------------------
INSERT INTO `user_level` VALUES ('ADM', 'Super Admin', '11111', '11111', '11111', '11111', '11111', '11111', '11111', '11111', '11111', '11111', '11111', '11111', '11111', '11111');
INSERT INTO `user_level` VALUES ('USR', 'User Bidang', '11111', '11111', '00000', '11111', '11111', '11111', '00000', '00000', '00000', '00000', '00000', '00000', '11111', '00000');
INSERT INTO `user_level` VALUES ('APR', 'Penyetuju', '11111', '11111', '00000', '11111', '11111', '11111', '00000', '00000', '00000', '00000', '00000', '00000', '11111', '00000');

-- ----------------------------
-- Table structure for user_token
-- ----------------------------
DROP TABLE IF EXISTS `user_token`;
CREATE TABLE `user_token`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `datetime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 581 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_token
-- ----------------------------
INSERT INTO `user_token` VALUES (577, '73f0e71508ddc43f0944d0e406cf8660', 1, '2019-10-28 02:55:26');
INSERT INTO `user_token` VALUES (559, '77276f0fc1f7893f5b61c58a6a181e8f', 5, '2019-09-28 10:39:22');
INSERT INTO `user_token` VALUES (394, '6489fc4116436e5e6b121da5df95e035', 6, '2015-05-01 22:46:08');
INSERT INTO `user_token` VALUES (460, '0471a72cacb7b1f4ef0f7a39b7f6addf', 7, '2015-08-21 02:39:12');
INSERT INTO `user_token` VALUES (558, 'cd0e2ef3d874fd079e4a13e3c216575a', 10, '2019-09-28 10:39:11');
INSERT INTO `user_token` VALUES (556, '07ac19ce25eb8a1f951520c1cad6f6f4', 11, '2019-09-28 10:38:07');
INSERT INTO `user_token` VALUES (578, 'd39da5410d014328a714d725ab44229a', 13, '2019-10-28 02:55:43');
INSERT INTO `user_token` VALUES (579, '6577535d9fa5f7ee5e180d4699e827be', 17, '2019-10-28 02:57:17');
INSERT INTO `user_token` VALUES (580, '66cea5b79974532f56bf6f7c6ab1e1ef', 24, '2019-10-28 03:12:29');

SET FOREIGN_KEY_CHECKS = 1;
